﻿using Flight.TravelAPI.Models.Enumeration;

namespace Flight.TravelAPI.Providers.Base
{
    public interface IEngine
    {
        GDS BasedOn { get; set; }
        string Engine { get; set; }
        string UserName { get; set; }
        string UserPwd { get; set; }
        string PccCode { get; set; }
        string RedEyeTime { get; set; }
        string ApiUrl { get; set; }
    }
}