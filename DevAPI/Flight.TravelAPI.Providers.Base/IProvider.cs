﻿using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Flight.TravelAPI.Models.Request;
namespace Flight.TravelAPI.Providers.Base
{
    public interface IProvider : IEngine
    {
        List<SearchType> SearchTypes { get; set; }
        ProviderDetail ProviderDetails { get; set; }

        IEnumerable<FlightContracts> Process(FlightSearchDetails flightSearchDetails, ConcurrentBag<ResponseDetail> responseDetails);
        ContractDetails GetDataUsingUniversalLocatorCode(string locatorCode,string patnerPCC);
        ContractDetails GetDataUsingPNR(string pnr);
    }
}