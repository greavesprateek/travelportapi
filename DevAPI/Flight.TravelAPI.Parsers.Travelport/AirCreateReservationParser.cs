﻿using System;
using System.Linq;
using System.Xml.Linq;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Parsers.Parser.Interfaces;
using Microsoft.Extensions.Logging;

namespace Flight.TravelAPI.Parsers.Travelport
{
    public class AirCreateReservationParser : IBookingParser
    {
        private readonly ILogger<AirCreateReservationParser> _logger;

        public AirCreateReservationParser(ILogger<AirCreateReservationParser> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public BookingResponse Parse(ProviderRequest ProviderReq, string response)
        {
            BookingResponse bookingResponse = new BookingResponse { ResponseXML = response };
            try
            {
                if (response != null)
                {
                    //XDocument airCreateResponse = XDocument.Parse(response);
                    XElement airCreateResponse = XElement.Parse(response);
                    //airCreateResponse = GetClearObject(airCreateResponse);
                    XNamespace nsSoapEnvelope = "http://schemas.xmlsoap.org/soap/envelope/";
                    XNamespace nsCommon_v50_0 = "http://www.travelport.com/schema/common_v50_0";
                    XNamespace nsUniversal = "http://www.travelport.com/schema/universal_v50_0";
                    XNamespace nsAir = "http://www.travelport.com/schema/air_v50_0";
                    #region Error Checking
                    var xErrorInfo = airCreateResponse.Descendants(nsCommon_v50_0 + "ErrorInfo");
                    var xFault = airCreateResponse.Descendants(nsCommon_v50_0 + "Fault");
                    if (xErrorInfo != null && xErrorInfo.Any())
                    {
                        var xCode = xErrorInfo.Descendants(nsCommon_v50_0 + "Code");
                        var xAuxdata = xErrorInfo.Descendants(nsCommon_v50_0 + "Auxdata");
                        
                        bookingResponse.Errors = new Error
                        {
                            Category = ErrorCategory.Error,
                            Code = xCode.Any() ? xCode.ElementAt(0).Value : "Error Code Not Found",
                            Message = xAuxdata.Any() ? xAuxdata.First().Elements().First().Element(nsCommon_v50_0 + "Description").Value : "Error Description Not Found",
                            Type = ErrorType.GDSError
                        };
                        bookingResponse.IsContractSoldOutOrUnavailable = true;
                        bookingResponse.CurrentBookingStatus = BookingStatus.SoldOutOrUnavailable;
                        return bookingResponse;
                    }
                    else if (xFault != null && xFault.Any())
                    {
                        var xfaultcode = xFault.Descendants("faultcode");
                        var xfaultstring = xFault.Descendants("faultstring");
                        var xDescription = xErrorInfo.Descendants(nsCommon_v50_0 + "Description");
                        var code = xfaultcode.Any() ? xfaultcode.First().Value : "Error Code Not Found";
                        var faultstring = xfaultstring.Any() ? xfaultstring.First().Value : "Error Description Not Found";
                        var description = xDescription.Any() ? xDescription.First().Value : "";
                        bookingResponse.Errors = new Error
                        {
                            Category = ErrorCategory.Error,
                            Code = $"{code}",
                            Message = $"{faultstring}. {description}",
                            Type = ErrorType.GDSError
                        };
                        bookingResponse.IsContractSoldOutOrUnavailable = true;
                        bookingResponse.CurrentBookingStatus = BookingStatus.SoldOutOrUnavailable;
                        return bookingResponse;
                    }
                    #endregion

                    #region Find PNR and Success Booking
                    var providerReservation = airCreateResponse.Descendants(nsUniversal + "ProviderReservationInfo");
                    if (providerReservation.Any())
                    {
                        string createDate = providerReservation.First().Attribute("CreateDate").Value;
                        if (!string.IsNullOrEmpty(createDate))
                            bookingResponse.BookingDate = GetDate(createDate);

                        bookingResponse.PNR = providerReservation.First().Attribute("LocatorCode").Value;
                    }
                    if (!string.IsNullOrEmpty(bookingResponse.PNR))
                    {
                        bookingResponse.CurrentBookingStatus = BookingStatus.BookingConfirmed;
                        bookingResponse.GDSStatus = GDSStatus.Confirmed;
                        bookingResponse.IsContractSoldOutOrUnavailable = false;
                        bookingResponse.IsPriceChanged = false;
                        bookingResponse.IsSuccess = true;
                        bookingResponse.SoldOutContract = null;
                        bookingResponse.PriceChangedContract = null;
                    }

                    var universalRecord = airCreateResponse.Descendants(nsUniversal + "UniversalRecord");
                    if (universalRecord.Any())
                    {
                        //Universal Record number
                        bookingResponse.GDSConfirmationNo = universalRecord.First().Attribute("LocatorCode").Value;
                        bookingResponse.Version = universalRecord.First().Attribute("Version").Value;
                    }
                    #endregion

                    #region find seats info
                    //if (ProviderReq.AncillarySeats != null && ProviderReq.AncillarySeats.Count > 0)
                    //{
                    //    try
                    //    {
                    //        IEnumerable<XElement> Seats = airCreateResponse.Descendants("AirSeatAssignment");
                    //        TripDetails tripDetails = ProviderReq.FlightBooking.SelectedContract.TripDetails;
                    //        if (Seats != null && Seats.Any())
                    //        {
                    //            foreach (XElement seat in Seats)
                    //            {
                    //                FlightSegments segment = null;
                    //                string segmentRef = seat.Attribute("SegmentRef").Value;
                    //                if (tripDetails.OutBoundSegment.Any(x => x.SegmentKey == segmentRef))
                    //                {
                    //                    segment = tripDetails.OutBoundSegment.First(x => x.SegmentKey == segmentRef);
                    //                }
                    //                else if (tripDetails.InBoundSegment.Any(x => x.SegmentKey == segmentRef))
                    //                {
                    //                    segment = tripDetails.InBoundSegment.First(x => x.SegmentKey == segmentRef);
                    //                }
                    //                if (segment != null && ProviderReq.AncillarySeats.Any(x => x.SegmentOrderId == segment.SegmentID))
                    //                {
                    //                    AncillarySeats ancillarySeats = ProviderReq.AncillarySeats.First(x => x.SegmentOrderId == segment.SegmentID + 1);
                    //                    ancillarySeats.Status = "HK";
                    //                    ancillarySeats.IsSeatAvailable = true;
                    //                    ancillarySeats.BookingResponseXML = seat.ToString();
                    //                }
                    //            }
                    //        }
                    //        bookingResponse.AncillarySeats = ProviderReq.AncillarySeats;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        _logger.LogError($"{ex.Message} {ex.StackTrace}");
                    //    }

                    //}
                    #endregion
                    bookingResponse.FareDifference = 0;
                    bookingResponse.Errors = new Error();
                    bookingResponse.NoOfTST = 0;
                    bookingResponse.PriceChangedContract = new ContractDetails();
                    bookingResponse.SoldOutContract = new ContractDetails();

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return bookingResponse;
        }

        private DateTime GetDate(string dateToParse, int year = -1)
        {
            try
            {
                if (!string.IsNullOrEmpty(dateToParse))
                {
                    if (year == -1)
                    {
                        year = DateTime.Now.Year;
                    }
                    int yearValue = 0;
                    int monthValue = 0;
                    int dayValue = 0;
                    int hourValue = 0;
                    int minuteValue = 0;

                    string[] dateAndTimeArray = dateToParse.Split('T');
                    if (dateAndTimeArray.Length > 1)
                    {
                        string[] dateArray = dateAndTimeArray.ElementAt(0).Split('-');
                        string[] timeArray = dateAndTimeArray.ElementAt(1).Split(':');

                        if (dateArray.Length > 2)
                        {
                            yearValue = int.Parse(dateArray.ElementAt(0));
                            monthValue = int.Parse(dateArray.ElementAt(1));
                            dayValue = int.Parse(dateArray.ElementAt(2));
                        }
                        if (timeArray.Length > 1)
                        {
                            hourValue = int.Parse(timeArray.ElementAt(0));
                            minuteValue = int.Parse(timeArray.ElementAt(1));
                        }
                    }
                    else
                    {
                        string[] dateArray = dateAndTimeArray.ElementAt(0).Split('-');
                        if (dateArray.Length > 2)
                        {
                            yearValue = int.Parse(dateArray.ElementAt(0));
                            monthValue = int.Parse(dateArray.ElementAt(1));
                            dayValue = int.Parse(dateArray.ElementAt(2));
                        }
                    }
                    return new DateTime(yearValue, monthValue, dayValue, hourValue, minuteValue, 0);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return new DateTime();
        }
    }
}
