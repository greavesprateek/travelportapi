﻿using Flight.TravelAPI.ClassExtensions;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using Flight.TravelAPI.Utilities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Flight.TravelAPI.Parsers.Travelport
{
    public class Parser
    {
        private readonly IConfigurationData _configurationData;
        private readonly ILogger<Parser> _logger;

        public Parser(ILogger<Parser> logger, IConfigurationData configurationData)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationData = configurationData ?? throw new ArgumentNullException(nameof(configurationData));
        }

        public ConcurrentBag<FlightContracts> Parse(string response, FlightSearchDetails flightSearchDetails,
            ProviderDetail detail)
        {
            var flightContracts = new ConcurrentBag<FlightContracts>();
            XNamespace airNamespace = "http://www.travelport.com/schema/air_v50_0";
            XNamespace commonNamespace = "http://www.travelport.com/schema/common_v50_0";
            XNamespace soapNamespace = "http://schemas.xmlsoap.org/soap/envelope/";

            try
            {
                if (string.IsNullOrEmpty(response))
                {
                    return flightContracts;
                }

                XElement xDoc = XElement.Parse(response);
                //xDoc = GetClearObject(xDoc);
                xDoc = xDoc.Descendants(soapNamespace + "Body").FirstOrDefault();

                #region Check For Any Fault/Error
                var fault = xDoc.Descendants("Fault").Any()
                    ? xDoc.Descendants("Fault")
                    : null;

                if (fault != null)
                    return flightContracts;

                var errorInfo = xDoc.Descendants(commonNamespace + "ErrorInfo").Any()
                    ? xDoc.Descendants(commonNamespace + "ErrorInfo")
                    : null;

                if (errorInfo != null)
                    return flightContracts;

                errorInfo = xDoc.Descendants(commonNamespace + "ResponseMessage").Any()
                    ? xDoc.Descendants(commonNamespace + "ResponseMessage")
                    : null;

                if (errorInfo != null && errorInfo.Any() && errorInfo.Any(x => x.Attribute(commonNamespace + "Code") != null && x.Attribute(commonNamespace + "Code").Value == "2027"))
                {
                    return flightContracts;
                }
                #endregion

                #region Reading/Identifying All The Neccessary Elements Of Response XML
                xDoc = xDoc.Descendants(airNamespace + "LowFareSearchRsp").FirstOrDefault();
                var xFlightDetailList = xDoc.Descendants(airNamespace + "FlightDetailsList");
                if (xFlightDetailList == null || !xFlightDetailList.Any()) return flightContracts;

                var xAirSegmentList = xDoc.Descendants(airNamespace + "AirSegmentList").Elements();
                if (xAirSegmentList == null || !xAirSegmentList.Any()) return flightContracts;

                var xFareInfoList = xDoc.Descendants(airNamespace + "FareInfoList");
                if (xFareInfoList == null || !xFareInfoList.Any()) return flightContracts;

                var xRoutList = xDoc.Descendants(airNamespace + "RouteList");
                if (xRoutList == null || !xRoutList.Any()) return flightContracts;

                var xAirPricingSolutionList = xDoc.Descendants(airNamespace + "AirPricingSolution");
                if (xAirPricingSolutionList == null || !xAirPricingSolutionList.Any()) return flightContracts;

                var xBrandList = xDoc.Descendants(airNamespace + "BrandList");
                //if (xBrandList == null || !xBrandList.Any()) return flightContracts;

                XElement[] flightDetailList_Array = xFlightDetailList as XElement[] ?? xFlightDetailList.ToArray();
                XElement[] airSegmentList_Array = xAirSegmentList as XElement[] ?? xAirSegmentList.ToArray();
                XElement[] fareInfoList_Array = xFareInfoList as XElement[] ?? xFareInfoList.ToArray();
                XElement[] routList_Array = xRoutList as XElement[] ?? xRoutList.ToArray();
                XElement[] airPricingSolutionList_Array = xAirPricingSolutionList as XElement[] ?? xAirPricingSolutionList.ToArray();
                XElement[] brandList_Array = xBrandList as XElement[] ?? xBrandList.ToArray();
                #endregion

                Dictionary<string, FlightSegments> flightSegmentsList = new Dictionary<string, FlightSegments>();
                flightSegmentsList = GetListOfSegment(airSegmentList_Array, airNamespace);
                int itemNumber = 0; //API Contract Number Variable

                foreach (var airPricingSolution in airPricingSolutionList_Array)
                {
                    try
                    {
                        //_logger.LogInformation($"Parsing Start For AirSolution : {airPricing}");
                        FlightContracts flightContract = new FlightContracts()
                        {
                            #region Initializing List and Objects
                            TripDetails = new TripDetails(),
                            ListOfSegmentIndex = new List<Connection>(),
                            //priceBaggageInfo = new List<string>(),
                            ListOfHostToken = new List<HostToken>(),
                            rules = new PriceChangeRules(),
                            AdultFare = null,
                            Child12To17Fare = null,
                            ChildFare = null,
                            InfantFare = null,
                            InfantInLapFare = null,
                            SeniorFare = null,
                            #endregion
                            //FlightSearchDetail = flightSearchDetails,
                            Provider = detail.ProviderEngine,
                            EnginePriority = detail.EnginePriority,
                            ItemNumber = ++itemNumber,
                            AdultCount = flightSearchDetails.AdultCount,
                            ChildCount = flightSearchDetails.ChildCount,
                            InfantCount = flightSearchDetails.InfantCount,
                            Child12To17Count = flightSearchDetails.Child12To17Count,
                            InfantInLapCount = flightSearchDetails.InfantInLapCount,
                            TripType = Convert.ToString(flightSearchDetails.TripType),
                            JourneyType = (flightSearchDetails.TripType == Models.Enumerations.TripType.ROUNDTRIP) ? "RT" : "OW"
                        };

                        //StringBuilder gdsResponseXML = new StringBuilder();
                        //gdsResponseXML.Append("<TravelportResponse>");
                        //gdsResponseXML.Append(airPricingSolution);

                        List<string> fareBasisCodes = new List<string>();
                        bool isOutbound = false;
                        bool isInBound = false;

                        #region Specific Flight/ AirPricing Related XElement Intialization
                        List<FlightSegments> xflightDetailList = new List<FlightSegments>();
                        List<XElement> xSegmentList = new List<XElement>();

                        List<XElement> xfareInfoList = new List<XElement>();
                        List<XElement> xbrandList = new List<XElement>();

                        #endregion
                        flightContract.ListOfSegmentIndex = GetConnectionList(airPricingSolution, airNamespace);
                        flightContract.ListOfHostToken = GetHostTokenList(airPricingSolution, airNamespace);

                        flightContract.AirPricingKey = airPricingSolution.Attribute("Key") != null ? airPricingSolution.Attribute("Key").Value : "";
                        flightContract.AirPricingKey = airPricingSolution.Attribute("Key") != null
                            ? airPricingSolution.Attributes("Key").ElementAtOrDefault(0).Value
                            : "";
                        var ApproximateBasePriceWithCurrency = airPricingSolution.Attribute("ApproximateBasePrice") != null
                            ? GrabAmount(airPricingSolution.Attribute("ApproximateBasePrice").Value)
                            : "";
                        var BasePriceWithCurrency = airPricingSolution.Attribute("BasePrice") != null
                            ? GrabAmount(airPricingSolution.Attribute("BasePrice").Value)
                            : "";
                        flightContract.TotalBaseFare = !string.IsNullOrEmpty(ApproximateBasePriceWithCurrency)
                            ? float.Parse(ApproximateBasePriceWithCurrency)
                            : 0.0f;
                        var ApproximateTaxesWithCurrency = airPricingSolution.Attribute("ApproximateTaxes") != null
                            ? GrabAmount(airPricingSolution.Attribute("ApproximateTaxes").Value)
                            : "";
                        var TaxesWithCurrency = airPricingSolution.Attribute("Taxes") != null
                            ? GrabAmount(airPricingSolution.Attribute("Taxes").Value)
                            : "";
                        flightContract.TotalTax = !string.IsNullOrEmpty(ApproximateTaxesWithCurrency)
                            ? float.Parse(ApproximateTaxesWithCurrency)
                            : 0.0f;
                        var ApproximateTotalPriceWithCurrency = airPricingSolution.Attribute("ApproximateTotalPrice") != null
                            ? GrabAmount(airPricingSolution.Attribute("ApproximateTotalPrice").Value)
                            : "";
                        var TotalPriceWithCurrency = airPricingSolution.Attribute("TotalPrice") != null
                            ? GrabAmount(airPricingSolution.Attribute("TotalPrice").Value)
                            : "";
                        flightContract.TotalGDSQuotedAmount = !string.IsNullOrEmpty(ApproximateTotalPriceWithCurrency)
                            ? float.Parse(ApproximateTotalPriceWithCurrency)
                            : 0.0f;

                        #region Getting AirSegment reference
                        var flightContractJourneySegmentRef = airPricingSolution.Descendants(airNamespace + "Journey").Any()
                            ? (from segment in airPricingSolution.Descendants(airNamespace + "Journey").Descendants(airNamespace + "AirSegmentRef")
                               select segment.Attributes("Key").ElementAtOrDefault(0).Value).ToList()
                            : null;
                        #endregion

                        #region Getting AirLeg reference
                        var legRef = airPricingSolution.Descendants(airNamespace + "LegRef").Any()
                            ? (from segment in airPricingSolution.Descendants(airNamespace + "LegRef")
                               select segment.Attributes("Key").ElementAtOrDefault(0).Value).ToList()
                            : null;
                        #endregion


                        #region Getting AirSegment XElement
                        foreach (var airSegmentKey in flightContractJourneySegmentRef)
                        {
                            xSegmentList.AddRange(from airSegment in airSegmentList_Array
                                                  where airSegment.Attributes("Key").ElementAtOrDefault(0).Value == airSegmentKey
                                                  select airSegment);
                        }
                        //foreach (var airSegment in xSegmentList)
                        //    gdsResponseXML.Append(airSegment);
                        #endregion

                        #region Getting FlightDetail Reference Key And FlightDetail Xelement
                        var flightDetailsRef = (from flightKey in xSegmentList.Descendants(airNamespace + "FlightDetailsRef")
                                                select flightKey.Attributes("Key").ElementAtOrDefault(0).Value).ToList();
                        foreach (var flightRef in flightDetailsRef)
                        {
                            xflightDetailList.Add(flightSegmentsList[flightRef]);
                        }
                        //foreach (var key in flightDetailsRef)
                        //    gdsResponseXML.Append((from flightDetail in flightDetailList_Array.Descendants(airNamespace + "FlightDetails")
                        //                           where flightDetail.Attributes("Key").ElementAtOrDefault(0).Value == key
                        //                           select flightDetail).FirstOrDefault());

                        #endregion

                        List<XElement> outBoundAirSegmentList = (from segment in xSegmentList
                                                                 where segment.Attributes("Group").ElementAtOrDefault(0).Value == "0"
                                                                 select segment).ToList();
                        List<XElement> inBoundAirSegmentList = (from segment in xSegmentList
                                                                where segment.Attributes("Group").ElementAtOrDefault(0).Value == "1"
                                                                select segment).ToList();

                        XElement singlePricingInfo = airPricingSolution.Descendants(airNamespace + "AirPricingInfo") != null
                          ? airPricingSolution.Descendants(airNamespace + "AirPricingInfo").FirstOrDefault()
                          : null;

                        var PlatingCarrier = singlePricingInfo.Attribute("PlatingCarrier") != null ? singlePricingInfo.Attribute("PlatingCarrier").Value : "";
                        var airlineCode = singlePricingInfo.Attribute("PlatingCarrier") != null ? singlePricingInfo.Attribute("PlatingCarrier").Value : "";
                        var airline = _configurationData.AllAirlines.FirstOrDefault(x =>
                           x.Key == airlineCode);
                        flightContract.ValidatingCarrier.AirlineCode = airlineCode;
                       
                        if (outBoundAirSegmentList.Any())
                        {
                            List<TimeSpan> boundLayOverTime = new List<TimeSpan>();
                            flightContract.TripDetails.OutBoundSegment = new List<FlightSegments>();
                            List<FlightSegments> flightSegments;// = new List<FlightSegments>();

                            flightSegments = GetBoundData(outBoundAirSegmentList, flightDetailList_Array, singlePricingInfo, flightSegmentsList, airNamespace, out boundLayOverTime, flightContract.ValidatingCarrier);
                            flightContract.TripDetails.OutBoundSegment.AddRange(flightSegments);
                            isOutbound = true;
                            flightContract.TotalOutBoundFlightDuration = boundLayOverTime != null
                               ? boundLayOverTime
                               .Aggregate(TimeSpan.Zero, (sumSoFar, nextTimeSpan) => sumSoFar.Add(nextTimeSpan))
                               : TimeSpan.Zero;
                            int totalstops = flightContract.TripDetails.OutBoundSegment.Count - 1;
                            flightContract.TripDetails.OutBoundSegment.ForEach(x => x.NoOfStops = totalstops);
                            foreach (var segment in flightContract.TripDetails.OutBoundSegment)
                                flightContract.FlightDuration = flightContract.FlightDuration.Add((TimeSpan)segment.FlightDuration);
                            flightContract.Origin = flightContract.TripDetails.OutBoundSegment.FirstOrDefault().Origin;
                            flightContract.DepartureDate = flightContract.TripDetails.OutBoundSegment.FirstOrDefault().DepartureDate;

                            flightContract.Destination = flightContract.TripDetails.OutBoundSegment.LastOrDefault().Destination;

                        }

                        if (inBoundAirSegmentList.Any())
                        {
                            List<TimeSpan> boundLayOverTime = new List<TimeSpan>();
                            flightContract.TripDetails.InBoundSegment = new List<FlightSegments>();
                            List<FlightSegments> flightSegments = new List<FlightSegments>();
                            flightSegments = GetBoundData(inBoundAirSegmentList, flightDetailList_Array, singlePricingInfo, flightSegmentsList, airNamespace, out boundLayOverTime, flightContract.ValidatingCarrier);
                            flightContract.TripDetails.InBoundSegment.AddRange(flightSegments);
                            isInBound = true;
                            flightContract.TotalInBoundFlightDuration = boundLayOverTime != null
                               ? boundLayOverTime
                               .Aggregate(TimeSpan.Zero, (sumSoFar, nextTimeSpan) => sumSoFar.Add(nextTimeSpan))
                               : TimeSpan.Zero;
                            int totalstops = flightContract.TripDetails.InBoundSegment.Count - 1;
                            flightContract.TripDetails.InBoundSegment.ForEach(x => x.NoOfStops = totalstops);
                            foreach (var segment in flightContract.TripDetails.InBoundSegment)
                                flightContract.FlightDuration = flightContract.FlightDuration.Add((TimeSpan)segment.FlightDuration);
                            flightContract.ArrivalDate = flightContract.TripDetails.InBoundSegment.LastOrDefault().ArrivalDate;
                        }
                        flightContract.CompleteTimeDuration = flightContract.TotalOutBoundFlightDuration
                            .Add(flightContract.TotalInBoundFlightDuration);

                        var Cat35Indicator = singlePricingInfo.Attribute("Cat35Indicator") != null
                            ? singlePricingInfo.Attribute("Cat35Indicator").Value
                            : "";
                        //var ETicketability = singlePricingInfo.Attribute("ETicketability") != null ? singlePricingInfo.Attribute("ETicketability").Value : "";
                        flightContract.lastTicketDate = singlePricingInfo.Attribute("LatestTicketingTime") != null ?
                            (GetDate(singlePricingInfo.Attribute("LatestTicketingTime").Value)).ToString()
                            : "";

                        flightContract.apigds_provider = singlePricingInfo.Attribute("ProviderCode") != null ? singlePricingInfo.Attribute("ProviderCode").Value : "";

                        //var Refundable = priceInfo.Attribute("Refundable") != null ? Convert.ToBoolean(priceInfo.Attribute("Refundable").Value) : false;
                        flightContract.rules.refundPenalty = singlePricingInfo.Attribute(airNamespace + "Refundable") != null ? (singlePricingInfo.Attribute(airNamespace + "Refundable").Value) : "false";
                        var ChangePenalty = singlePricingInfo.Descendants(airNamespace + "ChangePenalty").Any()
                            ? new List<string>()
                            : null;

                        if (ChangePenalty != null)
                        {
                            IEnumerable<XElement> xChangePenalties = singlePricingInfo.Descendants(airNamespace + "ChangePenalty");
                            foreach (XElement xChangePenalty in xChangePenalties)
                            {
                                XElement xAmount = xChangePenalty.Element(airNamespace + "Amount");
                                if (xAmount == null)
                                {
                                    xAmount = xChangePenalty.Element(airNamespace + "Percentage");
                                }
                                if (xAmount != null)
                                {

                                    ChangePenalty.Add(xAmount.Value);
                                }
                            }
                            flightContract.rules.changePenalty = ChangePenalty.FirstOrDefault() + "%";
                        }
                        var CancelPenalty = singlePricingInfo.Descendants(airNamespace + "CancelPenalty").Any()
                            ? new List<string>()
                            : null;

                        if (CancelPenalty != null)
                        {
                            IEnumerable<XElement> xCancelPenalties = singlePricingInfo.Descendants(airNamespace + "CancelPenalty");
                            foreach (XElement xCancelPenalty in xCancelPenalties)
                            {
                                XElement xAmount = xCancelPenalty.Element(airNamespace + "Amount");
                                if (xAmount == null)
                                {
                                    xAmount = xCancelPenalty.Element(airNamespace + "Percentage");
                                }
                                if (xAmount != null)
                                {
                                    CancelPenalty.Add(xAmount.Value);
                                }
                            }
                            flightContract.rules.refundPenalty = ChangePenalty.FirstOrDefault() + "%";
                        }

                        if (airPricingSolution.Descendants(airNamespace + "AirPricingInfo").Any())
                        {
                            #region Specific Flight/ AirPricing Related KeyList Intialization
                            List<string> fareInfoRef = new List<string>();
                            List<string> brandRef = new List<string>();
                            #endregion

                            /*
                             * Passenger Based Fare info in AirPricingInfo
                             * Seperate AirPricing for each PaxType
                             */
                            foreach (var priceInfo in airPricingSolution.Descendants(airNamespace + "AirPricingInfo"))
                            {
                                #region Getting FareInfo Refrence Key and FareInfo XElement
                                fareInfoRef = priceInfo.Descendants(airNamespace + "FareInfoRef").Any()
                                    ? (from segment in priceInfo.Descendants(airNamespace + "FareInfoRef")
                                       select segment.Attributes("Key").ElementAtOrDefault(0).Value).ToList()
                                    : null;
                                foreach (var fareinfoKey in fareInfoRef)
                                {
                                    xfareInfoList.AddRange(from fareinfo in fareInfoList_Array.Descendants(airNamespace + "FareInfo")
                                                           where fareinfo.Attributes("Key").ElementAtOrDefault(0).Value == fareinfoKey
                                                           select fareinfo);
                                }
                                //foreach (var fareInfo in xfareInfoList)
                                //    gdsResponseXML.Append(fareInfo);
                                #endregion

                                #region Getting Brand Key Reference and Brand XElement
                                brandRef = (from brand in xfareInfoList.Descendants(airNamespace + "Brand")
                                            where brand.Attributes("BrandID").Any()
                                            select brand.Attributes("BrandID").ElementAtOrDefault(0).Value).ToList();
                                if (brandList_Array.Any())
                                {
                                    foreach (var brandKey in brandRef)
                                    {
                                        xbrandList.AddRange(brandList_Array.Any()
                                            ? (from brand in brandList_Array.Descendants(airNamespace + "Brand")
                                               where brand.Attributes("BrandID").ElementAtOrDefault(0).Value == brandKey
                                               select brand).ToList()
                                            : null);
                                    }
                                    //foreach (var brand in xbrandList)
                                    //    gdsResponseXML.Append(brand);
                                    //gdsResponseXML.Append("</TravelportResponse>");
                                }
                                #endregion

                                #region Identifying FareType
                                var pricingSource = (from fareinfo in xfareInfoList
                                                     where fareinfo.Attributes("PrivateFare").Any()
                                                     select fareinfo).ToList();
                                flightContract.PricingSource = pricingSource != null && pricingSource.Any() ? FareType.PRIVATE.ToString() : FareType.PUBLISHED.ToString();
                                #endregion

                                {
                                    bool validContract = false;
                                    if (isOutbound)
                                    {
                                        flightContract.TripDetails.OutBoundSegment
                                            .ForEach(x => x.FareBasis = string.Join("_", (from fareBasisCode in xfareInfoList
                                                                                          select fareBasisCode.Attributes("FareBasis").ElementAtOrDefault(0).Value)));
                                        fareBasisCodes.AddRange(flightContract.TripDetails.OutBoundSegment.Select(x => x.FareBasis));
                                        validContract = CheckForValidContractBasedOnAirline(flightContract.PricingSource, detail, flightContract.TripDetails.OutBoundSegment);
                                        if (!validContract)
                                            return null;
                                    }
                                    if (isInBound)
                                    {
                                        flightContract.TripDetails.InBoundSegment
                                            .ForEach(x => x.FareBasis = string.Join("_", (from fareBasisCode in xfareInfoList
                                                                                          select fareBasisCode.Attributes("FareBasis").ElementAtOrDefault(0).Value)));
                                        fareBasisCodes.AddRange(flightContract.TripDetails.InBoundSegment.Select(x => x.FareBasis));
                                        validContract = CheckForValidContractBasedOnAirline(flightContract.PricingSource, detail, flightContract.TripDetails.InBoundSegment);
                                        if (!validContract)
                                            return null;
                                    }
                                    flightContract.FareBasisCode = string.Join("_", fareBasisCodes.Distinct());
                                }

                                #region Individual Passenger Fare Parsing
                                FareDetails passengerFareDetail = new FareDetails();
                                var passengerType = priceInfo.Element(airNamespace + "PassengerType") != null
                                    ? priceInfo.Element(airNamespace + "PassengerType")
                                    : null;
                                passengerFareDetail.PaxType = passengerType != null && passengerType.Attribute("Code") != null
                                    ? passengerType.Attribute("Code").Value
                                    : null;

                                passengerFareDetail.ListOfBookingInfo = priceInfo.Descendants(airNamespace + "BookingInfo").Any()
                                    ? (from bookingInfo in priceInfo.Descendants(airNamespace + "BookingInfo")
                                       select new BookingInfo
                                       {
                                           BookingCode = bookingInfo.Attribute("BookingCode") != null ? bookingInfo.Attribute("BookingCode").Value : "",
                                           BookingCount = bookingInfo.Attribute("BookingCount") != null ? bookingInfo.Attribute("BookingCount").Value : "",
                                           CabinClass = bookingInfo.Attribute("CabinClass") != null ? bookingInfo.Attribute("CabinClass").Value : "",
                                           FareInfoRef = bookingInfo.Attribute("FareInfoRef") != null ? bookingInfo.Attribute("FareInfoRef").Value : "",
                                           SegmentRef = bookingInfo.Attribute("SegmentRef") != null ? bookingInfo.Attribute("SegmentRef").Value : "",
                                           HostTokenRef = bookingInfo.Attribute("HostTokenRef") != null ? bookingInfo.Attribute("HostTokenRef").Value : "",
                                           FareInfoValue = priceInfo.Descendants("FareRuleKey").Any() ? priceInfo.Descendants("FareRuleKey").ElementAtOrDefault(0).Value : ""
                                       }).ToList()
                                    : null;

                                var minSeats = 10;
                                foreach (var segment in flightContract.TripDetails.OutBoundSegment)
                                {
                                    var listOfBookingInfo = passengerFareDetail.ListOfBookingInfo.Where(x => x.SegmentRef == segment.SegmentKey);
                                    if (listOfBookingInfo != null && listOfBookingInfo.Any())
                                    {
                                        segment.AvailableSeats = Convert.ToInt32(listOfBookingInfo.Min(x => x.BookingCount));
                                        if (minSeats > segment.AvailableSeats)
                                        {
                                            minSeats = segment.AvailableSeats;
                                        }
                                    }
                                }

                                if (flightContract.TripDetails.InBoundSegment != null && flightContract.TripDetails.InBoundSegment.Any())
                                {
                                    foreach (var segment in flightContract.TripDetails.InBoundSegment)
                                    {
                                        var listOfBookingInfo = passengerFareDetail.ListOfBookingInfo.Where(x => x.SegmentRef == segment.SegmentKey);
                                        if (listOfBookingInfo != null && listOfBookingInfo.Any())
                                        {
                                            segment.AvailableSeats = Convert.ToInt32(listOfBookingInfo.Min(x => x.BookingCount));
                                            if (minSeats > segment.AvailableSeats)
                                            {
                                                minSeats = segment.AvailableSeats;
                                            }
                                        }
                                    }
                                }
                                flightContract.MinSeatAvailableForContract = minSeats;

                                passengerFareDetail.FareInfo = xfareInfoList.Any()
                                    ? (from fare in xfareInfoList
                                       let FareRule = fare.Descendants(airNamespace + "FareRuleKey")
                                       select new FareInfoRule
                                       {
                                           Destination = fare.Attribute("Destination") != null ? fare.Attribute("Destination").Value : "",
                                           FareAmount = fare.Attribute("Amount") != null ? fare.Attribute("Amount").Value : "",
                                           Origin = fare.Attribute("Origin") != null ? fare.Attribute("Origin").Value : "",
                                           ProviderCode = FareRule.Any() ? FareRule.ElementAt(0).Attribute("ProviderCode") != null ? FareRule.ElementAt(0).Attribute("ProviderCode").Value : "" : "",//
                                           FareInfoValue = FareRule.Any() ? FareRule.ElementAtOrDefault(0).Value : "",
                                           FareRefKey = fare.Attribute("Key") != null ? fare.Attribute("Key").Value : "",
                                           FareBasis = fare.Attribute("FareBasis") != null ? fare.Attribute("FareBasis").Value : "",
                                           FareEffectiveDate = fare.Attribute("EffectiveDate") != null ? GetDate(fare.Attribute("EffectiveDate").Value) : new DateTime()
                                       }).ToList()
                                     : null;

                                var TaxList = priceInfo.Descendants(airNamespace + "TaxInfo").Any()
                                    ? (from tax in priceInfo.Descendants(airNamespace + "TaxInfo")
                                       select new //Tax
                                       {
                                           Amount = tax.Attribute("Amount").Value,
                                           Category = tax.Attribute("Category").Value
                                       }).ToList()
                                    : null;

                                passengerFareDetail.AirPricingInfoKey = priceInfo.Attribute("Key") != null ? priceInfo.Attributes("Key").ElementAtOrDefault(0).Value : "";
                                var passengerApproximateBasePriceWithCurrency = priceInfo.Attribute("ApproximateBasePrice") != null
                                    ? GrabAmount(priceInfo.Attribute("ApproximateBasePrice").Value)
                                    : "";
                                var passengerBasePriceWithCurrency = priceInfo.Attribute("BasePrice") != null
                                    ? GrabAmount(priceInfo.Attribute("BasePrice").Value)
                                    : "";
                                passengerFareDetail.BaseFare = !string.IsNullOrEmpty(passengerApproximateBasePriceWithCurrency)
                                    ? float.Parse(passengerApproximateBasePriceWithCurrency)
                                    : 0.0f;
                                var passengerApproximateTaxesWithCurrency = priceInfo.Attribute("ApproximateTaxes") != null
                                    ? GrabAmount(priceInfo.Attribute("ApproximateTaxes").Value)
                                    : "";
                                var passengerTaxesWithCurrency = priceInfo.Attribute("Taxes") != null
                                    ? GrabAmount(priceInfo.Attribute("Taxes").Value)
                                    : "";
                                passengerFareDetail.Tax = !string.IsNullOrEmpty(passengerApproximateTaxesWithCurrency)
                                    ? float.Parse(passengerApproximateTaxesWithCurrency)
                                    : 0.0f;
                                var passengerApproximateTotalPriceWithCurrency = priceInfo.Attribute("ApproximateTotalPrice") != null
                                    ? GrabAmount(priceInfo.Attribute("ApproximateTotalPrice").Value)
                                    : "";
                                var passengerTotalPriceWithCurrency = priceInfo.Attribute("TotalPrice") != null
                                    ? GrabAmount(priceInfo.Attribute("TotalPrice").Value)
                                    : "";
                                passengerFareDetail.TotalFare = !string.IsNullOrEmpty(passengerApproximateTotalPriceWithCurrency)
                                    ? float.Parse(passengerApproximateTotalPriceWithCurrency)
                                    : 0.0f;

                                passengerFareDetail.CurrencyCode = priceInfo.Attribute("passengerApproximateTotalPriceWithCurrency") != null
                                                                && !string.IsNullOrEmpty(priceInfo.Attribute("passengerApproximateTotalPriceWithCurrency").Value)
                                    ? Regex.Replace(priceInfo.Attribute("passengerApproximateTotalPriceWithCurrency").Value, @"\d+(\.\d+)?", string.Empty)
                                    : "";
                                #endregion

                                var PricingMethod = priceInfo.Attribute("PricingMethod") != null ? priceInfo.Attribute("PricingMethod").Value : "";
                                passengerFareDetail.ProviderCode = priceInfo.Attribute("ProviderCode") != null ? priceInfo.Attribute("ProviderCode").Value : "";

                                #region Passenger Identification and Fare Assignment
                                if (passengerFareDetail != null && passengerFareDetail.PaxType != null)
                                {
                                    switch (passengerFareDetail.PaxType)
                                    {
                                        case "ADT":
                                            passengerFareDetail.passengerCount = flightSearchDetails.AdultCount;
                                            flightContract.AdultFare = passengerFareDetail;
                                            break;
                                        case "CHD":
                                        case "CNN":
                                            passengerFareDetail.passengerCount = flightSearchDetails.ChildCount + flightSearchDetails.Child12To17Count;
                                            flightContract.ChildFare = passengerFareDetail;
                                            break;
                                        case "INS":
                                            passengerFareDetail.passengerCount = flightSearchDetails.InfantCount;
                                            flightContract.InfantFare = passengerFareDetail;
                                            break;
                                        case "INF":
                                            passengerFareDetail.passengerCount = flightSearchDetails.InfantInLapCount;
                                            flightContract.InfantInLapFare = passengerFareDetail;
                                            break;
                                        case "SEN":
                                            passengerFareDetail.passengerCount = flightSearchDetails.SeniorCount;
                                            flightContract.SeniorFare = passengerFareDetail;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                #endregion
                            }

                        }


                        flightContract.TotalGDSFareV2 = flightContract.TotalBaseFare + flightContract.TotalMarkup + flightContract.TotalTax + flightContract.TotalSupplierFee;
                        flightContract.TotalGDSQuotedAmount = flightContract.TotalGDSFareV2;

                        #region Contract Key And Date key Preparation
                        flightContract.Contractkey = UtilityMethods.PrepareContractKey(flightContract.TripDetails);
                        flightContract.DatesKey = UtilityMethods.PrepareDatesKey(flightContract);
                        #endregion

                        //flightContract.GDSResponseXML = gdsResponseXML.ToString();
                        flightContracts.Add(flightContract);

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"{ex.Message} {ex.StackTrace}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return flightContracts;
        }

        #region Travelport Helper Methods

        #region Parsing Methods
        private List<Connection> GetConnectionList(XElement airPricingSolution, XNamespace airNamespace)
        {
            List<Connection> connectionList = new List<Connection>();
            try
            {
                var segmentIndex = airPricingSolution.Descendants(airNamespace + "Connection");
                if (segmentIndex.Any())
                {
                    foreach (var index in segmentIndex)
                    {
                        Connection connection = new Connection
                        {
                            SegmentIndex = index.Attribute("SegmentIndex") == null ? "" : index.Attribute("SegmentIndex").Value,
                            StopOver = index.Attribute("StopOver") == null ? false : Convert.ToBoolean(index.Attribute("StopOver").Value)
                        };
                        connectionList.Add(connection);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return connectionList;
        }
        private List<HostToken> GetHostTokenList(XElement airPricingSolution, XNamespace airNamespace)
        {
            List<HostToken> hostTokenList = new List<HostToken>();
            try
            {
                var hostTokenXml = airPricingSolution.Descendants(airNamespace + "HostToken");
                if (hostTokenXml.Any())
                {
                    hostTokenList = (from token in hostTokenXml
                                     select new HostToken
                                     {
                                         HostTokenKey = token.Attribute("Key") != null ? token.Attribute("Key").Value : "",
                                         HostTokenValue = token.Value
                                     }).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return hostTokenList;
        }
        private Dictionary<string, FlightSegments> GetListOfSegment(XElement[] airSegmentList_Array, XNamespace airNamespace)
        {
            Dictionary<string, FlightSegments> flightSegmentsList = new Dictionary<string, FlightSegments>();
            try
            {
                foreach (var segment in airSegmentList_Array)
                {
                    try
                    {
                        FlightSegments flightSegment = new FlightSegments();

                        var fltdtlRef = segment.Descendants(airNamespace + "FlightDetailsRef").Any()
                            ? segment.Descendants(airNamespace + "FlightDetailsRef")
                            : null;

                        var flightRef = fltdtlRef != null
                            ? fltdtlRef.ElementAtOrDefault(0)
                            : null;

                        var FlightDetailsRef = flightRef != null && flightRef.Attribute("Key") != null
                            ? flightRef.Attribute("Key").Value
                            : null;
                        //segment.AirSegmentKey = seg.Attribute("Key") != null ? seg.Attribute("Key").Value : "";
                        flightSegment.SegmentKey = segment.Attribute("Key") != null
                            ? segment.Attribute("Key").Value
                            : "";
                        DateTime ArrivalTime = segment.Attribute("ArrivalTime") != null
                            ? GetDate(segment.Attribute("ArrivalTime").Value)
                            : new DateTime(); //ArrivalTime
                        flightSegment.ArrivalDate = ArrivalTime.Date;
                        flightSegment.ArrivalTime = new TimeSpan(ArrivalTime.Hour, ArrivalTime.Minute, ArrivalTime.Second);
                        //segment.Carrier = seg.Attribute("Carrier") != null ? seg.Attribute("Carrier").Value : "";
                        flightSegment.AirEquipmentTypeCode = segment.Attribute("Carrier") != null
                            ? segment.Attribute("Carrier").Value
                            : "";
                        //segment.ChangeOfPlane = seg.Attribute("ChangeOfPlane") != null ? seg.Attribute("ChangeOfPlane").Value : "";
                        DateTime DepartureTime = segment.Attribute("DepartureTime") != null
                            ? GetDate(segment.Attribute("DepartureTime").Value)
                            : new DateTime();//DepartureTime
                        flightSegment.DepartureDate = DepartureTime.Date;
                        flightSegment.DepartureTime = new TimeSpan(DepartureTime.Hour, DepartureTime.Minute, DepartureTime.Second);
                        flightSegment.Destination = segment.Attribute("Destination") != null
                            ? segment.Attribute("Destination").Value
                            : "";
                        //var destinationAirport = _configurationData.AllAirports.FirstOrDefault(x => x.Code == flightSegment.Destination);
                        //flightSegment.DestinationCity = destinationAirport == null
                        //    ? flightSegment.Destination
                        //    : destinationAirport.CityName;
                        flightSegment.Distance = segment.Attribute("Distance") != null
                            ? segment.Attribute("Distance").Value
                            : "";
                        flightSegment.EquipmentType = segment.Attribute("Equipment") != null
                            ? segment.Attribute("Equipment").Value
                            : "";
                        flightSegment.FlightNumber = segment.Attribute("FlightNumber") != null
                            ? segment.Attribute("FlightNumber").Value
                            : "";
                        //segment.FlightTime = seg.Attribute("FlightTime") != null ? seg.Attribute("FlightTime").Value : "";
                        var flightDuration = segment.Attribute("FlightTime") != null
                            ? (segment.Attribute("FlightTime").Value)
                            : "";
                        flightSegment.FlightDuration = TimeSpan.FromMinutes(Double.Parse(flightDuration));
                        //segment.Group = seg.Attribute("Group") != null ? Convert.ToInt32(seg.Attribute("Group").Value) : 0;
                        flightSegment.MarriageGroup = segment.Attribute("Group") != null
                            ? (segment.Attribute("Group").Value)
                            : "";
                        flightSegment.Origin = segment.Attribute("Origin") != null
                            ? segment.Attribute("Origin").Value
                            : "";
                        //var originAirport = _configurationData.AllAirports.FirstOrDefault(x => x.Code == flightSegment.Origin);
                        //flightSegment.OriginCity = originAirport == null
                        //    ? flightSegment.Origin
                        //    : originAirport.CityName;
                        flightSegment.ProviderCode = segment.Attribute("ProviderCode") != null
                            ? segment.Attribute("ProviderCode").Value
                            : "";
                        flightSegment.ProviderCode = (string.IsNullOrEmpty(flightSegment.ProviderCode) && segment.Descendants(airNamespace + "AirAvailInfo").Any())
                            ? segment.Descendants(airNamespace + "AirAvailInfo").ElementAtOrDefault(0).Attribute("ProviderCode").Value
                            : flightSegment.ProviderCode;

                        flightSegment.AvailabilitySource = segment.Attribute("AvailabilitySource") != null
                            ? segment.Attribute("AvailabilitySource").Value
                            : "";

                        flightSegment.ETicketability = segment.Attribute("ETicketability") != null ? segment.Attribute("ETicketability").Value : "";
                        flightSegment.ChangeOfPlane = segment.Attribute("ChangeOfPlane") != null ? segment.Attribute("ChangeOfPlane").Value : "";
                        flightSegment.ParticipantLevel = segment.Attribute("ParticipantLevel") != null ? segment.Attribute("ParticipantLevel").Value : "";
                        flightSegment.LinkAvailability = segment.Attribute("LinkAvailability") != null ? segment.Attribute("LinkAvailability").Value : "";
                        flightSegment.PolledAvailabilityOption = segment.Attribute("PolledAvailabilityOption") != null ? segment.Attribute("PolledAvailabilityOption").Value : "";
                        flightSegment.OptionalServicesIndicator = segment.Attribute("OptionalServicesIndicator") != null ? segment.Attribute("OptionalServicesIndicator").Value : "";
                        flightSegment.AvailabilityDisplayType = segment.Attribute("AvailabilityDisplayType") != null ? segment.Attribute("AvailabilityDisplayType").Value : "";

                        if (segment.Descendants(airNamespace + "CodeshareInfo").Any())
                        {
                            var operatingCarrier = segment.Descendants(airNamespace + "CodeshareInfo").First().Value;
                            var airlineName_string = operatingCarrier != null
                                ? operatingCarrier.ToString().Split(" ")
                                : null;
                            var airlineCode = airlineName_string != null
                                ? airlineName_string[0]
                                : null;
                            /*var airlineCode = operatingCarrier.Attribute("OperatingCarrier") != null
                                    ? operatingCarrier.Attribute("OperatingCarrier").Value
                                    : "";*/
                            if (airlineCode != null)
                            {
                                var airline = (from keyValue in _configurationData.AllAirlines
                                               let name = keyValue.Value.Name
                                               where name.ToLower().Contains(airlineCode.ToLower())
                                               select keyValue).FirstOrDefault();
                                flightSegment.OperatingCarrier = new Models.Response.Airline
                                {
                                    AirlineCode = airlineCode,
                                };
                            }
                            //segment.OperatingCarrier.AirLineNumber = operatingCarrier.Attribute("OperatingFlightNumber") != null ? operatingCarrier.Attribute("OperatingFlightNumber").Value : "";
                        }
                        else
                        {
                            
                            flightSegment.OperatingCarrier = new Models.Response.Airline
                            {
                                AirlineCode = segment.Attribute("Carrier") != null ? segment.Attribute("Carrier").Value : "",

                                //AirlineName = airline.Value == null ? airlineCode : airline.Value.Name,
                                // IsLowCostCarrierAirline = airline.Value.IsLCC
                            };
                            //if (!string.IsNullOrEmpty(segment.OperatingCarrier.AirlineCode))
                            //    segment.OperatingCarrier.AirlineName = MasterDataSets.AirLineList.Any(x => x.AirlineCode == segment.OperatingCarrier.AirlineCode) ? MasterDataSets.AirLineList.Where(dr => dr.AirlineCode.ToUpper() == segment.OperatingCarrier.AirlineCode.ToUpper()).FirstOrDefault().AirlineName : string.Empty;
                        }
                        if (!flightSegmentsList.Keys.Any(x => x == FlightDetailsRef))
                        {
                            flightSegmentsList.Add(FlightDetailsRef, flightSegment);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"{ex.Message} {ex.StackTrace}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return flightSegmentsList;
        }
        private void GetCabinDetail(XElement singlePricingInfo, string airSegmnetKey, XNamespace airNameSpace, ref FlightSegments flightSegment)
        {
            try
            {
                flightSegment.Cabin = singlePricingInfo.Descendants(airNameSpace + "BookingInfo") != null
                                       && singlePricingInfo.Descendants(airNameSpace + "BookingInfo").Attributes("CabinClass").Any()
                                      ? (from bookingInfo in singlePricingInfo.Descendants(airNameSpace + "BookingInfo")
                                         where bookingInfo.Attributes("SegmentRef").ElementAtOrDefault(0).Value == airSegmnetKey
                                         select bookingInfo.Attributes("CabinClass").ElementAtOrDefault(0).Value).FirstOrDefault()
                                      : null;
                CabinClass CabinClass = (CabinClass)Enum.Parse(typeof(CabinClass), flightSegment.Cabin);
                flightSegment.Class = GetCabinType(CabinClass);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
        }
        private List<FlightSegments> GetBoundData(List<XElement> boundAirSegmentList, XElement[] flightDetailList, XElement singlePricingInfo, Dictionary<string, FlightSegments> flightSegmentList, XNamespace airNameSpace, out List<TimeSpan> boundLayOverTimeList, Models.Response.Airline validatingCarrier)
        {
            List<FlightSegments> flightSegments = new List<FlightSegments>();
            boundLayOverTimeList = new List<TimeSpan>();
            try
            {
                foreach (XElement airSegment in boundAirSegmentList)
                {
                    var airSegmnetKey = airSegment.Attributes("Key").ElementAtOrDefault(0).Value;
                    var flightDetailRefKey = (from flightKey in airSegment.Descendants(airNameSpace + "FlightDetailsRef")
                                              select flightKey.Attributes("Key").ElementAtOrDefault(0).Value).ToList();
                    int segIndex = 0;
                    foreach (string key in flightDetailRefKey)
                    {
                        var segment = flightSegmentList[key];
                        segment.Id = segment.SegmentID = segIndex;
                        segIndex++;
                        segment.MarketingCarrier = new Models.Response.Airline()
                        {
                            AirlineCode = airSegment.Attributes("Carrier") != null
                            ? airSegment.Attributes("Carrier").ElementAtOrDefault(0).Value
                            : null,
                           
                        };
                        segment.ValidatingCarrier = validatingCarrier;
                        GetCabinDetail(singlePricingInfo, airSegmnetKey, airNameSpace, ref segment);
                        flightSegments.Add(segment);
                        var travelTime = (from flightDetail in flightDetailList.Descendants(airNameSpace + "FlightDetails")
                                          where flightDetail.Attributes("Key").ElementAtOrDefault(0).Value == key
                                          select flightDetail.Attributes("TravelTime").ElementAtOrDefault(0).Value)
                                          .FirstOrDefault();
                        if (!string.IsNullOrEmpty(travelTime))
                        {
                            TimeSpan calcTravelTime = new TimeSpan(0, Convert.ToInt32(travelTime), 0);
                            boundLayOverTimeList.Add(calcTravelTime);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return flightSegments;
        }
        #endregion

        #region Parser Helper Methods
        private DateTime GetDate(string dateToParse, int year = -1)
        {
            try
            {
                if (!string.IsNullOrEmpty(dateToParse))
                {
                    if (year == -1)
                    {
                        year = DateTime.Now.Year;
                    }
                    int yearValue = 0;
                    int monthValue = 0;
                    int dayValue = 0;
                    int hourValue = 0;
                    int minuteValue = 0;

                    string[] dateAndTimeArray = dateToParse.Split('T');
                    if (dateAndTimeArray.Length > 1)
                    {
                        string[] dateArray = dateAndTimeArray.ElementAt(0).Split('-');
                        string[] timeArray = dateAndTimeArray.ElementAt(1).Split(':');

                        if (dateArray.Length > 2)
                        {
                            yearValue = int.Parse(dateArray.ElementAt(0));
                            monthValue = int.Parse(dateArray.ElementAt(1));
                            dayValue = int.Parse(dateArray.ElementAt(2));
                        }
                        if (timeArray.Length > 1)
                        {
                            hourValue = int.Parse(timeArray.ElementAt(0));
                            minuteValue = int.Parse(timeArray.ElementAt(1));
                        }
                    }
                    else
                    {
                        string[] dateArray = dateAndTimeArray.ElementAt(0).Split('-');
                        if (dateArray.Length > 2)
                        {
                            yearValue = int.Parse(dateArray.ElementAt(0));
                            monthValue = int.Parse(dateArray.ElementAt(1));
                            dayValue = int.Parse(dateArray.ElementAt(2));
                        }
                    }
                    return new DateTime(yearValue, monthValue, dayValue, hourValue, minuteValue, 0);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return new DateTime();
        }
        private XElement GetClearObject(XElement xDoc)
        {
            try
            {
                foreach (XElement XE in xDoc.DescendantsAndSelf())
                {
                    XE.Name = XE.Name.LocalName;
                    XE.ReplaceAttributes((from xattrib in XE.Attributes().Where(xa => !xa.IsNamespaceDeclaration) select new XAttribute(xattrib.Name.LocalName, xattrib.Value)));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return xDoc;
        }
        private float CalculateAmount(FlightContracts flightContract, int paxCount, PaxType paxType, string caseType)
        {
            float calculatedAmount = 0.0f;
            try
            {
                if (paxCount > 0)
                {
                    FareDetails fareDetails = new FareDetails();
                    switch (paxType)
                    {
                        case PaxType.ADT:
                            fareDetails = flightContract.AdultFare;
                            break;
                        case PaxType.CNN:
                            fareDetails = flightContract.ChildFare;
                            break;
                        case PaxType.INS:
                            fareDetails = flightContract.InfantFare;
                            break;
                        case PaxType.INF:
                            fareDetails = flightContract.InfantInLapFare;
                            break;
                        case PaxType.SEN:
                            fareDetails = flightContract.SeniorFare;
                            break;
                        default:
                            break;
                    }

                    switch (caseType.ToUpper())
                    {
                        case "BASEFARE":
                            calculatedAmount = paxCount * fareDetails.BaseFare;
                            break;
                        case "TAX":
                            calculatedAmount = paxCount * fareDetails.Tax;
                            break;
                        default:
                            break;
                    }


                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return calculatedAmount;
        }
        private string GrabAmount(string price)
        {
            try
            {
                return Regex.Replace(price, @"[aA-zZ]", string.Empty);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return null;
        }
        private bool CheckForValidContractBasedOnAirline(string pricingSource, ProviderDetail detail, List<FlightSegments> flightSegmentsList)
        {
            bool validContract = false;
            try
            {
                string[] excludedAirlines = null;

                switch (pricingSource)
                {
                    case "PUBLISHED":
                        excludedAirlines = string.IsNullOrWhiteSpace(detail.ExcludedAirlinesForPublishFares)
                            ? null
                            : detail.ExcludedAirlinesForPublishFares.Split(',');
                        break;
                    case "PRIVATE":
                        excludedAirlines = string.IsNullOrWhiteSpace(detail.ExcludedAirlinesForPrivateFares)
                            ? null
                            : detail.ExcludedAirlinesForPrivateFares.Split(',');
                        break;
                    default:
                        excludedAirlines = null;
                        break;
                }

                if (excludedAirlines == null)
                    return true;

                validContract = (from Segment in flightSegmentsList
                                 let oC = Segment.OperatingCarrier
                                 let mC = Segment.OperatingCarrier
                                 let vC = Segment.OperatingCarrier
                                 where oC != null && excludedAirlines.Contains(oC.AirlineCode)
                                        || mC != null && excludedAirlines.Contains(mC.AirlineCode)
                                        || vC != null && excludedAirlines.Contains(vC.AirlineCode)
                                 select Segment)
                                 .ToList()
                                 .Any()
                                    ? false
                                    : true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return validContract;
        }
        private string GetCabinType(CabinClass cabinType)
        {
            string cabin = string.Empty;
            try
            {
                switch (cabinType)
                {
                    case CabinClass.First:
                        cabin = "F";
                        break;

                    case CabinClass.PremiumBusiness:
                        cabin = "J";
                        break;

                    case CabinClass.Business:
                        cabin = "C";
                        break;

                    case CabinClass.PremiumEconomy:
                        cabin = "S";
                        break;

                    case CabinClass.Economy:
                    case CabinClass.None:
                    case CabinClass.EconomyCoach:
                    default:
                        cabin = "Y";
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return cabin;
        }
        #endregion

        #endregion
    }
}
