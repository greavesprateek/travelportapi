﻿using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Flight.TravelAPI.Parsers.Travelport
{
    public class RetrivePNRParser
    {
        private readonly IConfigurationData _configurationData;
        private readonly ILogger<Parser> _logger;
        private XNamespace airNamespace = "http://www.travelport.com/schema/air_v50_0";
        private XNamespace commonNamespace = "http://www.travelport.com/schema/common_v50_0";
        private XNamespace soapNamespace = "http://schemas.xmlsoap.org/soap/envelope/";
        private XNamespace universal = "http://www.travelport.com/schema/universal_v50_0";

        public RetrivePNRParser(ILogger<Parser> logger, IConfigurationData configurationData)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationData = configurationData ?? throw new ArgumentNullException(nameof(configurationData));
        }

        public ContractDetails PNRResponseParse(string response, ProviderDetail detail, string reponseType)
        {
            ContractDetails flightContract = new ContractDetails();

            try
            {
                flightContract.ContractID = flightContract.ItemNumber = 1;
                flightContract.Provider = detail.ProviderEngine;
                //response = XDocument.Load(@"D:\APIWork\TravelportRQRS\2021-10-10\M4R6OA_\Searhing_M4R6OA__08-42-22-473_RS.xml").ToString();
                if (string.IsNullOrEmpty(response))
                {
                    return flightContract;
                }

                XElement xDoc = XElement.Parse(response);
                //xDoc = GetClearObject(xDoc);
                xDoc = xDoc.Descendants(soapNamespace + "Body").FirstOrDefault();

                #region Check For Any Fault/Error
                var fault = xDoc.Descendants("Fault").Any()
                    ? xDoc.Descendants("Fault")
                    : null;

                if (fault != null)
                    return flightContract;

                var errorInfo = xDoc.Descendants(commonNamespace + "ErrorInfo").Any()
                    ? xDoc.Descendants(commonNamespace + "ErrorInfo")
                    : null;

                if (errorInfo != null)
                    return flightContract;

                errorInfo = xDoc.Descendants(commonNamespace + "ResponseMessage").Any()
                    ? xDoc.Descendants(commonNamespace + "ResponseMessage")
                    : null;

                if (errorInfo != null && errorInfo.Any() && errorInfo.Any(x => x.Attribute(commonNamespace + "Code") != null && x.Attribute(commonNamespace + "Code").Value == "2027"))
                {
                    return flightContract;
                }
                #endregion

                #region Reading/Identifying All The Neccessary Elements Of Response XML
                xDoc = xDoc.Descendants(universal + reponseType).FirstOrDefault();

                var xAirReservation = xDoc.Descendants(airNamespace + "AirReservation");
                if (xAirReservation == null || !xAirReservation.Any()) return flightContract;

                var xAirSegmentList = xAirReservation.Descendants(airNamespace + "AirSegment");
                if (xAirSegmentList == null || !xAirSegmentList.Any()) return flightContract;

                var xFareInfoList = xAirReservation.Descendants(airNamespace + "FareInfo");
                //if (xFareInfoList == null || !xFareInfoList.Any()) return flightContract;

                var xBookingTraveller = xDoc.Descendants(commonNamespace + "BookingTraveler");
                if (xBookingTraveller == null || !xBookingTraveller.Any()) return flightContract;

                var xAirPricingInfoList = xDoc.Descendants(airNamespace + "AirPricingInfo");
                //if (xAirPricingInfoList == null || !xAirPricingInfoList.Any()) return flightContract;


                var xBrandList = xDoc.Descendants(airNamespace + "BrandList");

                XElement[] flightDetailList_Array = xAirReservation as XElement[] ?? xAirReservation.ToArray();
                XElement[] airSegmentList_Array = xAirSegmentList as XElement[] ?? xAirSegmentList.ToArray();
                XElement[] fareInfoList_Array = xFareInfoList as XElement[] ?? xFareInfoList.ToArray();
                XElement[] travellerList_Array = xBookingTraveller as XElement[] ?? xBookingTraveller.ToArray();
                XElement[] airPricingInfoList_Array = xAirPricingInfoList as XElement[] ?? xAirPricingInfoList.ToArray();
                XElement[] brandList_Array = xBrandList as XElement[] ?? xBrandList.ToArray();
                #endregion

                #region Traveller Infos
                List<TravellerDetails> travellerDetails = GetTravellerDetails(travellerList_Array);
                #endregion


                Dictionary<string, FlightSegments> flightSegmentsList = new Dictionary<string, FlightSegments>();
                flightSegmentsList = GetListOfSegment(airSegmentList_Array, airNamespace);
                var airPricingInfo = airPricingInfoList_Array.Any() ? airPricingInfoList_Array.First() : null;

                List<string> fareBasisCodes = new List<string>();
                bool isOutbound = false;
                bool isInBound = false;

                #region Specific Flight/ AirPricing Related XElement Intialization
                List<FlightSegments> xflightDetailList = new List<FlightSegments>();
                List<XElement> xSegmentList = new List<XElement>();

                List<XElement> xfareInfoList = new List<XElement>();
                List<XElement> xbrandList = new List<XElement>();

                #endregion
                flightContract.UniversalLocatorCode = xDoc.Descendants(universal + "UniversalRecord").Any()
                     ? (from p in xDoc.Descendants(universal + "UniversalRecord")
                        select p.Attributes("LocatorCode").ElementAtOrDefault(0).Value).FirstOrDefault()
                        : "";

                var providerRef = xDoc.Descendants(universal + "ProviderReservationInfo").FirstOrDefault();
                if (providerRef != null)
                {
                    flightContract.PNR = providerRef.Attribute("LocatorCode") != null
                        ? providerRef.Attribute("LocatorCode").Value
                        : "";
                    flightContract.PCC = providerRef.Attribute("OwningPCC") != null
                        ? providerRef.Attribute("OwningPCC").Value
                        : "";
                }

                string platingCarrier = string.Empty;
                if (airPricingInfo != null)
                {
                    flightContract.AirPricingKey = airPricingInfo.Attribute("Key") != null ? airPricingInfo.Attribute("Key").Value : "";

                    var ApproximateBasePriceWithCurrency = airPricingInfo.Attribute("ApproximateBasePrice") != null
                        ? GrabAmount(airPricingInfo.Attribute("ApproximateBasePrice").Value)
                        : "";
                    var BasePriceWithCurrency = airPricingInfo.Attribute("BasePrice") != null
                        ? GrabAmount(airPricingInfo.Attribute("BasePrice").Value)
                        : "";
                    flightContract.TotalBaseFare = !string.IsNullOrEmpty(ApproximateBasePriceWithCurrency)
                        ? float.Parse(ApproximateBasePriceWithCurrency)
                        : 0.0f;
                    var ApproximateTaxesWithCurrency = airPricingInfo.Attribute("ApproximateTaxes") != null
                        ? GrabAmount(airPricingInfo.Attribute("ApproximateTaxes").Value)
                        : "";
                    var TaxesWithCurrency = airPricingInfo.Attribute("Taxes") != null
                        ? GrabAmount(airPricingInfo.Attribute("Taxes").Value)
                        : "";
                    flightContract.TotalTax = !string.IsNullOrEmpty(ApproximateTaxesWithCurrency)
                        ? float.Parse(ApproximateTaxesWithCurrency)
                        : 0.0f;
                    var ApproximateTotalPriceWithCurrency = airPricingInfo.Attribute("ApproximateTotalPrice") != null
                        ? GrabAmount(airPricingInfo.Attribute("ApproximateTotalPrice").Value)
                        : "";
                    var TotalPriceWithCurrency = airPricingInfo.Attribute("TotalPrice") != null
                        ? GrabAmount(airPricingInfo.Attribute("TotalPrice").Value)
                        : "";
                    flightContract.TotalGDSQuotedAmount = !string.IsNullOrEmpty(ApproximateTotalPriceWithCurrency)
                        ? float.Parse(ApproximateTotalPriceWithCurrency)
                        : 0.0f;

                    platingCarrier = airPricingInfo.Attribute("PlatingCarrier") != null ? airPricingInfo.Attribute("PlatingCarrier").Value : "";

                    flightContract.ValidatingCarrier.AirlineCode = airPricingInfo.Attribute("PlatingCarrier") != null ? airPricingInfo.Attribute("PlatingCarrier").Value : "";

                    #region Getting AirSegment reference
                    var flightContractJourneySegmentRef = airPricingInfo.Descendants(airNamespace + "BookingInfo").Any()
                        ? (from segment in airPricingInfo.Descendants(airNamespace + "BookingInfo")
                           select segment.Attributes("SegmentRef").ElementAtOrDefault(0).Value).ToList()
                        : null;
                    #endregion
                }
                var outBoundAirSegmentList = flightSegmentsList.Where(x => x.Value.MarriageGroup == "0").Select(x => x.Value).ToList();
               


                var inBoundAirSegmentList = flightSegmentsList.Where(x => x.Value.MarriageGroup == "1").Select(x => x.Value).ToList();

                flightContract.TripType = outBoundAirSegmentList.Any() ? TripType.ROUNDTRIP.ToString() : TripType.ONEWAY.ToString();

                if (outBoundAirSegmentList.Any())
                {
                    List<TimeSpan> boundLayOverTime = new List<TimeSpan>();

                    flightContract.TripDetails.OutBoundSegment = outBoundAirSegmentList;
                    if (!string.IsNullOrEmpty(platingCarrier))
                        flightContract.TripDetails.OutBoundSegment.ForEach(x => x.ValidatingCarrier.AirlineCode = platingCarrier);
                    var travelTime = outBoundAirSegmentList.Sum(x => Convert.ToInt32(x.TravelTime)) + outBoundAirSegmentList.Sum(x => Convert.ToInt32(x.StopOverTime));
                    if (travelTime > 0)
                    {
                        TimeSpan calcTravelTime = new TimeSpan(0, travelTime, 0);
                        boundLayOverTime.Add(calcTravelTime);
                    }

                    isOutbound = true;
                    flightContract.TotalOutBoundFlightDuration = boundLayOverTime != null
                       ? boundLayOverTime
                       .Aggregate(TimeSpan.Zero, (sumSoFar, nextTimeSpan) => sumSoFar.Add(nextTimeSpan))
                       : TimeSpan.Zero;
                    int totalstops = flightContract.TripDetails.OutBoundSegment.Count - 1;
                    flightContract.TripDetails.OutBoundSegment.ForEach(x => x.NoOfStops = totalstops);
                    foreach (var segment in flightContract.TripDetails.OutBoundSegment)
                        flightContract.FlightDuration = flightContract.FlightDuration.Add((TimeSpan)segment.FlightDuration);
                    flightContract.Origin = flightContract.TripDetails.OutBoundSegment.FirstOrDefault().Origin;
                    flightContract.DepartureDate = flightContract.TripDetails.OutBoundSegment.FirstOrDefault().DepartureDate;
                    flightContract.Destination = flightContract.TripDetails.OutBoundSegment.LastOrDefault().Destination;
                }

                if (inBoundAirSegmentList.Any())
                {
                    List<TimeSpan> boundLayOverTime = new List<TimeSpan>();
                    flightContract.TripDetails.InBoundSegment = inBoundAirSegmentList;
                    if (!string.IsNullOrEmpty(platingCarrier))
                        flightContract.TripDetails.InBoundSegment.ForEach(x => x.ValidatingCarrier.AirlineCode = platingCarrier);
                    var travelTime = inBoundAirSegmentList.Sum(x => Convert.ToInt32(x.TravelTime)) + inBoundAirSegmentList.Sum(x => Convert.ToInt32(x.StopOverTime));
                    if (travelTime > 0)
                    {
                        TimeSpan calcTravelTime = new TimeSpan(0, Convert.ToInt32(travelTime), 0);
                        boundLayOverTime.Add(calcTravelTime);
                    }
                    isInBound = true;
                    flightContract.TotalInBoundFlightDuration = boundLayOverTime != null
                       ? boundLayOverTime
                       .Aggregate(TimeSpan.Zero, (sumSoFar, nextTimeSpan) => sumSoFar.Add(nextTimeSpan))
                       : TimeSpan.Zero;
                    int totalstops = flightContract.TripDetails.InBoundSegment.Count - 1;
                    flightContract.TripDetails.InBoundSegment.ForEach(x => x.NoOfStops = totalstops);
                    foreach (var segment in flightContract.TripDetails.InBoundSegment)
                        flightContract.FlightDuration = flightContract.FlightDuration.Add((TimeSpan)segment.FlightDuration);
                    flightContract.ArrivalDate = flightContract.TripDetails.InBoundSegment.LastOrDefault().ArrivalDate;
                }
                flightContract.CompleteTimeDuration = flightContract.TotalOutBoundFlightDuration
                    .Add(flightContract.TotalInBoundFlightDuration);

                if (airPricingInfo != null)
                {
                    var Cat35Indicator = airPricingInfo.Attribute("Cat35Indicator") != null
                    ? airPricingInfo.Attribute("Cat35Indicator").Value
                    : "";
                    //var ETicketability = singlePricingInfo.Attribute("ETicketability") != null ? singlePricingInfo.Attribute("ETicketability").Value : "";
                    flightContract.lastTicketDate = airPricingInfo.Attribute("LatestTicketingTime") != null ?
                        (GetDate(airPricingInfo.Attribute("LatestTicketingTime").Value)).ToString()
                        : "";

                    flightContract.apigds_provider = airPricingInfo.Attribute("ProviderCode") != null ? airPricingInfo.Attribute("ProviderCode").Value : "";

                    //var Refundable = priceInfo.Attribute("Refundable") != null ? Convert.ToBoolean(priceInfo.Attribute("Refundable").Value) : false;
                    flightContract.rules.refundPenalty = airPricingInfo.Attribute(airNamespace + "Refundable") != null ? (airPricingInfo.Attribute(airNamespace + "Refundable").Value) : "false";
                    var ChangePenalty = airPricingInfo.Descendants(airNamespace + "ChangePenalty").Any()
                        ? new List<string>()
                        : null;

                    if (ChangePenalty != null)
                    {
                        IEnumerable<XElement> xChangePenalties = airPricingInfo.Descendants(airNamespace + "ChangePenalty");
                        foreach (XElement xChangePenalty in xChangePenalties)
                        {
                            XElement xAmount = xChangePenalty.Element(airNamespace + "Amount");
                            if (xAmount == null)
                            {
                                xAmount = xChangePenalty.Element(airNamespace + "Percentage");
                            }
                            if (xAmount != null)
                            {

                                ChangePenalty.Add(xAmount.Value);
                            }
                        }
                        flightContract.rules.changePenalty = ChangePenalty.FirstOrDefault() + "%";
                    }
                    var CancelPenalty = airPricingInfo.Descendants(airNamespace + "CancelPenalty").Any()
                        ? new List<string>()
                        : null;

                    if (CancelPenalty != null)
                    {
                        IEnumerable<XElement> xCancelPenalties = airPricingInfo.Descendants(airNamespace + "CancelPenalty");
                        foreach (XElement xCancelPenalty in xCancelPenalties)
                        {
                            XElement xAmount = xCancelPenalty.Element(airNamespace + "Amount");
                            if (xAmount == null)
                            {
                                xAmount = xCancelPenalty.Element(airNamespace + "Percentage");
                            }
                            if (xAmount != null)
                            {
                                CancelPenalty.Add(xAmount.Value);
                            }
                        }
                        flightContract.rules.refundPenalty = ChangePenalty.FirstOrDefault() + "%";
                    }


                    #region Specific Flight/ AirPricing Related KeyList Intialization
                    List<string> fareInfoRef = new List<string>();
                    List<string> brandRef = new List<string>();
                    #endregion

                    /*
                     * Passenger Based Fare info in AirPricingInfo
                     * Seperate AirPricing for each PaxType
                     */

                    #region Getting FareInfo Refrence Key and FareInfo XElement
                    fareInfoRef = airPricingInfo.Descendants(airNamespace + "FareInfo").Any()
                        ? (from segment in airPricingInfo.Descendants(airNamespace + "FareInfo")
                           select segment.Attributes("Key").ElementAtOrDefault(0).Value).ToList()
                        : null;
                    var bookingRefs = airPricingInfo.Descendants(airNamespace + "BookingInfo");
                    foreach (var fareinfoKey in fareInfoRef)
                    {
                        //string segRefKey = (from b in bookingRefs
                        //                    where b.Attribute("FareInfoRef").Value == fareinfoKey
                        //                    select b.Attribute("SegmentRef").Value).FirstOrDefault();


                        xfareInfoList.AddRange(from fareinfo in fareInfoList_Array
                                               where fareinfo.Attributes("Key").ElementAtOrDefault(0).Value == fareinfoKey
                                               select fareinfo);
                    }
                    //foreach (var fareInfo in xfareInfoList)
                    //    gdsResponseXML.Append(fareInfo);
                    #endregion

                    #region Getting Brand Key Reference and Brand XElement
                    brandRef = (from brand in xfareInfoList.Descendants(airNamespace + "Brand")
                                where brand.Attributes("BrandID").Any()
                                select brand.Attributes("BrandID").ElementAtOrDefault(0).Value).ToList();
                    if (brandList_Array.Any())
                    {
                        foreach (var brandKey in brandRef)
                        {
                            xbrandList.AddRange(brandList_Array.Any()
                                ? (from brand in brandList_Array.Descendants(airNamespace + "Brand")
                                   where brand.Attributes("BrandID").ElementAtOrDefault(0).Value == brandKey
                                   select brand).ToList()
                                : null);
                        }
                    }
                    #endregion

                    #region Identifying FareType
                    var pricingSource = (from fareinfo in xfareInfoList
                                         where fareinfo.Attributes("PrivateFare").Any()
                                         select fareinfo).ToList();
                    flightContract.PricingSource = pricingSource != null && pricingSource.Any() ? FareType.PRIVATE.ToString() : FareType.PUBLISHED.ToString();
                    #endregion

                    bool validContract = false;
                    if (isOutbound)
                    {
                        flightContract.TripDetails.OutBoundSegment
                            .ForEach(x => x.FareBasis = string.Join("_", (from fareBasisCode in xfareInfoList
                                                                          select fareBasisCode.Attributes("FareBasis").ElementAtOrDefault(0).Value)));
                        fareBasisCodes.AddRange(flightContract.TripDetails.OutBoundSegment.Select(x => x.FareBasis));
                        validContract = CheckForValidContractBasedOnAirline(flightContract.PricingSource, detail, flightContract.TripDetails.OutBoundSegment);
                        if (!validContract)
                            return null;
                    }
                    if (isInBound)
                    {
                        flightContract.TripDetails.InBoundSegment
                            .ForEach(x => x.FareBasis = string.Join("_", (from fareBasisCode in xfareInfoList
                                                                          select fareBasisCode.Attributes("FareBasis").ElementAtOrDefault(0).Value)));
                        fareBasisCodes.AddRange(flightContract.TripDetails.InBoundSegment.Select(x => x.FareBasis));
                        validContract = CheckForValidContractBasedOnAirline(flightContract.PricingSource, detail, flightContract.TripDetails.InBoundSegment);
                        if (!validContract)
                            return null;
                    }
                    flightContract.FareBasisCode = string.Join("_", fareBasisCodes.Distinct());

                    #region Individual Passenger Fare Parsing
                    FareDetails passengerFareDetail = new FareDetails();
                    var passengerType = airPricingInfo.Element(airNamespace + "PassengerType") != null
                        ? airPricingInfo.Element(airNamespace + "PassengerType")
                        : null;
                    passengerFareDetail.PaxType = passengerType != null && passengerType.Attribute("Code") != null
                        ? passengerType.Attribute("Code").Value
                        : null;

                    passengerFareDetail.ListOfBookingInfo = airPricingInfo.Descendants(airNamespace + "BookingInfo").Any()
                        ? (from bookingInfo in airPricingInfo.Descendants(airNamespace + "BookingInfo")
                           select new BookingInfo
                           {
                               BookingCode = bookingInfo.Attribute("BookingCode") != null ? bookingInfo.Attribute("BookingCode").Value : "",
                               BookingCount = bookingInfo.Attribute("BookingCount") != null ? bookingInfo.Attribute("BookingCount").Value : "",
                               CabinClass = bookingInfo.Attribute("CabinClass") != null ? bookingInfo.Attribute("CabinClass").Value : "",
                               FareInfoRef = bookingInfo.Attribute("FareInfoRef") != null ? bookingInfo.Attribute("FareInfoRef").Value : "",
                               SegmentRef = bookingInfo.Attribute("SegmentRef") != null ? bookingInfo.Attribute("SegmentRef").Value : "",
                               HostTokenRef = bookingInfo.Attribute("HostTokenRef") != null ? bookingInfo.Attribute("HostTokenRef").Value : "",
                               FareInfoValue = airPricingInfo.Descendants("FareRuleKey").Any() ? airPricingInfo.Descendants("FareRuleKey").ElementAtOrDefault(0).Value : ""
                           }).ToList()
                        : null;



                    passengerFareDetail.FareInfo = xfareInfoList.Any()
                        ? (from fare in xfareInfoList
                           let FareRule = fare.Descendants(airNamespace + "FareRuleKey")
                           select new FareInfoRule
                           {
                               Destination = fare.Attribute("Destination") != null ? fare.Attribute("Destination").Value : "",
                               FareAmount = fare.Attribute("Amount") != null ? fare.Attribute("Amount").Value : "",
                               Origin = fare.Attribute("Origin") != null ? fare.Attribute("Origin").Value : "",
                               ProviderCode = FareRule.Any() ? FareRule.ElementAt(0).Attribute("ProviderCode") != null ? FareRule.ElementAt(0).Attribute("ProviderCode").Value : "" : "",//
                               FareInfoValue = FareRule.Any() ? FareRule.ElementAtOrDefault(0).Value : "",
                               FareRefKey = fare.Attribute("Key") != null ? fare.Attribute("Key").Value : "",
                               FareBasis = fare.Attribute("FareBasis") != null ? fare.Attribute("FareBasis").Value : "",
                               FareEffectiveDate = fare.Attribute("EffectiveDate") != null ? GetDate(fare.Attribute("EffectiveDate").Value) : new DateTime()
                           }).ToList()
                         : null;

                    var TaxList = airPricingInfo.Descendants(airNamespace + "TaxInfo").Any()
                        ? (from tax in airPricingInfo.Descendants(airNamespace + "TaxInfo")
                           select new //Tax
                           {
                               Amount = tax.Attribute("Amount").Value,
                               Category = tax.Attribute("Category").Value
                           }).ToList()
                        : null;

                    passengerFareDetail.AirPricingInfoKey = airPricingInfo.Attribute("Key") != null ? airPricingInfo.Attributes("Key").ElementAtOrDefault(0).Value : "";
                    var passengerApproximateBasePriceWithCurrency = airPricingInfo.Attribute("ApproximateBasePrice") != null
                        ? GrabAmount(airPricingInfo.Attribute("ApproximateBasePrice").Value)
                        : "";
                    var passengerBasePriceWithCurrency = airPricingInfo.Attribute("BasePrice") != null
                        ? GrabAmount(airPricingInfo.Attribute("BasePrice").Value)
                        : "";
                    passengerFareDetail.BaseFare = !string.IsNullOrEmpty(passengerApproximateBasePriceWithCurrency)
                        ? float.Parse(passengerApproximateBasePriceWithCurrency)
                        : 0.0f;
                    var passengerApproximateTaxesWithCurrency = airPricingInfo.Attribute("ApproximateTaxes") != null
                        ? GrabAmount(airPricingInfo.Attribute("ApproximateTaxes").Value)
                        : "";
                    var passengerTaxesWithCurrency = airPricingInfo.Attribute("Taxes") != null
                        ? GrabAmount(airPricingInfo.Attribute("Taxes").Value)
                        : "";
                    passengerFareDetail.Tax = !string.IsNullOrEmpty(passengerApproximateTaxesWithCurrency)
                        ? float.Parse(passengerApproximateTaxesWithCurrency)
                        : 0.0f;
                    var passengerApproximateTotalPriceWithCurrency = airPricingInfo.Attribute("ApproximateTotalPrice") != null
                        ? GrabAmount(airPricingInfo.Attribute("ApproximateTotalPrice").Value)
                        : "";
                    var passengerTotalPriceWithCurrency = airPricingInfo.Attribute("TotalPrice") != null
                        ? GrabAmount(airPricingInfo.Attribute("TotalPrice").Value)
                        : "";
                    passengerFareDetail.TotalFare = !string.IsNullOrEmpty(passengerApproximateTotalPriceWithCurrency)
                        ? float.Parse(passengerApproximateTotalPriceWithCurrency)
                        : 0.0f;

                    passengerFareDetail.CurrencyCode = airPricingInfo.Attribute("passengerApproximateTotalPriceWithCurrency") != null
                                                    && !string.IsNullOrEmpty(airPricingInfo.Attribute("passengerApproximateTotalPriceWithCurrency").Value)
                        ? Regex.Replace(airPricingInfo.Attribute("passengerApproximateTotalPriceWithCurrency").Value, @"\d+(\.\d+)?", string.Empty)
                        : "";
                    #endregion

                    var PricingMethod = airPricingInfo.Attribute("PricingMethod") != null ? airPricingInfo.Attribute("PricingMethod").Value : "";
                    passengerFareDetail.ProviderCode = airPricingInfo.Attribute("ProviderCode") != null ? airPricingInfo.Attribute("ProviderCode").Value : "";

                    #region Passenger Identification and Fare Assignment

                    if (passengerFareDetail != null && passengerFareDetail.PaxType != null)
                    {
                        int paxount = travellerDetails.Where(x => x.PaxType == passengerFareDetail.PaxType).Count();
                        switch (passengerFareDetail.PaxType)
                        {
                            case "ADT":
                                passengerFareDetail.passengerCount = paxount;
                                flightContract.AdultFare = passengerFareDetail;
                                flightContract.AdultCount = paxount;
                                break;
                            case "CHD":
                            case "CNN":
                                passengerFareDetail.passengerCount = paxount;
                                flightContract.ChildFare = passengerFareDetail;
                                break;
                            case "INS":
                                passengerFareDetail.passengerCount = paxount;
                                flightContract.InfantFare = passengerFareDetail;
                                break;
                            case "INF":
                                passengerFareDetail.passengerCount = paxount;
                                flightContract.InfantInLapFare = passengerFareDetail;
                                break;
                            case "SEN":
                                passengerFareDetail.passengerCount = paxount;
                                flightContract.SeniorFare = passengerFareDetail;
                                break;
                            default:
                                break;
                        }
                    }
                    #endregion


                }

                try
                {
                    #region Agency Info

                    var xAgencyInfos = xDoc.Descendants(commonNamespace + "AgencyInfo").Descendants(commonNamespace + "AgentAction");
                    if (xAgencyInfos != null && xAgencyInfos.Any())
                    {
                        flightContract.AgencyInfo = new AgencyInfo();
                        foreach (var xAgencyInfo in xAgencyInfos)
                        {
                            flightContract.AgencyInfo.AgentActions.Add(new AgentAction()
                            {
                                ActionType = xAgencyInfo.Attribute("ActionType") != null ? xAgencyInfo.Attribute("ActionType").Value : "",
                                AgencyCode = xAgencyInfo.Attribute("AgencyCode") != null ? xAgencyInfo.Attribute("AgencyCode").Value : "",
                                AgentCode = xAgencyInfo.Attribute("AgentCode") != null ? xAgencyInfo.Attribute("AgentCode").Value : "",
                                BranchCode = xAgencyInfo.Attribute("BranchCode") != null ? xAgencyInfo.Attribute("BranchCode").Value : "",
                                EventTime = xAgencyInfo.Attribute("EventTime") != null ? GetDate(xAgencyInfo.Attribute("EventTime").Value) : new DateTime(),
                            });
                        }
                    }
                    #endregion

                    #region FormOfPayment
                    var xFOPs = xDoc.Descendants(commonNamespace + "FormOfPayment");
                    if (xFOPs != null && xFOPs.Any())
                    {
                        flightContract.FormOfPayments = new List<FormOfPayment>();
                        foreach (var xfop in xFOPs)
                        {
                            var fop = new FormOfPayment();
                            fop.Key = xfop.Attribute("Key") != null ? xfop.Attribute("Key").Value : "";
                            fop.ProfileKey = xfop.Attribute("ProfileKey") != null ? xfop.Attribute("ProfileKey").Value : "";
                            fop.Type = xfop.Attribute("Type") != null ? xfop.Attribute("Type").Value : "";
                            fop.Resuable = xfop.Attribute("Resuable") != null ? Convert.ToBoolean(xfop.Attribute("Resuable").Value) : false;

                            var checkInfo = xfop.Descendants(commonNamespace + "Check").FirstOrDefault();
                            if (checkInfo != null)
                            {
                                fop.Check = new Check()
                                {
                                    AccountNumber = checkInfo.Attribute("AccountNumber") != null ? checkInfo.Attribute("AccountNumber").Value : "",
                                    RoutingNumber = checkInfo.Attribute("RoutingNumber") != null ? checkInfo.Attribute("RoutingNumber").Value : "",
                                    CheckNumber = checkInfo.Attribute("CheckNumber") != null ? checkInfo.Attribute("CheckNumber").Value : ""
                                };
                            }
                            flightContract.FormOfPayments.Add(fop);
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message} {ex.StackTrace}");
                }



                flightContract.TotalGDSFareV2 = flightContract.TotalBaseFare + flightContract.TotalMarkup + flightContract.TotalTax + flightContract.TotalSupplierFee;
                flightContract.TotalGDSQuotedAmount = flightContract.TotalGDSFareV2;


                //flightContract.GDSResponseXML = gdsResponseXML.ToString();
                // flightContract.Add(flightContract);

            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }


            return flightContract;
        }

        #region Travelport Helper Methods

        #region Parsing Methods
        private List<TravellerDetails> GetTravellerDetails(XElement[] travellerList_Array)
        {
            List<TravellerDetails> travellerDetailsList = new List<TravellerDetails>();
            TravellerDetails travellerDetails = null;

            var xPhones = travellerList_Array.Descendants(commonNamespace + "PhoneNumber");

            foreach (var xTraveller in travellerList_Array)
            {
                Enum.TryParse(xTraveller.Attribute("TravelerType") != null
                            ? xTraveller.Attribute("TravelerType").Value
                            : "", out PaxType paxType);

                travellerDetails = new TravellerDetails(paxType);
                //DOB="1999-06-07" ="M"
                travellerDetails.DOB = Convert.ToDateTime(xTraveller.Attribute("DOB") != null
                            ? xTraveller.Attribute("DOB").Value
                            : new DateTime().ToString());
                travellerDetails.Gender = xTraveller.Attribute("Gender") != null
                            ? xTraveller.Attribute("Gender").Value
                            : "";
                var bookingTraveller = xTraveller.Descendants(commonNamespace + "BookingTravelerName");
                if (bookingTraveller.Any())
                {
                    var traveller = bookingTraveller.First();
                    travellerDetails.FirstName = traveller.Attribute("First") != null
                                ? traveller.Attribute("First").Value
                                : "";
                    travellerDetails.LastName = traveller.Attribute("Last") != null
                           ? traveller.Attribute("Last").Value
                           : "";
                }
                var shippingAddress = xTraveller.Descendants(commonNamespace + "ShippingAddress").FirstOrDefault();
                if (shippingAddress != null)
                {
                    travellerDetails.ShippingAddress = new Models.Travelport.Address()
                    {
                        AddressName = shippingAddress.Descendants(commonNamespace + "AddressName").Any() ? shippingAddress.Descendants(commonNamespace + "AddressName").FirstOrDefault().Value : string.Empty,
                        Street = shippingAddress.Descendants(commonNamespace + "Street").Any() ? shippingAddress.Descendants(commonNamespace + "Street").FirstOrDefault().Value : string.Empty,
                        City = shippingAddress.Descendants(commonNamespace + "City").Any() ? shippingAddress.Descendants(commonNamespace + "City").FirstOrDefault().Value : string.Empty,
                        State = shippingAddress.Descendants(commonNamespace + "State").Any() ? shippingAddress.Descendants(commonNamespace + "State").FirstOrDefault().Value : string.Empty,
                        PostalCode = shippingAddress.Descendants(commonNamespace + "PostalCode").Any() ? shippingAddress.Descendants(commonNamespace + "PostalCode").FirstOrDefault().Value : string.Empty,
                        Country = shippingAddress.Descendants(commonNamespace + "Country").Any() ? shippingAddress.Descendants(commonNamespace + "Country").FirstOrDefault().Value : string.Empty,
                    };
                    //var providerReservationInfoRef = shippingAddress.Descendants(commonNamespace + "ProviderReservationInfoRef").FirstOrDefault();
                    //if(providerReservationInfoRef!=null)
                    //{
                    //    string prifKey= providerReservationInfoRef.Attribute("Key") != null
                    //            ? providerReservationInfoRef.Attribute("Key").Value
                    //            : "";
                    //    if(!string.IsNullOrEmpty(prifKey))
                    //    {
                    //        var phn = xPhones.Descendants(commonNamespace + "ProviderReservationInfoRef").Where(x => x.HasAttributes && x.Attribute("Key").Value == prifKey).FirstOrDefault();
                    //    }
                    //}
                    var phn = xTraveller.Descendants(commonNamespace + "PhoneNumber").FirstOrDefault();
                    if (phn != null)
                    {
                        travellerDetails.ShippingAddress.PhoneNumber = phn.Attribute("Number") != null
                                ? phn.Attribute("Number").Value
                                : "";
                        travellerDetails.ShippingAddress.Location = phn.Attribute("Location") != null
                                ? phn.Attribute("Location").Value
                                : "";
                    }


                }
                var address = xTraveller.Descendants(commonNamespace + "Address").FirstOrDefault();
                if (address != null)
                {
                    travellerDetails.Address = new Models.Travelport.Address()
                    {
                        AddressName = address.Descendants(commonNamespace + "AddressName").Any() ? address.Descendants(commonNamespace + "AddressName").FirstOrDefault().Value : string.Empty,
                        Street = address.Descendants(commonNamespace + "Street").Any() ? address.Descendants(commonNamespace + "Street").FirstOrDefault().Value : string.Empty,
                        City = address.Descendants(commonNamespace + "City").Any() ? address.Descendants(commonNamespace + "City").FirstOrDefault().Value : string.Empty,
                        State = address.Descendants(commonNamespace + "State").Any() ? address.Descendants(commonNamespace + "State").FirstOrDefault().Value : string.Empty,
                        PostalCode = address.Descendants(commonNamespace + "PostalCode").Any() ? address.Descendants(commonNamespace + "PostalCode").FirstOrDefault().Value : string.Empty,
                        Country = address.Descendants(commonNamespace + "Country").Any() ? address.Descendants(commonNamespace + "Country").FirstOrDefault().Value : string.Empty,
                    };
                }

                var ssr = xTraveller.Descendants(commonNamespace + "SSR").FirstOrDefault();
                if (ssr != null)
                {
                    travellerDetails.SSR = new Models.Travelport.SSR()
                    {
                        Carrier = ssr.Attribute("Carrier") != null ? ssr.Attribute("Carrier").Value : "",
                        FreeText = ssr.Attribute("FreeText") != null ? ssr.Attribute("FreeText").Value : "",
                        Status = ssr.Attribute("Status") != null ? ssr.Attribute("Status").Value : "",
                        Type = ssr.Attribute("Type") != null ? ssr.Attribute("Type").Value : "",
                    };

                }
                travellerDetailsList.Add(travellerDetails);
            }
            return travellerDetailsList;
        }

        private Dictionary<string, FlightSegments> GetListOfSegment(XElement[] airSegmentList_Array, XNamespace airNamespace)
        {
            Dictionary<string, FlightSegments> flightSegmentsList = new Dictionary<string, FlightSegments>();
            try
            {
                foreach (var segment in airSegmentList_Array)
                {
                    try
                    {
                        FlightSegments flightSegment = new FlightSegments();

                        flightSegment.SegmentKey = segment.Attribute("Key") != null
                            ? segment.Attribute("Key").Value
                            : "";
                        flightSegment.MarriageGroup = segment.Attribute("Group") != null
                           ? (segment.Attribute("Group").Value)
                           : "0";
                        flightSegment.MarketingCarrier = new Airline()
                        {
                            AirlineCode = segment.Attribute("Carrier") != null ? segment.Attribute("Carrier").Value : "",
                        };
                        flightSegment.Id = segment.Attribute("TravelOrder") != null
                                                  ? Convert.ToInt32(segment.Attribute("TravelOrder").Value)
                                                  : 0;
                        flightSegment.SegmentID = segment.Attribute("ProviderSegmentOrder") != null
                                                   ? Convert.ToInt32(segment.Attribute("ProviderSegmentOrder").Value)
                                                   : 0;
                        flightSegment.Cabin = segment.Attribute("CabinClass") != null ? segment.Attribute("CabinClass").Value : "";
                        CabinClass cabinClass = (CabinClass)Enum.Parse(typeof(CabinClass), flightSegment.Cabin);
                        flightSegment.Class = GetCabinType(cabinClass);

                        flightSegment.FlightNumber = segment.Attribute("FlightNumber") != null
                                                   ? segment.Attribute("FlightNumber").Value
                                                   : "";

                        flightSegment.Origin = segment.Attribute("Origin") != null ? segment.Attribute("Origin").Value : "";

                        flightSegment.Destination = segment.Attribute("Destination") != null
                            ? segment.Attribute("Destination").Value
                            : "";

                        DateTime DepartureTime = segment.Attribute("DepartureTime") != null
                            ? GetDate(segment.Attribute("DepartureTime").Value)
                            : new DateTime();
                        flightSegment.DepartureDate = DepartureTime.Date;
                        flightSegment.DepartureTime = new TimeSpan(DepartureTime.Hour, DepartureTime.Minute, DepartureTime.Second);

                        DateTime ArrivalTime = segment.Attribute("ArrivalTime") != null
                            ? GetDate(segment.Attribute("ArrivalTime").Value)
                            : new DateTime(); //ArrivalTime
                        flightSegment.ArrivalDate = ArrivalTime.Date;
                        flightSegment.ArrivalTime = new TimeSpan(ArrivalTime.Hour, ArrivalTime.Minute, ArrivalTime.Second);


                        flightSegment.Distance = segment.Attribute("Distance") != null
                            ? segment.Attribute("Distance").Value
                            : "";
                        flightSegment.EquipmentType = segment.Attribute("Equipment") != null
                            ? segment.Attribute("Equipment").Value
                            : "";
                        flightSegment.Equipment = segment.Attribute("Equipment") != null
                           ? segment.Attribute("Equipment").Value
                           : "";
                        var flightDetail = segment.Descendants(airNamespace + "FlightDetails").FirstOrDefault();
                        if (flightDetail != null)
                        {
                            var flightDuration = flightDetail.Attribute("TravelTime") != null
                               ? (flightDetail.Attribute("TravelTime").Value)
                               : null;
                            flightSegment.TravelTime = flightDuration;


                            var flightTime = flightDetail.Attribute("FlightTime") != null
                               ? (flightDetail.Attribute("FlightTime").Value)
                               : null;
                            flightSegment.FlightTime = flightTime;
                        }
                        flightSegment.StopOverTime = segment.Descendants(airNamespace + "Connection").Any()
                             ? segment.Descendants(airNamespace + "Connection").First().Attribute("Duration").Value
                            : null;

                        flightSegment.FlightDuration = TimeSpan.FromMinutes(Double.Parse(flightSegment.TravelTime));
                        flightSegment.ProviderCode = segment.Attribute("ProviderCode") != null
                            ? segment.Attribute("ProviderCode").Value
                            : "";
                        flightSegment.InTerminal = segment.Descendants(airNamespace + "FlightDetails").Any()
                            ? (segment.Descendants(airNamespace + "FlightDetails").First().Attribute("OriginTerminal") != null ?
                                segment.Descendants(airNamespace + "FlightDetails").First().Attribute("OriginTerminal").Value
                                : "")
                            : "";
                        flightSegment.OutTerminal = segment.Descendants(airNamespace + "FlightDetails").Any()
                            ? (segment.Descendants(airNamespace + "FlightDetails").First().Attribute("DestinationTerminal") != null ?
                                segment.Descendants(airNamespace + "FlightDetails").First().Attribute("DestinationTerminal").Value
                                : "")
                            : "";

                        flightSegment.ETicketability = segment.Attribute("ETicketability") != null ? segment.Attribute("ETicketability").Value : "";
                        flightSegment.ChangeOfPlane = segment.Attribute("ChangeOfPlane") != null ? segment.Attribute("ChangeOfPlane").Value : "";
                        flightSegment.ParticipantLevel = segment.Attribute("ParticipantLevel") != null ? segment.Attribute("ParticipantLevel").Value : "";
                        flightSegment.LinkAvailability = segment.Attribute("LinkAvailability") != null ? segment.Attribute("LinkAvailability").Value : "";
                        flightSegment.PolledAvailabilityOption = segment.Attribute("PolledAvailabilityOption") != null ? segment.Attribute("PolledAvailabilityOption").Value : "";
                        flightSegment.OptionalServicesIndicator = segment.Attribute("OptionalServicesIndicator") != null ? segment.Attribute("OptionalServicesIndicator").Value : "";
                        flightSegment.AvailabilityDisplayType = segment.Attribute("AvailabilityDisplayType") != null ? segment.Attribute("AvailabilityDisplayType").Value : "";

                        if (segment.Descendants(airNamespace + "CodeshareInfo").Any())
                        {
                            var operatingCarrier = segment.Descendants(airNamespace + "CodeshareInfo").First().Value;
                            var airlineName_string = operatingCarrier != null
                                ? operatingCarrier.ToString().Split(" ")
                                : null;
                            var airlineCode = airlineName_string != null
                                ? airlineName_string[0]
                                : null;
                            /*var airlineCode = operatingCarrier.Attribute("OperatingCarrier") != null
                                    ? operatingCarrier.Attribute("OperatingCarrier").Value
                                    : "";*/
                            if (airlineCode != null)
                            {
                                flightSegment.OperatingCarrier = new Models.Response.Airline
                                {
                                    AirlineCode = airlineCode,
                                };
                            }
                            //segment.OperatingCarrier.AirLineNumber = operatingCarrier.Attribute("OperatingFlightNumber") != null ? operatingCarrier.Attribute("OperatingFlightNumber").Value : "";
                        }

                        if (!flightSegmentsList.Keys.Any(x => x == flightSegment.SegmentKey))
                        {
                            flightSegmentsList.Add(flightSegment.SegmentKey, flightSegment);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"{ex.Message} {ex.StackTrace}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return flightSegmentsList;
        }
        #endregion

        #region Parser Helper Methods
        private DateTime GetDate(string dateToParse, int year = -1)
        {
            try
            {
                if (!string.IsNullOrEmpty(dateToParse))
                {
                    if (year == -1)
                    {
                        year = DateTime.Now.Year;
                    }
                    int yearValue = 0;
                    int monthValue = 0;
                    int dayValue = 0;
                    int hourValue = 0;
                    int minuteValue = 0;

                    string[] dateAndTimeArray = dateToParse.Split('T');
                    if (dateAndTimeArray.Length > 1)
                    {
                        string[] dateArray = dateAndTimeArray.ElementAt(0).Split('-');
                        string[] timeArray = dateAndTimeArray.ElementAt(1).Split(':');

                        if (dateArray.Length > 2)
                        {
                            yearValue = int.Parse(dateArray.ElementAt(0));
                            monthValue = int.Parse(dateArray.ElementAt(1));
                            dayValue = int.Parse(dateArray.ElementAt(2));
                        }
                        if (timeArray.Length > 1)
                        {
                            hourValue = int.Parse(timeArray.ElementAt(0));
                            minuteValue = int.Parse(timeArray.ElementAt(1));
                        }
                    }
                    else
                    {
                        string[] dateArray = dateAndTimeArray.ElementAt(0).Split('-');
                        if (dateArray.Length > 2)
                        {
                            yearValue = int.Parse(dateArray.ElementAt(0));
                            monthValue = int.Parse(dateArray.ElementAt(1));
                            dayValue = int.Parse(dateArray.ElementAt(2));
                        }
                    }
                    return new DateTime(yearValue, monthValue, dayValue, hourValue, minuteValue, 0);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return new DateTime();
        }
        private string GrabAmount(string price)
        {
            try
            {
                return Regex.Replace(price, @"[aA-zZ]", string.Empty);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return null;
        }
        private bool CheckForValidContractBasedOnAirline(string pricingSource, ProviderDetail detail, List<FlightSegments> flightSegmentsList)
        {
            bool validContract = false;
            try
            {
                string[] excludedAirlines = null;

                switch (pricingSource)
                {
                    case "PUBLISHED":
                        excludedAirlines = string.IsNullOrWhiteSpace(detail.ExcludedAirlinesForPublishFares)
                            ? null
                            : detail.ExcludedAirlinesForPublishFares.Split(',');
                        break;
                    case "PRIVATE":
                        excludedAirlines = string.IsNullOrWhiteSpace(detail.ExcludedAirlinesForPrivateFares)
                            ? null
                            : detail.ExcludedAirlinesForPrivateFares.Split(',');
                        break;
                    default:
                        excludedAirlines = null;
                        break;
                }

                if (excludedAirlines == null)
                    return true;

                validContract = (from Segment in flightSegmentsList
                                 let oC = Segment.OperatingCarrier
                                 let mC = Segment.OperatingCarrier
                                 let vC = Segment.OperatingCarrier
                                 where oC != null && excludedAirlines.Contains(oC.AirlineCode)
                                        || mC != null && excludedAirlines.Contains(mC.AirlineCode)
                                        || vC != null && excludedAirlines.Contains(vC.AirlineCode)
                                 select Segment)
                                 .ToList()
                                 .Any()
                                    ? false
                                    : true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return validContract;
        }
        private string GetCabinType(CabinClass cabinType)
        {
            string cabin = string.Empty;
            try
            {
                switch (cabinType)
                {
                    case CabinClass.First:
                        cabin = "F";
                        break;

                    case CabinClass.PremiumBusiness:
                        cabin = "J";
                        break;

                    case CabinClass.Business:
                        cabin = "C";
                        break;

                    case CabinClass.PremiumEconomy:
                        cabin = "S";
                        break;

                    case CabinClass.Economy:
                    case CabinClass.None:
                    case CabinClass.EconomyCoach:
                    default:
                        cabin = "Y";
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return cabin;
        }
        #endregion

        #endregion
    }
}
