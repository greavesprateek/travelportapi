﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Booking.Travelport;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Parsers.Parser.Interfaces;
using Flight.TravelAPI.Parsers.Travelport;
using Microsoft.Extensions.Logging;

namespace Flight.TravelAPI.Parsers.Travelport
{
    public class AirPriceParser : IBookingParser
    {
        private readonly ILogger<AirPriceParser> _logger;
        private readonly IConfigurationData _configurationData;

        public IConfigurationData ConfigurationData { get; }

        public AirPriceParser(ILogger<AirPriceParser> logger, IConfigurationData configurationData)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationData = configurationData ?? throw new ArgumentNullException(nameof(configurationData));
        }

        public BookingResponse Parse(ProviderRequest ProviderReq, string response)
        {
            BookingResponse bookingResponse = new BookingResponse { ResponseXML = response };
            var soldoutContract = ProviderReq.SelectedContract;
            var selectedContract = ProviderReq.SelectedContract;
            try
            {
                soldoutContract = DeepCopy.CreateDeepCopy(ProviderReq.SelectedContract);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Deep copy failed on {ProviderReq.SelectedContract} {ex.Message}");
            }

            try
            {
                if (string.IsNullOrEmpty(response))
                {
                    bookingResponse.IsContractSoldOutOrUnavailable = true;
                    bookingResponse.SoldOutContract = soldoutContract;
                    bookingResponse.SoldOutContract.IsContractSoldOutOrUnavailable = true;
                    bookingResponse.SoldOutContract.IsFlightSoldOrUnavailable = true;
                    bookingResponse.SoldOutContract.SelectedContractBookingStatus = bookingResponse.CurrentBookingStatus = BookingStatus.SoldOutOrUnavailable;

                   // _logger.LogError($"Contract Sold Out For Flight Guid : {ProviderReq.SelectedContract.FlightSearchDetail.FlightGuid}");
                    _logger.LogError($"AirPriceResponse is NULL or empty");
                    return bookingResponse;
                }

                XElement xDoc = XElement.Parse(response);

                XNamespace nsSoapEnvelope = "http://schemas.xmlsoap.org/soap/envelope/";
                XNamespace nsCommon_v50_0 = "http://www.travelport.com/schema/common_v50_0";

                var xErrorInfo = xDoc.Descendants(nsCommon_v50_0 + "ErrorInfo");
                var xFault = xDoc.Descendants(nsSoapEnvelope + "Fault");
                if ((xErrorInfo != null && xErrorInfo.Any()) || (xFault != null && xFault.Any()))
                {
                    var xfaultcode = xFault.Descendants("faultcode");
                    var xfaultstring = xFault.Descendants("faultstring");
                    var xDescription = xErrorInfo.Descendants(nsCommon_v50_0 + "Description");
                    var code = xfaultcode.Any() ? xfaultcode.First().Value : "Error Code Not Found";
                    var faultstring = xfaultstring.Any() ? xfaultstring.First().Value : "Error Description Not Found";
                    var description = xDescription.Any() ? xDescription.First().Value : "";

                    bookingResponse.IsContractSoldOutOrUnavailable = true;
                    bookingResponse.SoldOutContract = soldoutContract;
                    bookingResponse.SoldOutContract.IsContractSoldOutOrUnavailable = true;
                    bookingResponse.SoldOutContract.IsFlightSoldOrUnavailable = true;
                    bookingResponse.SoldOutContract.SelectedContractBookingStatus = bookingResponse.CurrentBookingStatus = BookingStatus.SoldOutOrUnavailable;
                    bookingResponse.Errors = new Error
                    {
                        Category = ErrorCategory.Error,
                        Code = $"{code}",
                        Message = $"{faultstring}. {description}",
                        Type = ErrorType.GDSError
                    };
                    return bookingResponse;
                }

                XNamespace nsAir = "http://www.travelport.com/schema/air_v50_0";

                var xAirSegmentList = xDoc.Descendants(nsAir + "AirSegment");
                var xAirPricingSolutionList = xDoc.Descendants(nsAir + "AirPricingSolution");
                var xFareInfoList = xDoc.Descendants(nsAir + "FareInfo");

                List<Segment> listOfSegment = null;
                AirPricing airPricing = null;
                List<AirPricing> listOfAirPricing = null;

                if (xAirSegmentList.Any())
                {
                    listOfSegment = GetListOfSegment(xAirSegmentList);
                }

                string[] existingFareBasisCodes = ProviderReq.SelectedContract.FareBasisCode.Split(new char[] { '_' });

                List<string> existingFareBasisCodesModified = new List<string>();
                foreach (string farebasis in existingFareBasisCodes)
                {
                    string[] tempBasis = farebasis.Split(new char[] { '/' });
                    if (tempBasis.Any())
                    {
                        existingFareBasisCodesModified.Add(tempBasis.ElementAt(0));
                    }
                }

                if (xAirPricingSolutionList.Any())
                {
                    List<string> fareBasisCodes = null;
                    List<string> fareBasisCodesModified = null;
                    listOfAirPricing = GetAirPricingList(xAirPricingSolutionList);
                    foreach (AirPricing airPricingTemp in listOfAirPricing)
                    {
                        fareBasisCodes = new List<string>();
                        fareBasisCodesModified = new List<string>();
                        foreach (AirPricingInfo airPricingInfo in airPricingTemp.AirPricingInfo)
                        {
                            foreach (FareInfoRule fareInfoRule in airPricingInfo.ListOfFareInfo)
                            {
                                //fareInfoRule.FareBasis == providerRequest.SelectedContract.
                                if (!fareBasisCodes.Any(x => x == fareInfoRule.FareBasis))
                                {
                                    fareBasisCodes.Add(fareInfoRule.FareBasis);
                                    string[] tempBasis = fareInfoRule.FareBasis.Split(new char[] { '/' });
                                    if (tempBasis.Any())
                                    {
                                        fareBasisCodesModified.Add(tempBasis.ElementAt(0));
                                    }
                                }
                            }
                        }
                        if (string.Join("_", fareBasisCodes) == ProviderReq.SelectedContract.FareBasisCode)
                        {
                            airPricing = airPricingTemp;
                            break;
                        }
                        else if (string.Join("_", fareBasisCodesModified) == string.Join("_", existingFareBasisCodesModified))
                        {
                            airPricing = airPricingTemp;
                            break;
                        }
                    }
                }
                if (airPricing == null)
                {
                    _logger.LogError($"Could for find farebasis code for {ProviderReq.SelectedContract.FareBasisCode} in {response}");
                    airPricing = listOfAirPricing.Any() ? listOfAirPricing.ElementAt(0) : null;
                    if (airPricing == null)
                    {
                        return null;
                    }
                }

                selectedContract.Fees = airPricing.Fees;
                selectedContract.ApproximateTaxes = airPricing.ApproximateTaxes;
                selectedContract.QuoteDate = airPricing.QuoteDate;
                selectedContract.ActualCurrencyBaseFare = airPricing.ActualCurrencyBaseFare;
                selectedContract.AirPricingKey = airPricing.AirPricingKey;
                selectedContract.EquivalentBasePrice = airPricing.EquivalentBasePrice;
                foreach (var outbound in ProviderReq.SelectedContract.TripDetails.OutBoundSegment)
                {
                    var segment = listOfSegment.Where(x => x.FlightNumber == outbound.FlightNumber && x.Origin == outbound.Origin && x.Destination == outbound.Destination && x.Group == 0).ElementAtOrDefault(0);

                    if (airPricing != null && segment != null)
                    {
                        var airPriceInfo = airPricing.AirPricingInfo.Any() ? airPricing.AirPricingInfo.ElementAt(0) : null;
                        if (airPriceInfo != null)
                        {
                            var pricingInfo = airPriceInfo.ListOfBookingInfo.Where(x => x.SegmentRef == segment.AirSegmentKey);
                            if (pricingInfo.Any())
                            {
                                outbound.SegmentKey = segment.AirSegmentKey;
                                outbound.HostedTokenKey = pricingInfo.First().HostTokenRef;
                                outbound.ProviderCode = segment.ProviderCode;
                                outbound.FlightTime = segment.FlightTime;
                                outbound.TravelTime = segment.TravelTime;
                                outbound.Distance = segment.Distance;
                                outbound.ClassOfService = segment.ClassOfService;
                                outbound.Equipment = segment.Equipment;
                                outbound.ChangeOfPlane = segment.ChangeOfPlane;
                                outbound.OptionalServicesIndicator = segment.OptionalServicesIndicator;
                                outbound.AvailabilitySource = segment.AvailabilitySource;
                                outbound.ParticipantLevel = segment.ParticipantLevel;
                                outbound.LinkAvailability = segment.LinkAvailability;
                                outbound.PolledAvailabilityOption = segment.PolledAvailabilityOption;
                                outbound.AvailabilityDisplayType = segment.AvailabilityDisplayType;

                                var fareXmlObject = xFareInfoList != null && xFareInfoList.Any(x => x.Attribute("FareBasis").Value == outbound.FareBasis && x.Attribute("Origin").Value == outbound.Origin && x.Attribute("Destination").Value == outbound.Destination) ? xFareInfoList.First(x => x.Attribute("FareBasis").Value == outbound.FareBasis && x.Attribute("Origin").Value == outbound.Origin && x.Attribute("Destination").Value == outbound.Destination) : null;
                                outbound.Cabin = pricingInfo.First().CabinClass;
                                int flightDuration = !string.IsNullOrEmpty(segment.FlightTime) ? Convert.ToInt32(segment.FlightTime) : 0;
                                if (flightDuration != 0)
                                {
                                    outbound.FlightDuration = new TimeSpan(0, flightDuration, 0);
                                }
                                outbound.MarketingCarrier = new Airline { AirlineCode = segment.Carrier };
                                outbound.OperatingCarrier = segment.OperatingCarrier;
                            }
                        }
                    }

                }
                if (ProviderReq.SelectedContract.TripType == TripType.ROUNDTRIP.ToString())
                {
                    foreach (var inbound in ProviderReq.SelectedContract.TripDetails.InBoundSegment)
                    {
                        var segment = listOfSegment.Where(x => x.FlightNumber == inbound.FlightNumber && x.Origin == inbound.Origin && x.Destination == inbound.Destination && x.Group == 1).ElementAtOrDefault(0);

                        if (airPricing != null && segment != null)
                        {
                            var airPriceInfo = airPricing.AirPricingInfo.Any() ? airPricing.AirPricingInfo.ElementAt(0) : null;
                            if (airPriceInfo != null)
                            {
                                var pricingInfo = airPriceInfo.ListOfBookingInfo.Where(x => x.SegmentRef == segment.AirSegmentKey);
                                if (pricingInfo.Any())
                                {
                                    inbound.SegmentKey = segment.AirSegmentKey;
                                    inbound.HostedTokenKey = pricingInfo.First().HostTokenRef;
                                    inbound.ProviderCode = segment.ProviderCode;
                                    inbound.FlightTime = segment.FlightTime;
                                    inbound.TravelTime = segment.TravelTime;
                                    inbound.Distance = segment.Distance;
                                    inbound.ClassOfService = segment.ClassOfService;
                                    inbound.Equipment = segment.Equipment;
                                    inbound.ChangeOfPlane = segment.ChangeOfPlane;
                                    inbound.OptionalServicesIndicator = segment.OptionalServicesIndicator;
                                    inbound.AvailabilitySource = segment.AvailabilitySource;
                                    inbound.ParticipantLevel = segment.ParticipantLevel;
                                    inbound.LinkAvailability = segment.LinkAvailability;
                                    inbound.PolledAvailabilityOption = segment.PolledAvailabilityOption;
                                    inbound.AvailabilityDisplayType = segment.AvailabilityDisplayType;

                                    var fareXmlObject = xFareInfoList != null && xFareInfoList.Any(x => x.Attribute("FareBasis").Value == inbound.FareBasis && x.Attribute("Origin").Value == inbound.Origin && x.Attribute("Destination").Value == inbound.Destination) ? xFareInfoList.First(x => x.Attribute("FareBasis").Value == inbound.FareBasis && x.Attribute("Origin").Value == inbound.Origin && x.Attribute("Destination").Value == inbound.Destination) : null;
                                    inbound.Cabin = pricingInfo.First().CabinClass;
                                    int flightDuration = !string.IsNullOrEmpty(segment.FlightTime) ? Convert.ToInt32(segment.FlightTime) : 0;
                                    if (flightDuration > 0)
                                    {
                                        inbound.FlightDuration = new TimeSpan(0, flightDuration, 0);
                                    }
                                    inbound.MarketingCarrier = new Airline { AirlineCode = segment.Carrier };
                                    inbound.OperatingCarrier = segment.OperatingCarrier;
                                }
                            }
                        }
                    }
                }
                if (airPricing.AirPricingInfo != null && airPricing.AirPricingInfo.Count > 0)
                {
                    selectedContract.ListOfHostToken = airPricing.HostTokenList;
                    var adultFare = airPricing.AirPricingInfo.Where(x => x.PassengerType == "ADT").First();
                    if (adultFare != null)
                    {
                        var currencyCode = adultFare.ApproximateBasePrice != null ? Regex.Replace(adultFare.ApproximateBasePrice, @"\d+(\.\d+)?", string.Empty) : "";
                        var baseFare = float.Parse(adultFare.ApproximateBasePrice != null ? GetFloatValue(adultFare.ApproximateBasePrice) : "0");
                        var ActualBaseFare = float.Parse(adultFare.BasePrice != null ? GetFloatValue(adultFare.BasePrice) : "0");
                        var taxes = float.Parse(adultFare.Taxes != null ? GetFloatValue(adultFare.Taxes) : "0");
                        var totalPrice = float.Parse(adultFare.TotalPrice != null ? GetFloatValue(adultFare.TotalPrice) : "0");
                        var fareBasis = selectedContract.TripDetails.OutBoundSegment.ElementAtOrDefault(0).FareBasis;
                        var actualCurrencyBaseFare = adultFare.BasePrice;
                        var equivalentBasePrice = adultFare.EquivalentBasePrice;
                        var refundable = adultFare.Refundable;
                        var eTicketability = adultFare.ETicketability;
                        if (selectedContract.AdultFare != null)
                        {

                            selectedContract.AdultFare.ProviderCode = adultFare.ProviderCode;
                            selectedContract.AdultFare.AirPricingInfoKey = adultFare.AirPricingInfoKey;
                            selectedContract.AdultFare.ActualBaseFare = ActualBaseFare;
                            selectedContract.AdultFare.BaseFare = baseFare;
                            selectedContract.AdultFare.CurrencyCode = currencyCode;
                            selectedContract.AdultFare.FareCode = fareBasis;
                            selectedContract.AdultFare.PaxType = "ADT";
                            selectedContract.AdultFare.Tax = taxes;
                            selectedContract.AdultFare.TotalFare = totalPrice;
                            selectedContract.AdultFare.ApproximateTaxes = adultFare.ApproximateTaxes;
                            selectedContract.AdultFare.LatestTicketingTime = adultFare.LatestTicketingTime;
                            selectedContract.AdultFare.PricingMethod = adultFare.PricingMethod;
                            selectedContract.AdultFare.IncludesVAT = adultFare.IncludesVAT;
                            selectedContract.AdultFare.PlatingCarrier = adultFare.PlatingCarrier;
                            selectedContract.AdultFare.ActualCurrencyBaseFare = actualCurrencyBaseFare;
                            selectedContract.AdultFare.EquivalentBasePrice = equivalentBasePrice;
                            selectedContract.AdultFare.Refundable = refundable;
                            selectedContract.AdultFare.ETicketability = eTicketability;
                            //selectedContract.AdultFare.Markup = travellerMarkup;

                        }
                        selectedContract.FareBasisCode = fareBasis;
                        selectedContract.AdultFare.FareInfo = new List<FareInfoRule>();
                        selectedContract.AdultFare.ListOfBookingInfo = new List<BookingInfo>();
                        var adultFareInfo = airPricing.AirPricingInfo.Where(x => x.PassengerType == "ADT");
                        List<FareInfoRule> listOfadultFareInfoRule = new List<FareInfoRule>();
                        List<BookingInfo> adultBookingInfoList = new List<BookingInfo>();
                        foreach (var fareInfo in adultFareInfo)
                        {
                            var ruleList = (from farerule in fareInfo.ListOfFareInfo
                                            select new FareInfoRule
                                            {
                                                Destination = farerule.Destination,
                                                FareAmount = farerule.FareAmount,
                                                FareBasis = farerule.FareBasis,
                                                FareEffectiveDate = farerule.FareEffectiveDate,
                                                FareInfoValue = farerule.FareInfoValue,
                                                FareRefKey = farerule.FareRefKey,
                                                Origin = farerule.Origin,
                                                ProviderCode = farerule.ProviderCode,
                                                NegotiatedFare = farerule.NegotiatedFare,
                                                NotValidBefore = farerule.NotValidBefore,
                                                NotValidAfter = farerule.NotValidAfter,
                                                TaxAmount = farerule.TaxAmount,
                                                DepartureDate = farerule.DepartureDate,
                                            }).ToList();
                            if (ruleList != null)
                            {
                                listOfadultFareInfoRule.AddRange(ruleList);
                            }

                            var bookingList = (from booking in fareInfo.ListOfBookingInfo
                                               select new BookingInfo
                                               {
                                                   BookingCode = booking.BookingCode,
                                                   BookingCount = booking.BookingCount,
                                                   CabinClass = booking.CabinClass,
                                                   FareInfoRef = booking.FareInfoRef,
                                                   FareInfoValue = booking.FareInfoValue,
                                                   HostTokenRef = booking.HostTokenRef,
                                                   SegmentRef = booking.SegmentRef,
                                               }).ToList();
                            if (bookingList != null)
                            {
                                adultBookingInfoList.AddRange(bookingList);
                            }
                        }

                        selectedContract.AdultFare.FareInfo.AddRange(listOfadultFareInfoRule);
                        selectedContract.AdultFare.ListOfBookingInfo.AddRange(adultBookingInfoList);
                    }

                    if (ProviderReq.SelectedContract.ChildCount > 0 || ProviderReq.SelectedContract.Child12To17Count > 0)
                    {
                        var childFare = airPricing.AirPricingInfo.Where(x => x.PassengerType == "CNN").First();

                        if (childFare != null)
                        {
                            var currencyCode = childFare.ApproximateBasePrice != null ? Regex.Replace(childFare.ApproximateBasePrice, @"\d+(\.\d+)?", string.Empty) : "";
                            var baseFare = float.Parse(childFare.ApproximateBasePrice != null ? GetFloatValue(childFare.ApproximateBasePrice) : "0");
                            var ActualBaseFare = float.Parse(childFare.BasePrice != null ? GetFloatValue(childFare.BasePrice) : "0");
                            var taxes = float.Parse(childFare.Taxes != null ? GetFloatValue(childFare.Taxes) : "0");
                            var totalPrice = float.Parse(childFare.TotalPrice != null ? GetFloatValue(childFare.TotalPrice) : "0");
                            var fareBasis = selectedContract.TripDetails.OutBoundSegment.ElementAtOrDefault(0).FareBasis;
                            var actualCurrencyBaseFare = childFare.BasePrice;
                            var equivalentBasePrice = childFare.EquivalentBasePrice;
                            var refundable = childFare.Refundable;
                            var eTicketability = childFare.ETicketability;
                            if (selectedContract.ChildFare != null)
                            {
                                selectedContract.ChildFare.ProviderCode = childFare.ProviderCode;
                                selectedContract.ChildFare.AirPricingInfoKey = childFare.AirPricingInfoKey;
                                selectedContract.ChildFare.ActualBaseFare = ActualBaseFare;
                                selectedContract.ChildFare.BaseFare = baseFare;
                                selectedContract.ChildFare.CurrencyCode = currencyCode;
                                selectedContract.ChildFare.FareCode = fareBasis;
                                selectedContract.ChildFare.PaxType = "CNN";
                                selectedContract.ChildFare.Tax = taxes;
                                selectedContract.ChildFare.TotalFare = totalPrice;
                                selectedContract.ChildFare.ApproximateTaxes = childFare.ApproximateTaxes;
                                selectedContract.ChildFare.LatestTicketingTime = childFare.LatestTicketingTime;
                                selectedContract.ChildFare.PricingMethod = childFare.PricingMethod;
                                selectedContract.ChildFare.IncludesVAT = childFare.IncludesVAT;
                                selectedContract.ChildFare.PlatingCarrier = childFare.PlatingCarrier;
                                selectedContract.ChildFare.ActualCurrencyBaseFare = actualCurrencyBaseFare;
                                selectedContract.ChildFare.EquivalentBasePrice = equivalentBasePrice;
                                selectedContract.ChildFare.Refundable = refundable;
                                selectedContract.ChildFare.ETicketability = eTicketability;

                            }
                            if (selectedContract.Child12To17Count > 0)
                            {
                                if (selectedContract.Child12To17Fare != null)
                                {

                                    selectedContract.Child12To17Fare.ProviderCode = childFare.ProviderCode;
                                    selectedContract.Child12To17Fare.AirPricingInfoKey = childFare.AirPricingInfoKey;
                                    selectedContract.Child12To17Fare.ActualBaseFare = ActualBaseFare;
                                    selectedContract.Child12To17Fare.BaseFare = baseFare;
                                    selectedContract.Child12To17Fare.CurrencyCode = currencyCode;
                                    selectedContract.Child12To17Fare.FareCode = fareBasis;
                                    selectedContract.Child12To17Fare.PaxType = "CNN";
                                    selectedContract.Child12To17Fare.Tax = taxes;
                                    selectedContract.Child12To17Fare.TotalFare = totalPrice;
                                    selectedContract.Child12To17Fare.ApproximateTaxes = childFare.ApproximateTaxes;
                                    selectedContract.Child12To17Fare.LatestTicketingTime = childFare.LatestTicketingTime;
                                    selectedContract.Child12To17Fare.PricingMethod = childFare.PricingMethod;
                                    selectedContract.Child12To17Fare.IncludesVAT = childFare.IncludesVAT;
                                    selectedContract.Child12To17Fare.PlatingCarrier = childFare.PlatingCarrier;
                                    selectedContract.Child12To17Fare.ActualCurrencyBaseFare = actualCurrencyBaseFare;
                                    selectedContract.Child12To17Fare.EquivalentBasePrice = equivalentBasePrice;
                                    selectedContract.Child12To17Fare.Refundable = refundable;
                                    selectedContract.Child12To17Fare.ETicketability = eTicketability;

                                };
                            }

                            selectedContract.ChildFare.FareInfo = new List<FareInfoRule>();
                            selectedContract.ChildFare.ListOfBookingInfo = new List<BookingInfo>();
                            var childFareInfo = airPricing.AirPricingInfo.Where(x => x.PassengerType == "CNN");
                            List<FareInfoRule> listOfChildFareInfoRule = new List<FareInfoRule>();
                            List<BookingInfo> childBookingInfoList = new List<BookingInfo>();
                            foreach (var fareInfo in childFareInfo)
                            {
                                var ruleList = (from farerule in fareInfo.ListOfFareInfo
                                                select new FareInfoRule
                                                {
                                                    Destination = farerule.Destination,
                                                    FareAmount = farerule.FareAmount,
                                                    FareBasis = farerule.FareBasis,
                                                    FareEffectiveDate = farerule.FareEffectiveDate,
                                                    FareInfoValue = farerule.FareInfoValue,
                                                    FareRefKey = farerule.FareRefKey,
                                                    Origin = farerule.Origin,
                                                    ProviderCode = farerule.ProviderCode,
                                                    NegotiatedFare = farerule.NegotiatedFare,
                                                    NotValidBefore = farerule.NotValidBefore,
                                                    NotValidAfter = farerule.NotValidAfter,
                                                    TaxAmount = farerule.TaxAmount,
                                                    DepartureDate = farerule.DepartureDate,
                                                }).ToList();
                                if (ruleList != null)
                                {
                                    listOfChildFareInfoRule.AddRange(ruleList);
                                }
                                var bookingList = (from booking in fareInfo.ListOfBookingInfo
                                                   select new BookingInfo
                                                   {
                                                       BookingCode = booking.BookingCode,
                                                       BookingCount = booking.BookingCount,
                                                       CabinClass = booking.CabinClass,
                                                       FareInfoRef = booking.FareInfoRef,
                                                       FareInfoValue = booking.FareInfoValue,
                                                       HostTokenRef = booking.HostTokenRef,
                                                       SegmentRef = booking.SegmentRef,
                                                   }).ToList();
                                if (bookingList != null)
                                {
                                    childBookingInfoList.AddRange(bookingList);
                                }
                            }

                            selectedContract.ChildFare.FareInfo.AddRange(listOfChildFareInfoRule);
                            selectedContract.ChildFare.ListOfBookingInfo.AddRange(childBookingInfoList);

                        }
                    }
                    if (ProviderReq.SelectedContract.InfantInLapCount > 0)
                    {
                        var infantInLapFare = airPricing.AirPricingInfo.Where(x => x.PassengerType == "INF").First();
                        if (infantInLapFare != null)
                        {
                            var currencyCode = infantInLapFare.ApproximateBasePrice != null ? Regex.Replace(infantInLapFare.ApproximateBasePrice, @"\d+(\.\d+)?", string.Empty) : "";
                            var baseFare = float.Parse(infantInLapFare.ApproximateBasePrice != null ? GetFloatValue(infantInLapFare.ApproximateBasePrice) : "0");
                            var ActualBaseFare = float.Parse(infantInLapFare.BasePrice != null ? GetFloatValue(infantInLapFare.BasePrice) : "0");
                            var taxes = float.Parse(infantInLapFare.Taxes != null ? GetFloatValue(infantInLapFare.Taxes) : "0");
                            var totalPrice = float.Parse(infantInLapFare.TotalPrice != null ? GetFloatValue(infantInLapFare.TotalPrice) : "0");
                            var fareBasis = selectedContract.TripDetails.OutBoundSegment.ElementAtOrDefault(0).FareBasis;
                            var actualCurrencyBaseFare = infantInLapFare.BasePrice;
                            var equivalentBasePrice = infantInLapFare.EquivalentBasePrice;
                            var refundable = infantInLapFare.Refundable;
                            var eTicketability = infantInLapFare.ETicketability;

                            if (selectedContract.InfantInLapFare != null)
                            {
                                selectedContract.InfantInLapFare.ProviderCode = infantInLapFare.ProviderCode;
                                selectedContract.InfantInLapFare.AirPricingInfoKey = infantInLapFare.AirPricingInfoKey;
                                selectedContract.InfantInLapFare.ActualBaseFare = ActualBaseFare;
                                selectedContract.InfantInLapFare.BaseFare = baseFare;
                                selectedContract.InfantInLapFare.CurrencyCode = currencyCode;
                                selectedContract.InfantInLapFare.FareCode = fareBasis;
                                selectedContract.InfantInLapFare.PaxType = "INF";
                                selectedContract.InfantInLapFare.Tax = taxes;
                                selectedContract.InfantInLapFare.TotalFare = totalPrice;
                                selectedContract.InfantInLapFare.ApproximateTaxes = infantInLapFare.ApproximateTaxes;
                                selectedContract.InfantInLapFare.LatestTicketingTime = infantInLapFare.LatestTicketingTime;
                                selectedContract.InfantInLapFare.PricingMethod = infantInLapFare.PricingMethod;
                                selectedContract.InfantInLapFare.IncludesVAT = infantInLapFare.IncludesVAT;
                                selectedContract.InfantInLapFare.PlatingCarrier = infantInLapFare.PlatingCarrier;
                                selectedContract.InfantInLapFare.ActualCurrencyBaseFare = actualCurrencyBaseFare;
                                selectedContract.InfantInLapFare.EquivalentBasePrice = equivalentBasePrice;
                                selectedContract.InfantInLapFare.Refundable = refundable;
                                selectedContract.InfantInLapFare.ETicketability = eTicketability;

                            };

                            selectedContract.InfantInLapFare.FareInfo = new List<FareInfoRule>();
                            selectedContract.InfantInLapFare.ListOfBookingInfo = new List<BookingInfo>();
                            var infantInLapFareInfo = airPricing.AirPricingInfo.Where(x => x.PassengerType == "INF");
                            List<FareInfoRule> listOfInfantInLapFareInfoRule = new List<FareInfoRule>();
                            List<BookingInfo> infantInLapBookingInfoList = new List<BookingInfo>();
                            foreach (var fareInfo in infantInLapFareInfo)
                            {
                                var ruleList = (from farerule in fareInfo.ListOfFareInfo
                                                select new FareInfoRule
                                                {
                                                    Destination = farerule.Destination,
                                                    FareAmount = farerule.FareAmount,
                                                    FareBasis = farerule.FareBasis,
                                                    FareEffectiveDate = farerule.FareEffectiveDate,
                                                    FareInfoValue = farerule.FareInfoValue,
                                                    FareRefKey = farerule.FareRefKey,
                                                    Origin = farerule.Origin,
                                                    ProviderCode = farerule.ProviderCode,
                                                    NegotiatedFare = farerule.NegotiatedFare,
                                                    NotValidBefore = farerule.NotValidBefore,
                                                    NotValidAfter = farerule.NotValidAfter,
                                                    TaxAmount = farerule.TaxAmount,
                                                    DepartureDate = farerule.DepartureDate,
                                                }).ToList();
                                if (ruleList != null)
                                {
                                    listOfInfantInLapFareInfoRule.AddRange(ruleList);
                                }
                                var bookingList = (from booking in fareInfo.ListOfBookingInfo
                                                   select new BookingInfo
                                                   {
                                                       BookingCode = booking.BookingCode,
                                                       BookingCount = booking.BookingCount,
                                                       CabinClass = booking.CabinClass,
                                                       FareInfoRef = booking.FareInfoRef,
                                                       FareInfoValue = booking.FareInfoValue,
                                                       HostTokenRef = booking.HostTokenRef,
                                                       SegmentRef = booking.SegmentRef,
                                                   }).ToList();
                                if (bookingList != null)
                                {
                                    infantInLapBookingInfoList.AddRange(bookingList);
                                }
                            }
                            selectedContract.InfantInLapFare.FareInfo.AddRange(listOfInfantInLapFareInfoRule);
                            selectedContract.InfantInLapFare.ListOfBookingInfo.AddRange(infantInLapBookingInfoList);
                        }

                    }
                    if (ProviderReq.SelectedContract.InfantCount > 0)
                    {
                        var infantFare = airPricing.AirPricingInfo.Where(x => x.PassengerType == "INS").FirstOrDefault();
                        if (infantFare != null)
                        {
                            var currencyCode = infantFare.ApproximateBasePrice != null ? Regex.Replace(infantFare.ApproximateBasePrice, @"\d+(\.\d+)?", string.Empty) : "";
                            var baseFare = float.Parse(infantFare.ApproximateBasePrice != null ? GetFloatValue(infantFare.ApproximateBasePrice) : "0");
                            var ActualBaseFare = float.Parse(infantFare.BasePrice != null ? GetFloatValue(infantFare.BasePrice) : "0");
                            var taxes = float.Parse(infantFare.Taxes != null ? GetFloatValue(infantFare.Taxes) : "0");
                            var totalPrice = float.Parse(infantFare.TotalPrice != null ? GetFloatValue(infantFare.TotalPrice) : "0");
                            var fareBasis = selectedContract.TripDetails.OutBoundSegment.ElementAtOrDefault(0).FareBasis;
                            var actualCurrencyBaseFare = infantFare.BasePrice;
                            var equivalentBasePrice = infantFare.EquivalentBasePrice;
                            var refundable = infantFare.Refundable;
                            var eTicketability = infantFare.ETicketability;

                            if (selectedContract.InfantFare != null)
                            {

                                selectedContract.InfantFare.ProviderCode = infantFare.ProviderCode;
                                selectedContract.InfantFare.AirPricingInfoKey = infantFare.AirPricingInfoKey;
                                selectedContract.InfantFare.ActualBaseFare = ActualBaseFare;
                                selectedContract.InfantFare.BaseFare = baseFare;
                                selectedContract.InfantFare.CurrencyCode = currencyCode;
                                selectedContract.InfantFare.FareCode = fareBasis;
                                selectedContract.InfantFare.PaxType = "INS";
                                selectedContract.InfantFare.Tax = taxes;
                                selectedContract.InfantFare.TotalFare = totalPrice;
                                selectedContract.InfantFare.ApproximateTaxes = infantFare.ApproximateTaxes;
                                selectedContract.InfantFare.LatestTicketingTime = infantFare.LatestTicketingTime;
                                selectedContract.InfantFare.PricingMethod = infantFare.PricingMethod;
                                selectedContract.InfantFare.IncludesVAT = infantFare.IncludesVAT;
                                selectedContract.InfantFare.PlatingCarrier = infantFare.PlatingCarrier;
                                selectedContract.InfantFare.ActualCurrencyBaseFare = actualCurrencyBaseFare;
                                selectedContract.InfantFare.EquivalentBasePrice = equivalentBasePrice;
                                selectedContract.InfantFare.Refundable = refundable;
                                selectedContract.InfantFare.ETicketability = eTicketability;

                            };

                            selectedContract.InfantFare.FareInfo = new List<FareInfoRule>();
                            selectedContract.InfantFare.ListOfBookingInfo = new List<BookingInfo>();
                            var infantFareInfo = airPricing.AirPricingInfo.Where(x => x.PassengerType == "INS");
                            List<FareInfoRule> listOfInfantFareInfoRule = new List<FareInfoRule>();
                            List<BookingInfo> infantBookingInfoList = new List<BookingInfo>();
                            foreach (var fareInfo in infantFareInfo)
                            {
                                var ruleList = (from farerule in fareInfo.ListOfFareInfo
                                                select new FareInfoRule
                                                {
                                                    Destination = farerule.Destination,
                                                    FareAmount = farerule.FareAmount,
                                                    FareBasis = farerule.FareBasis,
                                                    FareEffectiveDate = farerule.FareEffectiveDate,
                                                    FareInfoValue = farerule.FareInfoValue,
                                                    FareRefKey = farerule.FareRefKey,
                                                    Origin = farerule.Origin,
                                                    ProviderCode = farerule.ProviderCode,
                                                    NegotiatedFare = farerule.NegotiatedFare,
                                                    NotValidBefore = farerule.NotValidBefore,
                                                    NotValidAfter = farerule.NotValidAfter,
                                                    TaxAmount = farerule.TaxAmount,
                                                    DepartureDate = farerule.DepartureDate,
                                                }).ToList();
                                if (ruleList != null)
                                {
                                    listOfInfantFareInfoRule.AddRange(ruleList);
                                }
                                var bookingList = (from booking in fareInfo.ListOfBookingInfo
                                                   select new BookingInfo
                                                   {
                                                       BookingCode = booking.BookingCode,
                                                       BookingCount = booking.BookingCount,
                                                       CabinClass = booking.CabinClass,
                                                       FareInfoRef = booking.FareInfoRef,
                                                       FareInfoValue = booking.FareInfoValue,
                                                       HostTokenRef = booking.HostTokenRef,
                                                       SegmentRef = booking.SegmentRef,
                                                   }).ToList();
                                if (bookingList != null)
                                {
                                    infantBookingInfoList.AddRange(bookingList);
                                }
                            }
                            selectedContract.InfantFare.FareInfo.AddRange(listOfInfantFareInfoRule);
                            selectedContract.InfantFare.ListOfBookingInfo.AddRange(infantBookingInfoList);
                        }

                    }
                    var totalMarkup = selectedContract.TotalMarkup;
                    var supplierFee = selectedContract.TotalSupplierFee;
                    
                    selectedContract.TotalBaseFare = float.Parse(airPricing.ApproximateBasePrice != null ? GetFloatValue(airPricing.ApproximateBasePrice) : "0");
                    selectedContract.TotalTax = float.Parse(airPricing.ApproximateTaxes != null ? GetFloatValue(airPricing.ApproximateTaxes) : "0");
                    selectedContract.TotalGDSFareV2 = float.Parse(airPricing.ApproximateTotalPrice != null ? GetFloatValue(airPricing.ApproximateTotalPrice) : "0") + totalMarkup + supplierFee;
                }

                if (Math.Round(selectedContract.TotalGDSFareV2, 0) > Math.Round(soldoutContract.TotalGDSFareV2, 0))
                {
                    bookingResponse.CurrentBookingStatus = BookingStatus.PriceChanged;
                    bookingResponse.IsPriceChanged = true;
                    bookingResponse.PriceChangedContract = selectedContract;
                    bookingResponse.IsSuccess = false;
                    return bookingResponse;
                }
                if (selectedContract.TotalGDSFareV2 < soldoutContract.TotalGDSFareV2)
                {
                    bookingResponse.CurrentBookingStatus = BookingStatus.BookWithLowerPrice;
                    bookingResponse.IsPriceChanged = false;
                    //bookingResponse.PriceChangedContract = selectedContract;
                }
                bookingResponse.IsSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return bookingResponse;
        }

        #region Travelport Helper Method

        public List<Segment> GetListOfSegment(IEnumerable<XElement> airSegmentList)
        {
            List<Segment> listofsegment = new List<Segment>();
            XNamespace nsAir = "http://www.travelport.com/schema/air_v50_0";
            try
            {
                foreach (var seg in airSegmentList)
                {
                    Segment segment = new Segment
                    {
                        AirSegmentKey = seg.Attribute("Key") != null ? seg.Attribute("Key").Value : "",
                        Group = seg.Attribute("Group") != null ? Convert.ToInt32(seg.Attribute("Group").Value) : 0,
                        Carrier = seg.Attribute("Carrier") != null ? seg.Attribute("Carrier").Value : "",
                        FlightNumber = seg.Attribute("FlightNumber") != null ? seg.Attribute("FlightNumber").Value : "",
                        ProviderCode = seg.Attribute("ProviderCode") != null ? seg.Attribute("ProviderCode").Value : "",
                        Origin = seg.Attribute("Origin") != null ? seg.Attribute("Origin").Value : "",
                        Destination = seg.Attribute("Destination") != null ? seg.Attribute("Destination").Value : "",
                        DepartureTime = seg.Attribute("DepartureTime") != null ? GetDate(seg.Attribute("DepartureTime").Value) : new DateTime(),
                        ArrivalTime = seg.Attribute("ArrivalTime") != null ? GetDate(seg.Attribute("ArrivalTime").Value) : new DateTime(),
                        FlightTime = seg.Attribute("FlightTime") != null ? seg.Attribute("FlightTime").Value : "",
                        TravelTime = seg.Attribute("TravelTime") != null ? seg.Attribute("TravelTime").Value : "",
                        Distance = seg.Attribute("Distance") != null ? seg.Attribute("Distance").Value : "",
                        ClassOfService = seg.Attribute("ClassOfService") != null ? seg.Attribute("ClassOfService").Value : "",
                        Equipment = seg.Attribute("Equipment") != null ? seg.Attribute("Equipment").Value : "",
                        ChangeOfPlane = seg.Attribute("ChangeOfPlane") != null ? seg.Attribute("ChangeOfPlane").Value : "",
                        OptionalServicesIndicator = seg.Attribute("OptionalServicesIndicator") != null ? seg.Attribute("OptionalServicesIndicator").Value : "",
                        AvailabilitySource = seg.Attribute("AvailabilitySource") != null ? seg.Attribute("AvailabilitySource").Value : "",
                        ParticipantLevel = seg.Attribute("ParticipantLevel") != null ? seg.Attribute("ParticipantLevel").Value : "",
                        LinkAvailability = seg.Attribute("LinkAvailability") != null ? seg.Attribute("LinkAvailability").Value : "",
                        PolledAvailabilityOption = seg.Attribute("PolledAvailabilityOption") != null ? seg.Attribute("PolledAvailabilityOption").Value : "",
                        AvailabilityDisplayType = seg.Attribute("AvailabilityDisplayType") != null ? seg.Attribute("AvailabilityDisplayType").Value : "",
                    };
                    var xAirAvailInfo = seg.Descendants(nsAir + "AirAvailInfo"); //Not Found
                    if (string.IsNullOrEmpty(segment.ProviderCode) && xAirAvailInfo.Any())
                    {
                        segment.ProviderCode = xAirAvailInfo.ElementAtOrDefault(0).Attribute("ProviderCode").Value;
                    }

                    var fltdtlRef = seg.Descendants(nsAir + "FlightDetailsRef"); //Not Found
                    if (fltdtlRef != null)
                    {
                        var flightRef = fltdtlRef.ElementAtOrDefault(0);
                        if (flightRef != null)
                        {
                            segment.FlightDetailsRef = flightRef.Attribute("Key") != null ? flightRef.Attribute("Key").Value : "";
                        }
                    }
                    var xCodeshareInfo = seg.Descendants(nsAir + "CodeshareInfo");
                    if (xCodeshareInfo.Any())
                    {
                        var operatingCarrier = xCodeshareInfo.First();
                        segment.OperatingCarrier = new Airline();
                        segment.OperatingCarrier.AirlineCode = operatingCarrier.Attribute("OperatingCarrier") != null ? operatingCarrier.Attribute("OperatingCarrier").Value : "";
                    }
                    else
                    {
                        segment.OperatingCarrier = new Airline();
                        segment.OperatingCarrier.AirlineCode = seg.Attribute("Carrier") != null ? seg.Attribute("Carrier").Value : "";
                    }
                    listofsegment.Add(segment);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace} {airSegmentList}");
            }
            return listofsegment;
        }

        public List<AirPricing> GetAirPricingList(IEnumerable<XElement> airPricingSolutionList)
        {
            List<AirPricing> listOfPricing = new List<AirPricing>();
            XNamespace nsAir = "http://www.travelport.com/schema/air_v50_0";
            XNamespace nsCommon_v42_0 = "http://www.travelport.com/schema/common_v50_0";
            try
            {
                foreach (var pricing in airPricingSolutionList)
                {
                    AirPricing airPricing = new AirPricing
                    {
                        AirPricingKey = pricing.Attribute("Key") != null ? pricing.Attribute("Key").Value : "",
                        ApproximateBasePrice = pricing.Attribute("ApproximateBasePrice") != null ? pricing.Attribute("ApproximateBasePrice").Value : "",
                        ApproximateTaxes = pricing.Attribute("ApproximateTaxes") != null ? pricing.Attribute("ApproximateTaxes").Value : "",
                        ApproximateTotalPrice = pricing.Attribute("ApproximateTotalPrice") != null ? pricing.Attribute("ApproximateTotalPrice").Value : "",
                        Fees = pricing.Attribute("Fees") != null ? pricing.Attribute("Fees").Value : "",
                        QuoteDate = pricing.Attribute("QuoteDate") != null ? pricing.Attribute("QuoteDate").Value : "",
                        ActualCurrencyBaseFare = pricing.Attribute("BasePrice") != null ? pricing.Attribute("BasePrice").Value : "",
                        EquivalentBasePrice = pricing.Attribute("EquivalentBasePrice") != null ? pricing.Attribute("EquivalentBasePrice").Value : ""
                    };
                    var xJourney = pricing.Descendants(nsAir + "Journey"); //Not Found
                    if (xJourney.Any())
                    {
                        airPricing.JourneySegmentRef = (from segment in xJourney.Descendants(nsAir + "AirSegmentRef")
                                                        select segment.Attributes("Key").ElementAtOrDefault(0).Value).ToList();
                    }
                    var hostTokenXml = pricing.Descendants(nsCommon_v42_0 + "HostToken");
                    if (hostTokenXml.Any())
                    {
                        airPricing.HostTokenList = (from token in hostTokenXml
                                                    select new HostToken
                                                    {
                                                        HostTokenKey = token.Attribute("Key") != null ? token.Attribute("Key").Value : "",
                                                        HostTokenValue = token.Value
                                                    }).ToList();
                    }
                    else
                    {
                        airPricing.HostTokenList = new List<HostToken>();
                    }

                    var xConnections = pricing.Descendants(nsAir + "Connection"); //Not Found
                    if (xConnections.Any())
                    {
                        airPricing.ListOfSegmentIndex = new List<Connection>();
                        foreach (var xConnection in xConnections)
                        {
                            Connection connection = new Connection();
                            string indexValue = xConnection.Attribute("SegmentIndex") == null ? "" : xConnection.Attribute("SegmentIndex").Value;
                            string stopOver = xConnection.Attribute("StopOver") == null ? "false" : xConnection.Attribute("StopOver").Value;

                            if (!string.IsNullOrEmpty(indexValue))
                            {
                                connection.SegmentIndex = indexValue;
                                //connection.StopOver = Convert.ToBoolean(stopOver);
                                airPricing.ListOfSegmentIndex.Add(connection);
                            }
                        }
                    }

                    var xAirPricingInfo = pricing.Descendants(nsAir + "AirPricingInfo");
                    if (xAirPricingInfo.Any())
                    {
                        airPricing.AirPricingInfo = new List<AirPricingInfo>();
                        foreach (var priceInfo in xAirPricingInfo)
                        {
                            var xFareInfo = priceInfo.Descendants(nsAir + "FareInfo");
                            AirPricingInfo info = new AirPricingInfo
                            {
                                AirPricingInfoKey = priceInfo.Attribute("Key") != null ? priceInfo.Attribute("Key").Value : "",
                                ApproximateBasePrice = priceInfo.Attribute("ApproximateBasePrice") != null ? priceInfo.Attribute("ApproximateBasePrice").Value : "",
                                ApproximateTaxes = priceInfo.Attribute("ApproximateTaxes") != null ? priceInfo.Attribute("ApproximateTaxes").Value : "",
                                ApproximateTotalPrice = priceInfo.Attribute("ApproximateTotalPrice") != null ? priceInfo.Attribute("ApproximateTotalPrice").Value : "",

                                BasePrice = priceInfo.Attribute("BasePrice") != null ? priceInfo.Attribute("BasePrice").Value : "",
                                Taxes = priceInfo.Attribute("Taxes") != null ? priceInfo.Attribute("Taxes").Value : "",
                                TotalPrice = priceInfo.Attribute("TotalPrice") != null ? priceInfo.Attribute("TotalPrice").Value : "",

                                Cat35Indicator = priceInfo.Attribute("Cat35Indicator") != null ? priceInfo.Attribute("Cat35Indicator").Value : "",

                                ETicketability = priceInfo.Attribute("ETicketability") != null ? priceInfo.Attribute("ETicketability").Value : "",
                                LatestTicketingTime = priceInfo.Attribute("LatestTicketingTime") != null ? GetDate(priceInfo.Attribute("LatestTicketingTime").Value) : new DateTime(),
                                ActualCurrencyBaseFare = priceInfo.Attribute("BasePrice") != null ? priceInfo.Attribute("BasePrice").Value : "",
                                PlatingCarrier = priceInfo.Attribute("PlatingCarrier") != null ? priceInfo.Attribute("PlatingCarrier").Value : "",
                                PricingMethod = priceInfo.Attribute("PricingMethod") != null ? priceInfo.Attribute("PricingMethod").Value : "",
                                ProviderCode = priceInfo.Attribute("ProviderCode") != null ? priceInfo.Attribute("ProviderCode").Value : "",
                                IncludesVAT = priceInfo.Attribute("IncludesVAT") != null ? priceInfo.Attribute("IncludesVAT").Value : "",
                                Refundable = priceInfo.Attribute("Refundable") != null ? Convert.ToBoolean(priceInfo.Attribute("Refundable").Value) : false,
                                EquivalentBasePrice = priceInfo.Attribute("EquivalentBasePrice") != null ? priceInfo.Attribute("EquivalentBasePrice").Value : "",

                                ListOfFareInfo = (from fare in xFareInfo
                                                  let FareRule = fare.Descendants(nsAir + "FareRuleKey")
                                                  select new FareInfoRule
                                                  {
                                                      Destination = fare.Attribute("Destination") != null ? fare.Attribute("Destination").Value : "",
                                                      FareAmount = fare.Attribute("Amount") != null ? fare.Attribute("Amount").Value : "",
                                                      Origin = fare.Attribute("Origin") != null ? fare.Attribute("Origin").Value : "",
                                                      ProviderCode = FareRule.Any() ? FareRule.ElementAt(0).Attribute("ProviderCode") != null ? FareRule.ElementAt(0).Attribute("ProviderCode").Value : "" : "",//
                                                      FareInfoValue = FareRule.Any() ? FareRule.ElementAtOrDefault(0).Value : "",
                                                      FareRefKey = fare.Attribute("Key") != null ? fare.Attribute("Key").Value : "",
                                                      FareBasis = fare.Attribute("FareBasis") != null ? fare.Attribute("FareBasis").Value : "",
                                                      FareEffectiveDate = fare.Attribute("EffectiveDate") != null ? GetDate(fare.Attribute("EffectiveDate").Value) : new DateTime(),
                                                      DepartureDate = fare.Attribute("DepartureDate") != null ? fare.Attribute("DepartureDate").Value : "",
                                                      NegotiatedFare = fare.Attribute("NegotiatedFare") != null ? fare.Attribute("NegotiatedFare").Value : "",
                                                      NotValidBefore = fare.Attribute("NotValidBefore") != null ? fare.Attribute("NotValidBefore").Value : "",
                                                      NotValidAfter = fare.Attribute("NotValidAfter") != null ? fare.Attribute("NotValidAfter").Value : "",
                                                      TaxAmount = fare.Attribute("TaxAmount") != null ? fare.Attribute("TaxAmount").Value : "",
                                                  }).ToList()
                            };
                            var xBookingInfo = pricing.Descendants(nsAir + "BookingInfo");
                            var xFareRuleKey = priceInfo.Descendants(nsAir + "FareRuleKey");
                            if (xBookingInfo.Any())
                            {
                                info.ListOfBookingInfo = (from bookingInfo in xBookingInfo
                                                          select new BookingInfo
                                                          {
                                                              BookingCode = bookingInfo.Attribute("BookingCode") != null ? bookingInfo.Attribute("BookingCode").Value : "",
                                                              BookingCount = bookingInfo.Attribute("BookingCount") != null ? bookingInfo.Attribute("BookingCount").Value : "",
                                                              CabinClass = bookingInfo.Attribute("CabinClass") != null ? bookingInfo.Attribute("CabinClass").Value : "",
                                                              FareInfoRef = bookingInfo.Attribute("FareInfoRef") != null ? bookingInfo.Attribute("FareInfoRef").Value : "",
                                                              SegmentRef = bookingInfo.Attribute("SegmentRef") != null ? bookingInfo.Attribute("SegmentRef").Value : "",
                                                              HostTokenRef = bookingInfo.Attribute("HostTokenRef") != null ? bookingInfo.Attribute("HostTokenRef").Value : "",
                                                              FareInfoValue = xFareRuleKey.Any() ? xFareRuleKey.ElementAtOrDefault(0).Value : "",
                                                          }
                                                ).ToList();
                            }

                            var xPassengerType = priceInfo.Descendants(nsAir + "PassengerType");
                            if (xPassengerType.Any())
                            {
                                info.PassengerType = xPassengerType.FirstOrDefault().Attribute("Code").Value;
                            }

                            var xChangePenalties = priceInfo.Descendants(nsAir + "ChangePenalty");
                            if (xChangePenalties.Any())
                            {
                                if (info.ChangePenalty == null)
                                {
                                    info.ChangePenalty = new List<string>();
                                }
                                foreach (XElement xChangePenalty in xChangePenalties)
                                {
                                    XElement xAmount = xChangePenalty.Element("Amount");
                                    if (xAmount == null)
                                    {
                                        xAmount = xChangePenalty.Element("Percentage");
                                    }
                                    else
                                    {

                                        info.ChangePenalty.Add(xAmount.Value);
                                    }
                                }
                            }

                            var xTaxInfo = priceInfo.Descendants(nsAir + "TaxInfo");
                            if (xTaxInfo.Any())
                            {
                                info.TaxList = (from tax in xTaxInfo
                                                select new TaxInfo
                                                {
                                                    Amount = tax.Attribute("Amount").Value,
                                                    Category = tax.Attribute("Category").Value
                                                }
                                                ).ToList();
                            }
                            airPricing.AirPricingInfo.Add(info);
                        }

                    }
                    listOfPricing.Add(airPricing);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace} {airPricingSolutionList}");
            }
            return listOfPricing;
        }

        private DateTime GetDate(string dateToParse, int year = -1)
        {
            try
            {
                if (!string.IsNullOrEmpty(dateToParse))
                {
                    if (year == -1)
                    {
                        year = DateTime.Now.Year;
                    }
                    int yearValue = 0;
                    int monthValue = 0;
                    int dayValue = 0;
                    int hourValue = 0;
                    int minuteValue = 0;

                    string[] dateAndTimeArray = dateToParse.Split('T');
                    if (dateAndTimeArray.Length > 1)
                    {
                        string[] dateArray = dateAndTimeArray.ElementAt(0).Split('-');
                        string[] timeArray = dateAndTimeArray.ElementAt(1).Split(':');

                        if (dateArray.Length > 2)
                        {
                            yearValue = int.Parse(dateArray.ElementAt(0));
                            monthValue = int.Parse(dateArray.ElementAt(1));
                            dayValue = int.Parse(dateArray.ElementAt(2));
                        }
                        if (timeArray.Length > 1)
                        {
                            hourValue = int.Parse(timeArray.ElementAt(0));
                            minuteValue = int.Parse(timeArray.ElementAt(1));
                        }
                    }
                    else
                    {
                        string[] dateArray = dateAndTimeArray.ElementAt(0).Split('-');
                        if (dateArray.Length > 2)
                        {
                            yearValue = int.Parse(dateArray.ElementAt(0));
                            monthValue = int.Parse(dateArray.ElementAt(1));
                            dayValue = int.Parse(dateArray.ElementAt(2));
                        }
                    }
                    return new DateTime(yearValue, monthValue, dayValue, hourValue, minuteValue, 0);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return new DateTime();
        }

        private string GetFloatValue(string price)
        {
            return Regex.Replace(price, @"[aA-zZ]", string.Empty);
        }
        #endregion
    }
}
