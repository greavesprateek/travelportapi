﻿
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Response;
using System.Collections.Generic;

namespace Flight.TravelAPI.Parsers.Travelport
{
    internal static class DeepCopy
    {
        internal static ContractDetails CreateDeepCopy(ContractDetails selectedContract)
        {
            return new ContractDetails
            {
                AccountCode = selectedContract.AccountCode != null ? string.Copy(selectedContract.AccountCode) : null,
                AdultCount = selectedContract.AdultCount,
                AdultFare = selectedContract.AdultFare != null ? CreateDeepCopy(selectedContract.AdultFare) : null,
                AirPricingKey = selectedContract.AirPricingKey != null ? string.Copy(selectedContract.AirPricingKey) : null,
                apigds_provider = selectedContract.apigds_provider != null ? string.Copy(selectedContract.apigds_provider) : null,
                ArrivalDate = selectedContract.ArrivalDate,
                CheapestPriceFromActualAirport = selectedContract.CheapestPriceFromActualAirport,
                Child12To17Count = selectedContract.Child12To17Count,
                Child12To17Fare = selectedContract.Child12To17Fare != null ? CreateDeepCopy(selectedContract.Child12To17Fare) : null,
                ChildCount = selectedContract.ChildCount,
                ChildFare = selectedContract.ChildFare != null ? CreateDeepCopy(selectedContract.ChildFare) : null,
                CompleteTimeDuration = selectedContract.CompleteTimeDuration,
                ContractID = selectedContract.ContractID,
                Contractkey = selectedContract.Contractkey != null ? string.Copy(selectedContract.Contractkey) : null,
                CorporateID = selectedContract.CorporateID != null ? string.Copy(selectedContract.CorporateID) : null,
                DatesKey = selectedContract.DatesKey != null ? string.Copy(selectedContract.DatesKey) : null,
                DepartureDate = selectedContract.DepartureDate,
                Destination = selectedContract.Destination != null ? string.Copy(selectedContract.Destination) : null,
                domesticTicket = selectedContract.domesticTicket,
                EnginePriority = selectedContract.EnginePriority,
                FareBasisCode = selectedContract.FareBasisCode != null ? string.Copy(selectedContract.FareBasisCode) : null,
                FareCategoryID = selectedContract.FareCategoryID,
                FareType = selectedContract.FareType != null ? string.Copy(selectedContract.FareType) : null,
                FlightDuration = selectedContract.FlightDuration,
                //FlightSearchDetail = selectedContract.FlightSearchDetail != null ? CreateDeepCopy(selectedContract.FlightSearchDetail) : null,
                GDSResponseXML = selectedContract.GDSResponseXML != null ? string.Copy(selectedContract.GDSResponseXML) : null,
                InfantCount = selectedContract.InfantCount,
                InfantFare = selectedContract.InfantFare != null ? CreateDeepCopy(selectedContract.InfantFare) : null,
                InfantInLapCount = selectedContract.InfantInLapCount,
                InfantInLapFare = selectedContract.InfantInLapFare != null ? CreateDeepCopy(selectedContract.InfantInLapFare) : null,
                IsAlternateDateContract = selectedContract.IsAlternateDateContract,
                IsContractContainsLCCAirline = selectedContract.IsContractContainsLCCAirline,
                IsContractSoldOutOrUnavailable = selectedContract.IsContractSoldOutOrUnavailable,
                IsContractWithoutTime = selectedContract.IsContractWithoutTime,
                IsFlightSoldOrUnavailable = selectedContract.IsFlightSoldOrUnavailable,
                IsMultipleAirlineContract = selectedContract.IsMultipleAirlineContract,
                IsNearByAirport = selectedContract.IsNearByAirport,
                IsPassport = selectedContract.IsPassport,
                IsPriceChanged = selectedContract.IsPriceChanged,
                IsPubToNetContract = selectedContract.IsPubToNetContract,
                IsPubtonetEnabled = selectedContract.IsPubtonetEnabled,
                IsRateRuleCompleted = selectedContract.IsRateRuleCompleted,
                ItemNumber = selectedContract.ItemNumber,
                JourneyType = selectedContract.JourneyType != null ? string.Copy(selectedContract.JourneyType) : null,
                lastTicketDate = selectedContract.lastTicketDate != null ? string.Copy(selectedContract.lastTicketDate) : null,
                layover = selectedContract.layover,
                ListOfHostToken = selectedContract.ListOfHostToken != null ? CreateDeepCopy(selectedContract.ListOfHostToken) : null,
                ListOfSegmentIndex = selectedContract.ListOfSegmentIndex != null ? CreateDeepCopy(selectedContract.ListOfSegmentIndex) : null,
                majorAirline = selectedContract.majorAirline,
                MaxNoOfStopsInContract = selectedContract.MaxNoOfStopsInContract,
                MinCommissionFee = selectedContract.MinCommissionFee,
                MinSeatAvailableForContract = selectedContract.MinSeatAvailableForContract,
                operatedBy = selectedContract.operatedBy,
                Origin = selectedContract.Origin != null ? string.Copy(selectedContract.Origin) : null,
                overnightFlight = selectedContract.overnightFlight,
                overnightStay = selectedContract.overnightStay,
                PaymentRequired = selectedContract.PaymentRequired,
                PNR = selectedContract.PNR != null ? string.Copy(selectedContract.PNR) : null,
                //priceBaggageInfo = selectedContract.priceBaggageInfo != null ? CreateDeepCopy(selectedContract.priceBaggageInfo) : null,
                PricingSource = selectedContract.PricingSource != null ? string.Copy(selectedContract.PricingSource) : null,
                Provider = selectedContract.Provider != null ? string.Copy(selectedContract.Provider) : null,
                RequestedPaxCodes = selectedContract.RequestedPaxCodes != null ? string.Copy(selectedContract.RequestedPaxCodes) : null,
                rules = selectedContract.rules != null ? CreateDeepCopy(selectedContract.rules) : null,
                Provider_SesionID = selectedContract.Provider_SesionID != null ? string.Copy(selectedContract.Provider_SesionID) : null,
                SelectedContractBookingStatus = selectedContract.SelectedContractBookingStatus,
                SeniorCount = selectedContract.SeniorCount,
                SeniorFare = selectedContract.SeniorFare != null ? CreateDeepCopy(selectedContract.SeniorFare) : null,
                studentTicket = selectedContract.studentTicket,
                TotalBaseFare = selectedContract.TotalBaseFare,
                TotalGDSFareV2 = selectedContract.TotalGDSFareV2,
                TotalGDSQuotedAmount = selectedContract.TotalGDSQuotedAmount,
                TotalInBoundFlightDuration = selectedContract.TotalInBoundFlightDuration,
                TotalMarkup = selectedContract.TotalMarkup,
                TotalOutBoundFlightDuration = selectedContract.TotalOutBoundFlightDuration,
                TotalSupplierFee = selectedContract.TotalSupplierFee,
                TotalTax = selectedContract.TotalTax,
                TransactionFee = selectedContract.TransactionFee,
                TransactionID = selectedContract.TransactionID,
                TripDetails = selectedContract.TripDetails != null ? CreateDeepCopy(selectedContract.TripDetails) : null,
                TripType = selectedContract.TripType != null ? string.Copy(selectedContract.TripType) : null,
                ValidatingCarrier = selectedContract.ValidatingCarrier != null ? CreateDeepCopy(selectedContract.ValidatingCarrier) : null,
            };
        }

        private static FareDetails CreateDeepCopy(FareDetails fareDetails)
        {
            return new FareDetails
            {
                ActualBaseFare = fareDetails.ActualBaseFare,
                AirPricingInfoKey = fareDetails.AirPricingInfoKey != null ? string.Copy(fareDetails.AirPricingInfoKey) : null,
                AlternateCurrencyPrice = fareDetails.AlternateCurrencyPrice,
                AlternateCurrencyType = fareDetails.AlternateCurrencyType != null ? string.Copy(fareDetails.AlternateCurrencyType) : null,
                BaseFare = fareDetails.BaseFare,
                CurrencyCode = fareDetails.CurrencyCode != null ? string.Copy(fareDetails.CurrencyCode) : null,
                DISCOUNT_MARKUP_AMOUNT = fareDetails.DISCOUNT_MARKUP_AMOUNT,
                FareCode = fareDetails.FareCode != null ? string.Copy(fareDetails.FareCode) : null,
                FareInfo = fareDetails.FareInfo != null ? CreateDeepCopy(fareDetails.FareInfo) : null,
                IS_FIXED_DISCOUNT = fareDetails.IS_FIXED_DISCOUNT,
                IsYouth = fareDetails.IsYouth,
                ListOfBookingInfo = fareDetails.ListOfBookingInfo != null ? CreateDeepCopy(fareDetails.ListOfBookingInfo) : null,
                Markup = fareDetails.Markup,
                passengerCount = fareDetails.passengerCount,
                PaxType = fareDetails.PaxType != null ? string.Copy(fareDetails.PaxType) : null,
                ProviderCode = fareDetails.ProviderCode != null ? string.Copy(fareDetails.ProviderCode) : null,
                SupplierFee = fareDetails.SupplierFee,
                Tax = fareDetails.Tax,
                TotalFare = fareDetails.TotalFare,
            };
        }

        private static List<FareInfoRule> CreateDeepCopy(List<FareInfoRule> fareInfoRules)
        {
            List<FareInfoRule> newFareInfoRule = new List<FareInfoRule>();
            foreach (FareInfoRule fareInfoRule in fareInfoRules)
            {
                newFareInfoRule.Add
                (
                    new FareInfoRule
                    {
                        Destination = fareInfoRule.Destination != null ? string.Copy(fareInfoRule.Destination) : null,
                        FareAmount = fareInfoRule.FareAmount != null ? string.Copy(fareInfoRule.FareAmount) : null,
                        FareBasis = fareInfoRule.FareBasis != null ? string.Copy(fareInfoRule.FareBasis) : null,
                        FareEffectiveDate = fareInfoRule.FareEffectiveDate,
                        FareInfoValue = fareInfoRule.FareInfoValue != null ? string.Copy(fareInfoRule.FareInfoValue) : null,
                        FareRefKey = fareInfoRule.FareRefKey != null ? string.Copy(fareInfoRule.FareRefKey) : null,
                        Origin = fareInfoRule.Origin != null ? string.Copy(fareInfoRule.Origin) : null,
                        ProviderCode = fareInfoRule.ProviderCode != null ? string.Copy(fareInfoRule.ProviderCode) : null,
                    }
                );
            }
            return newFareInfoRule;
        }

        private static List<BookingInfo> CreateDeepCopy(List<BookingInfo> bookingInfos)
        {
            List<BookingInfo> newBookingInfos = new List<BookingInfo>();
            foreach (BookingInfo bookingInfo in bookingInfos)
            {
                newBookingInfos.Add
                (
                    new BookingInfo
                    {
                        BookingCode = bookingInfo.BookingCode != null ? string.Copy(bookingInfo.BookingCode) : null,
                        BookingCount = bookingInfo.BookingCount != null ? string.Copy(bookingInfo.BookingCount) : null,
                        CabinClass = bookingInfo.CabinClass != null ? string.Copy(bookingInfo.CabinClass) : null,
                        FareInfoRef = bookingInfo.FareInfoRef != null ? string.Copy(bookingInfo.FareInfoRef) : null,
                        FareInfoValue = bookingInfo.FareInfoValue != null ? string.Copy(bookingInfo.FareInfoValue) : null,
                        HostTokenRef = bookingInfo.HostTokenRef != null ? string.Copy(bookingInfo.HostTokenRef) : null,
                        SegmentRef = bookingInfo.SegmentRef != null ? string.Copy(bookingInfo.SegmentRef) : null,
                    }
                );
            }
            return newBookingInfos;
        }

        private static SignInToken CreateDeepCopy(SignInToken signInToken)
        {
            return new SignInToken
            {
                SeqNumber = signInToken.SeqNumber,
                SessionId = signInToken.SessionId != null ? string.Copy(signInToken.SessionId) : null,
                SessionTime = signInToken.SessionTime,
                Token = signInToken.Token != null ? string.Copy(signInToken.Token) : null,
            };
        }

        private static FlightSearchDetails CreateDeepCopy(FlightSearchDetails flightSearchDetails)
        {
            return new FlightSearchDetails
            {
                AdultCount = flightSearchDetails.AdultCount,
                Child12To17Count = flightSearchDetails.Child12To17Count,
                ChildCount = flightSearchDetails.ChildCount,
                ClientIp = flightSearchDetails.ClientIp != null ? string.Copy(flightSearchDetails.ClientIp) : null,
                DepartureDate = flightSearchDetails.DepartureDate,
                DestinationAirportCode = flightSearchDetails.DestinationAirportCode != null ? string.Copy(flightSearchDetails.DestinationAirportCode) : null,
                FlightGuid = flightSearchDetails.FlightGuid != null ? string.Copy(flightSearchDetails.FlightGuid) : null,
                InfantCount = flightSearchDetails.InfantCount,
                InfantInLapCount = flightSearchDetails.InfantInLapCount,
                OriginAirportCode = flightSearchDetails.OriginAirportCode != null ? string.Copy(flightSearchDetails.OriginAirportCode) : null,
                ReturnDate = flightSearchDetails.ReturnDate,
                SeniorCount = flightSearchDetails.SeniorCount,
                TripType = flightSearchDetails.TripType,
                AdvanceSearchModifier = flightSearchDetails.AdvanceSearchModifier != null ? CreateDeepCopy(flightSearchDetails.AdvanceSearchModifier) : null


            };
        }

        private static List<HostToken> CreateDeepCopy(List<HostToken> hostTokens)
        {
            List<HostToken> newHostTokens = new List<HostToken>();
            foreach (HostToken hostToken in hostTokens)
            {
                newHostTokens.Add
                (
                    new HostToken
                    {
                        HostTokenKey = hostToken.HostTokenKey != null ? string.Copy(hostToken.HostTokenKey) : null,
                        HostTokenValue = hostToken.HostTokenValue != null ? string.Copy(hostToken.HostTokenValue) : null,
                    }
                );
            }
            return newHostTokens;
        }

        private static List<Connection> CreateDeepCopy(List<Connection> connections)
        {
            List<Connection> newConnections = new List<Connection>();
            foreach (Connection connection in connections)
            {
                newConnections.Add
                (
                    new Connection
                    {
                        SegmentIndex = connection.SegmentIndex != null ? string.Copy(connection.SegmentIndex) : null,
                        StopOver = connection.StopOver,
                    }
                );
            }
            return newConnections;
        }

        private static List<string> CreateDeepCopy(List<string> listData)
        {
            List<string> newConnections = new List<string>();
            foreach (string data in listData)
            {
                newConnections.Add
                (
                    string.Copy(data)
                );
            }
            return newConnections;
        }

        private static PriceChangeRules CreateDeepCopy(PriceChangeRules priceChangeRules)
        {
            return new PriceChangeRules
            {
                changePenalty = priceChangeRules.changePenalty != null ? string.Copy(priceChangeRules.changePenalty) : null,
                endorsementPenalty = priceChangeRules.endorsementPenalty != null ? string.Copy(priceChangeRules.endorsementPenalty) : null,
                maximumStay = priceChangeRules.maximumStay != null ? string.Copy(priceChangeRules.maximumStay) : null,
                minimumStay = priceChangeRules.minimumStay != null ? string.Copy(priceChangeRules.minimumStay) : null,
                penalties = priceChangeRules.penalties != null ? string.Copy(priceChangeRules.penalties) : null,
                refundPenalty = priceChangeRules.refundPenalty != null ? string.Copy(priceChangeRules.refundPenalty) : null,
            };
        }

        private static TripDetails CreateDeepCopy(TripDetails tripDetails)
        {
            return new TripDetails
            {
                InBoundSegment = tripDetails.InBoundSegment != null ? CreateDeepCopy(tripDetails.InBoundSegment) : null,
                OutBoundSegment = tripDetails.OutBoundSegment != null ? CreateDeepCopy(tripDetails.OutBoundSegment) : null,
            };
        }

        private static List<FlightSegments> CreateDeepCopy(List<FlightSegments> flightSegments)
        {
            List<FlightSegments> newFlightSegments = new List<FlightSegments>();
            foreach (FlightSegments flightSegment in flightSegments)
            {
                newFlightSegments.Add
                (
                    new FlightSegments
                    {
                        AirEquipmentTypeCode = flightSegment.AirEquipmentTypeCode != null ? string.Copy(flightSegment.AirEquipmentTypeCode) : null,
                        ArrivalDate = flightSegment.ArrivalDate,
                        ArrivalTime = flightSegment.ArrivalTime,
                        AvailabilitySource = flightSegment.AvailabilitySource != null ? string.Copy(flightSegment.AvailabilitySource) : null,
                        AvailableSeats = flightSegment.AvailableSeats,
                        Cabin = flightSegment.Cabin != null ? string.Copy(flightSegment.Cabin) : null,
                        Class = flightSegment.Class != null ? string.Copy(flightSegment.Class) : null,
                        CompanyFranchiseDetails = flightSegment.CompanyFranchiseDetails != null ? string.Copy(flightSegment.CompanyFranchiseDetails) : null,
                        DepartureDate = flightSegment.DepartureDate,
                        DepartureTime = flightSegment.DepartureTime,
                        Destination = flightSegment.Destination != null ? string.Copy(flightSegment.Destination) : null,
                        Distance = flightSegment.Distance != null ? string.Copy(flightSegment.Distance) : null,
                        EquipmentType = flightSegment.EquipmentType != null ? string.Copy(flightSegment.EquipmentType) : null,
                        FareBasis = flightSegment.FareBasis != null ? string.Copy(flightSegment.FareBasis) : null,
                        FlightDuration = flightSegment.FlightDuration != null ? flightSegment.FlightDuration : null,
                        FlightNumber = flightSegment.FlightNumber != null ? string.Copy(flightSegment.FlightNumber) : null,
                        HostedTokenKey = flightSegment.HostedTokenKey != null ? string.Copy(flightSegment.HostedTokenKey) : null,
                        Id = flightSegment.Id,
                        InTerminal = flightSegment.InTerminal != null ? string.Copy(flightSegment.InTerminal) : null,
                        IsReturn = flightSegment.IsReturn,
                        IsSoldOut = flightSegment.IsSoldOut,
                        IsTripFlexible = flightSegment.IsTripFlexible,
                        majorAirline = flightSegment.majorAirline,
                        MarketingCarrier = flightSegment.MarketingCarrier != null ? CreateDeepCopy(flightSegment.MarketingCarrier) : null,
                        MarriageGroup = flightSegment.MarriageGroup != null ? string.Copy(flightSegment.MarriageGroup) : null,
                        NoOfStops = flightSegment.NoOfStops,
                        OperatingCarrier = flightSegment.OperatingCarrier != null ? CreateDeepCopy(flightSegment.OperatingCarrier) : null,
                        Origin = flightSegment.Origin != null ? string.Copy(flightSegment.Origin) : null,
                        OutTerminal = flightSegment.OutTerminal != null ? string.Copy(flightSegment.OutTerminal) : null,
                        ProviderCode = flightSegment.ProviderCode != null ? string.Copy(flightSegment.ProviderCode) : null,
                        SegmentID = flightSegment.SegmentID,
                        SegmentKey = flightSegment.SegmentKey != null ? string.Copy(flightSegment.SegmentKey) : null,
                        StopOverTime = flightSegment.StopOverTime != null ? string.Copy(flightSegment.StopOverTime) : null,
                        TechnicalStop = flightSegment.TechnicalStop != null ? CreateDeepCopy(flightSegment.TechnicalStop) : null,
                        ValidatingCarrier = flightSegment.ValidatingCarrier != null ? CreateDeepCopy(flightSegment.ValidatingCarrier) : null,
                    }
                );
            }
            return newFlightSegments;
        }

        private static Airline CreateDeepCopy(Airline airline)
        {
            return new Airline
            {
                AirlineCode = airline.AirlineCode != null ? string.Copy(airline.AirlineCode) : null,
                //AirlineName = airline.AirlineName != null ? string.Copy(airline.AirlineName) : null,
                //AirLineNumber = airline.AirLineNumber != null ? string.Copy(airline.AirLineNumber) : null,
                //IsLowCostCarrierAirline = airline.IsLowCostCarrierAirline,
            };
        }

        private static List<TechnicalStop> CreateDeepCopy(List<TechnicalStop> technicalStops)
        {
            List<TechnicalStop> newTechnicalStop = new List<TechnicalStop>();
            foreach (TechnicalStop technicalStop in technicalStops)
            {
                newTechnicalStop.Add
                (
                    new TechnicalStop
                    {
                        LstStopDetails = technicalStop.LstStopDetails != null ? CreateDeepCopy(technicalStop.LstStopDetails) : null,
                    }
                );
            }
            return newTechnicalStop;
        }

        private static List<StopDetails> CreateDeepCopy(List<StopDetails> stopDetails)
        {
            List<StopDetails> newStopDetails = new List<StopDetails>();
            foreach (StopDetails stopDetail in stopDetails)
            {
                newStopDetails.Add
                (
                    new StopDetails
                    {
                        Date = stopDetail.Date != null ? string.Copy(stopDetail.Date) : null,
                        DateQualifier = stopDetail.DateQualifier != null ? string.Copy(stopDetail.DateQualifier) : null,
                        FirstTime = stopDetail.FirstTime != null ? string.Copy(stopDetail.FirstTime) : null,
                        LocationId = stopDetail.LocationId != null ? string.Copy(stopDetail.LocationId) : null,
                    }
                );
            }
            return newStopDetails;
        }
        private static AdvanceSearchModifier CreateDeepCopy(AdvanceSearchModifier advanceSearchModifier)
        {
            AdvanceSearchModifier newModifier = new AdvanceSearchModifier();
            newModifier.DirectFlights = advanceSearchModifier.DirectFlights;
            newModifier.CabinClass = advanceSearchModifier.CabinClass;
            newModifier.IsRedEyeFlight = advanceSearchModifier.IsRedEyeFlight;
            newModifier.DirectFlights = advanceSearchModifier.DirectFlights;
            newModifier.ExcludeGroundTransportation = advanceSearchModifier.ExcludeGroundTransportation;
            newModifier.JetServiceOnly = advanceSearchModifier.JetServiceOnly;
            newModifier.MaxJourneyTime_Hours = advanceSearchModifier.MaxJourneyTime_Hours;
            newModifier.DepPermittedConnectionPoints = advanceSearchModifier.DepPermittedConnectionPoints;
            newModifier.PrefferedCarrierCodes = advanceSearchModifier.PrefferedCarrierCodes;
            newModifier.ProhibitedCarrierCodes = advanceSearchModifier.ProhibitedCarrierCodes;
            newModifier.DepProhibitedConnectionPoints = advanceSearchModifier.DepProhibitedConnectionPoints;
            newModifier.ArvProhibitedConnectionPoints = advanceSearchModifier.ArvProhibitedConnectionPoints;
            newModifier.ArvPermittedConnectionPoints = advanceSearchModifier.ArvPermittedConnectionPoints;
            newModifier.SearchArrTime = advanceSearchModifier.SearchArrTime != null ? CreateDeepCopy(advanceSearchModifier.SearchArrTime) : null; ;
            newModifier.SearchDepTime = advanceSearchModifier.SearchDepTime != null ? CreateDeepCopy(advanceSearchModifier.SearchDepTime) : null;
            return newModifier;
        }

        private static SearchTimeRange CreateDeepCopy(SearchTimeRange st)
        {
            SearchTimeRange searchTime = new SearchTimeRange();
            searchTime.EarliestTime = st.EarliestTime;
            searchTime.LatestTime = st.LatestTime;
            searchTime.PreferredTime = st.PreferredTime;
            return searchTime;
        }
    }
}
