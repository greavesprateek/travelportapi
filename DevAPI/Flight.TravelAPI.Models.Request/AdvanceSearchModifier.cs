﻿using Flight.TravelAPI.Models.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Flight.TravelAPI.Models.Request
{
    public class AdvanceSearchModifier
    {
        public bool IsRedEyeFlight { get; set; }
        public CabinClass CabinClass { get; set; }
        public List<string> PrefferedCarrierCodes { get; set; }
        public List<string> ProhibitedCarrierCodes { get; set; }
        public List<string> DepPermittedConnectionPoints { get; set; }
        public List<string> DepProhibitedConnectionPoints { get; set; }
        public List<string> ArvPermittedConnectionPoints { get; set; }
        public List<string> ArvProhibitedConnectionPoints { get; set; }
        public bool ExcludeGroundTransportation { get; set; }
        public bool DirectFlights { get; set; }
        public bool JetServiceOnly { get; set; }
        public int MaxJourneyTime_Hours { get; set; }
        public SearchTimeRange SearchDepTime { get; set; }
        public SearchTimeRange SearchArrTime { get; set; }
    }

    public class SearchTimeRange
    {
        public TimeSpan EarliestTime { get; set; }
        public TimeSpan LatestTime { get; set; }
        public TimeSpan PreferredTime { get; set; }
    }
}
