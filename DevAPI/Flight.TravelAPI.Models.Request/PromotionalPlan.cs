﻿namespace Flight.TravelAPI.Models.Request
{
    public enum PromotionalPlan
    {
        Normal,
        Regular,
        Critical
    }
}