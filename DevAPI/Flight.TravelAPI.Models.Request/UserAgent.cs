﻿namespace Flight.TravelAPI.Models.Request
{
    public enum UserAgent
    {
        None = 0,
        Desktop = 1,
        Mobile = 2
    }
}