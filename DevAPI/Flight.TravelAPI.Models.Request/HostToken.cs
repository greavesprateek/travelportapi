﻿namespace Flight.TravelAPI.Models.Request
{
    public class HostToken
    {
        public string HostTokenKey { get; set; }
        public string HostTokenValue { get; set; }
    }
}