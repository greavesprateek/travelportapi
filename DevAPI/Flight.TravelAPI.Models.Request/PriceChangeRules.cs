﻿namespace Flight.TravelAPI.Models.Request
{
    public class PriceChangeRules
    {
        public string refundPenalty { get; set; }
        public string changePenalty { get; set; }
        public string endorsementPenalty { get; set; }
        public string penalties { get; set; }
        public string minimumStay { get; set; }
        public string maximumStay { get; set; }
    }
}