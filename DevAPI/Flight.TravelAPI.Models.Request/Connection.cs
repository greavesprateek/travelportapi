﻿namespace Flight.TravelAPI.Models.Request
{
    public class Connection
    {
        public bool StopOver { get; set; }
        public string SegmentIndex { get; set; }
    }
}