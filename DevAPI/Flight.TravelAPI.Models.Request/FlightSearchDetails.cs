﻿using Flight.TravelAPI.Models.Enumerations;
using System;

namespace Flight.TravelAPI.Models.Request
{
    public class FlightSearchDetails
    {
        public string OriginAirportCode { get; set; }
        public string DestinationAirportCode { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int AdultCount { get; set; }
        public int SeniorCount { get; set; }
        public int ChildCount { get; set; }
        public int Child12To17Count { get; set; }
        public int InfantCount { get; set; }
        public int InfantInLapCount { get; set; }
        public TripType TripType { get; set; }
        public string ClientIp { get; set; }
        public string FlightGuid { get; set; }
        //[JsonIgnore]
        public AdvanceSearchModifier AdvanceSearchModifier { get; set; }

    }
}