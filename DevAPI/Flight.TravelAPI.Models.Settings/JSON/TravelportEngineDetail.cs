﻿using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using Flight.TravelAPI.Providers.Base;
using Flight.TravelAPI.Providers.Travelport.Interfaces;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using System.Collections.Generic;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;

namespace Flight.TravelAPI.Models.Settings.JSON
{
    public class TravelportEngineDetail
    {
        public GDS BasedOn { get; set; }
        public string Engine { get; set; }
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        public string PccCode { get; set; }
        public string BranchCode { get; set; }
        public string Domain { get; set; }
        public string ApiUrl { get; set; }
        public string Timeout { get; set; }
        public string RedEyeTime { get; set; }
        public string RedEyeEndTime { get; set; }

        public IEnumerable<IProvider> Update(ProviderDetail providerDetail, ITravelportEngine travelportEngine)
        {
            if (providerDetail != null)
            {
                travelportEngine.BasedOn = GDS.Travelport;
                travelportEngine.Engine = Engine;
                travelportEngine.Domain = Domain;
                travelportEngine.ApiUrl = ApiUrl;
                travelportEngine.Timeout = Timeout;
                travelportEngine.UserName = UserName;
                travelportEngine.UserPwd = UserPwd;
                travelportEngine.PccCode = PccCode;
                travelportEngine.BranchCode = BranchCode;
                travelportEngine.ProviderDetails = providerDetail;
                travelportEngine.RedEyeTime = RedEyeTime;
                travelportEngine.RedEyeEndTime = RedEyeEndTime;
                travelportEngine.SearchTypes.Add(SearchType.Regular);
                if (providerDetail.IsFlexi) travelportEngine.SearchTypes.Add(SearchType.Flexi);
                if (providerDetail.IsNearBy) travelportEngine.SearchTypes.Add(SearchType.NearBy);

                yield return travelportEngine;
            }
        }

        public IEnumerable<IWorkflow> Update(IWorkflow workflow, ITravelportEngine engine, IDirector _director, IClient _client, BookingParserResolver _parserResolver, IConfigurationData configurationData)
        {
            engine.Engine = Engine;
            engine.ApiUrl = ApiUrl;
            engine.UserName = UserName;
            engine.UserPwd = UserPwd;
            engine.PccCode = PccCode;
            engine.BranchCode = BranchCode;
            engine.Domain = Domain;
            workflow.Engine = engine;
            workflow.PrepareWorkflow(_director, _client, _parserResolver,configurationData);
            yield return workflow;
        }
    }
}
