﻿using Flight.TravelAPI.Builder;
using Flight.TravelAPI.Builder.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Providers.Base;
using Flight.TravelAPI.Providers.Travelport.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flight.TravelAPI.TravelportBuilder
{
    public class TravelportSearchBuilder : BuilderBase, ISearchBuilder
    {
        private readonly IConfigurationData _configurationData;
        private readonly ILogger<TravelportSearchBuilder> _logger;

        public TravelportSearchBuilder(ILogger<TravelportSearchBuilder> logger, IConfigurationData configurationData)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationData = configurationData ?? throw new ArgumentNullException(nameof(configurationData));
        }

        public void BuildBody(FlightSearchDetails flightSearchDetails, IProvider provider, SearchType searchType, KeyValuePair<string, string> qualifier = default(KeyValuePair<string, string>))
        {
            if (!(provider is ITravelportEngine travelportEngine)) return;
            var strBuilder = new StringBuilder();
            try
            {
                var details = travelportEngine.ProviderDetails;
                string airNamespace = @"http://www.travelport.com/schema/air_v50_0";
                string commonNamespace = @"http://www.travelport.com/schema/common_v50_0";
                string fareType = "PublicAndPrivateFares";

                if (details.IsPublishFareEnable && ((searchType == SearchType.Regular && details.IsPrivateFareEnable) || (searchType == SearchType.Flexi && details.IsPrivateFlexi) || (searchType == SearchType.NearBy && details.IsPrivateNearBy)))
                {
                    fareType = "PublicAndPrivateFares";
                }
                else if (details.IsPublishFareEnable && !((searchType == SearchType.Regular && details.IsPrivateFareEnable) || (searchType == SearchType.Flexi && details.IsPrivateFlexi) || (searchType == SearchType.NearBy && details.IsPrivateNearBy)))
                {
                    fareType = "PublicFaresOnly";
                }
                else if (!details.IsPublishFareEnable && ((searchType == SearchType.Regular && details.IsPrivateFareEnable) || (searchType == SearchType.Flexi && details.IsPrivateFlexi) || (searchType == SearchType.NearBy && details.IsPrivateNearBy)))
                {
                    fareType = "PrivateFaresOnly";
                }

                string[] lstExcludedAirlinesForPublishFares = string.IsNullOrWhiteSpace(provider.ProviderDetails.ExcludedAirlinesForPublishFares) ? null : provider.ProviderDetails.ExcludedAirlinesForPublishFares.Split(',');
                string[] lstExcludedAirlinesForPrivateFares = string.IsNullOrWhiteSpace(provider.ProviderDetails.ExcludedAirlinesForPrivateFares) ? null : provider.ProviderDetails.ExcludedAirlinesForPrivateFares.Split(',');

                strBuilder.Append($"<LowFareSearchReq xmlns='{airNamespace}' AuthorizedBy='user' SolutionResult='true' TargetBranch='{travelportEngine.BranchCode}' TraceId='{flightSearchDetails.FlightGuid}'>");

                strBuilder.Append($"<BillingPointOfSaleInfo xmlns='{commonNamespace}' OriginApplication='UAPI'/>");
                /*one way leg */
                strBuilder.Append("<SearchAirLeg>");

                DeptONDsRequest(flightSearchDetails, strBuilder, commonNamespace, searchType);

                DeptSearchDateTimeRequest(flightSearchDetails, searchType, travelportEngine, strBuilder, details, commonNamespace);

                AdvOptionAirLegModifierRequest(flightSearchDetails, strBuilder, commonNamespace);
                if (flightSearchDetails.AdvanceSearchModifier != null && flightSearchDetails.AdvanceSearchModifier.CabinClass != CabinClass.None)
                    AddCabinClass(flightSearchDetails, strBuilder, commonNamespace);
                strBuilder.Append("</SearchAirLeg>");

                /*Round Trip Leg*/
                if (flightSearchDetails.TripType == Models.Enumerations.TripType.ROUNDTRIP)
                {
                    strBuilder.Append("<SearchAirLeg>");
                    if (searchType == SearchType.NearBy)
                    {
                        strBuilder.Append("<SearchOrigin>");
                        strBuilder.Append($"<CityOrAirport xmlns='{commonNamespace}' Code='{flightSearchDetails.DestinationAirportCode}'/>");
                        strBuilder.Append("</SearchOrigin>");
                        strBuilder.Append("<SearchDestination>");
                        strBuilder.Append($"<CityOrAirport xmlns='{commonNamespace}' Code='{flightSearchDetails.OriginAirportCode}'/>");
                        strBuilder.Append("</SearchDestination>");
                    }
                    else
                    {
                        strBuilder.Append("<SearchOrigin>");
                        strBuilder.Append($"<CityOrAirport xmlns='{commonNamespace}' Code='{flightSearchDetails.DestinationAirportCode}' PreferCity='true' />");
                        strBuilder.Append("</SearchOrigin>");
                        strBuilder.Append("<SearchDestination>");
                        strBuilder.Append($"<CityOrAirport xmlns='{commonNamespace}' Code='{flightSearchDetails.OriginAirportCode}' PreferCity='true' />");
                        strBuilder.Append("</SearchDestination>");
                    }

                    ArvSearchDateTimeRequest(flightSearchDetails, searchType, travelportEngine, strBuilder, details, commonNamespace);

                    AdvOptionAirLegModifierRequest(flightSearchDetails, strBuilder, commonNamespace, true);
                    if (flightSearchDetails.AdvanceSearchModifier != null && flightSearchDetails.AdvanceSearchModifier.CabinClass != CabinClass.None)
                        AddCabinClass(flightSearchDetails, strBuilder, commonNamespace);
                    strBuilder.Append("</SearchAirLeg>");
                }

                AirSearchModifierRequest(flightSearchDetails, strBuilder, commonNamespace, travelportEngine);

                /*Passenger*/
                for (int adult = 0; adult < flightSearchDetails.AdultCount; adult++)
                {
                    strBuilder.Append($"<SearchPassenger xmlns='{commonNamespace}' Code='ADT'  />");
                }

                int ChildCount = flightSearchDetails.ChildCount + flightSearchDetails.Child12To17Count;
                for (int child = 0; child < ChildCount; child++)
                {
                    strBuilder.Append($"<SearchPassenger xmlns='{commonNamespace}' Code='CNN' />");
                }

                for (int infant = 0; infant < flightSearchDetails.InfantCount; infant++)
                {
                    strBuilder.Append($"<SearchPassenger xmlns='{commonNamespace}' Code='INS'  />");
                }

                for (int infantLp = 0; infantLp < flightSearchDetails.InfantInLapCount; infantLp++)
                {
                    strBuilder.Append($"<SearchPassenger xmlns='{commonNamespace}' Code='INF'  />");
                }

                strBuilder.Append($"<AirPricingModifiers ReturnFareAttributes='true' FaresIndicator='{fareType}'>");
                strBuilder.Append("<AccountCodes>");
                strBuilder.Append($"<AccountCode xmlns='{commonNamespace}' Code='-' />");
                strBuilder.Append("</AccountCodes>");
                strBuilder.Append("</AirPricingModifiers>");

                strBuilder.Append("</LowFareSearchReq>");
                //strBuilder.Append("</soapenv:Body>");
                //strBuilder.Append("</soapenv:Envelope>");

                Body = strBuilder.ToString();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
        }

        private static void DeptSearchDateTimeRequest(FlightSearchDetails flightSearchDetails, SearchType searchType, ITravelportEngine travelportEngine, StringBuilder strBuilder, Models.Settings.ProviderDetails.ProviderDetail details, string commonNamespace)
        {
            var advSearch = flightSearchDetails.AdvanceSearchModifier;
            bool isUpdated = false;
            if (advSearch != null)
            {

                if (advSearch.IsRedEyeFlight)
                {
                    isUpdated = true;
                    strBuilder.Append("<SearchDepTime>");
                    strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{travelportEngine.RedEyeTime}' LatestTime='{flightSearchDetails.DepartureDate.AddDays(1).ToString("yyyy-MM-dd")}{travelportEngine.RedEyeEndTime}'/>");
                    strBuilder.Append("</SearchDepTime>");
                }
                else if (advSearch.SearchDepTime != null)
                {
                    isUpdated = true;
                    var depTime = advSearch.SearchDepTime;
                    if (depTime.PreferredTime != null)
                        strBuilder.Append($"<SearchDepTime PreferredTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{depTime.PreferredTime}'");
                    else
                        strBuilder.Append("<SearchDepTime>");

                    if (depTime.EarliestTime != null && depTime.LatestTime != null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{depTime.EarliestTime}' LatestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{depTime.LatestTime}'/>");
                    else if (depTime.EarliestTime != null && depTime.LatestTime == null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{depTime.EarliestTime}' LatestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{"T23:59:59.000"}'/>");
                    else if (depTime.EarliestTime == null && depTime.LatestTime != null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}' LatestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{depTime.LatestTime}'/>");

                    strBuilder.Append("</SearchDepTime>");
                }

                if (advSearch.SearchArrTime != null)
                {
                    var arrTime = advSearch.SearchArrTime;
                    if (arrTime.PreferredTime != null)
                        strBuilder.Append($"<SearchArvTime PreferredTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{arrTime.PreferredTime}'");
                    else
                        strBuilder.Append("<SearchArvTime>");

                    if (arrTime.EarliestTime != null && arrTime.LatestTime != null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{arrTime.EarliestTime}' LatestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{arrTime.LatestTime}'/>");
                    else if (arrTime.EarliestTime != null && arrTime.LatestTime == null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{arrTime.EarliestTime}' LatestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{"T23:59:59.000"}'/>");
                    else if (arrTime.EarliestTime == null && arrTime.LatestTime != null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}' LatestTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}{arrTime.LatestTime}'/>");

                    strBuilder.Append("</SearchArvTime>");
                }
            }
            if (!isUpdated)
            {
                if (flightSearchDetails.DepartureDate == flightSearchDetails.ReturnDate)
                {
                    strBuilder.Append($"<SearchDepTime PreferredTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}T08:00'/>");
                }
                else
                {
                    strBuilder.Append($"<SearchDepTime PreferredTime='{flightSearchDetails.DepartureDate.ToString("yyyy-MM-dd")}'/>");
                }
            }

        }

        private static void DeptONDsRequest(FlightSearchDetails flightSearchDetails, StringBuilder strBuilder, string commonNamespace, SearchType searchType)
        {
            strBuilder.Append("<SearchOrigin>");
            if (searchType == SearchType.NearBy)
            {
                strBuilder.Append($"<CityOrAirport xmlns='{commonNamespace}' Code='{flightSearchDetails.OriginAirportCode}'/>");
                strBuilder.Append("</SearchOrigin>");
                strBuilder.Append("<SearchDestination>");
                strBuilder.Append($"<CityOrAirport xmlns='{commonNamespace}' Code='{flightSearchDetails.DestinationAirportCode}'/>");
                strBuilder.Append("</SearchDestination>");
            }
            else
            {
                strBuilder.Append($"<CityOrAirport xmlns='{commonNamespace}' Code='{flightSearchDetails.OriginAirportCode}' PreferCity='true'/>");
                strBuilder.Append("</SearchOrigin>");
                strBuilder.Append("<SearchDestination>");
                strBuilder.Append($"<CityOrAirport xmlns='{commonNamespace}' Code='{flightSearchDetails.DestinationAirportCode}' PreferCity='true'/>");
                strBuilder.Append("</SearchDestination>");
            }
        }

        private static void AdvOptionAirLegModifierRequest(FlightSearchDetails flightSearchDetails, StringBuilder strBuilder, string commonNamespace, bool IsReturnLeg = false)
        {
            var advSearch = flightSearchDetails.AdvanceSearchModifier;
            if (advSearch != null)
            {
                if (!IsReturnLeg)
                {
                    if (advSearch.DepPermittedConnectionPoints != null || advSearch.DepProhibitedConnectionPoints != null)
                    {
                        strBuilder.Append("<AirLegModifiers>");
                        if (advSearch.DepPermittedConnectionPoints != null && advSearch.DepPermittedConnectionPoints.Count > 0)
                        {
                            strBuilder.Append("<PermittedConnectionPoints>");
                            foreach (var cp in advSearch.DepPermittedConnectionPoints)
                            {
                                strBuilder.Append($"<ConnectionPoint><City Code=='{cp}' /></ConnectionPoint>");
                            }
                            strBuilder.Append("</PermittedConnectionPoints>");
                        }
                        if (advSearch.DepProhibitedConnectionPoints != null && advSearch.DepProhibitedConnectionPoints.Count > 0)
                        {
                            strBuilder.Append("<ProhibitedConnectionPoints >");
                            foreach (var cp in advSearch.DepProhibitedConnectionPoints)
                            {
                                strBuilder.Append($"<ConnectionPoint><City Code=='{cp}' /></ConnectionPoint>");
                            }
                            strBuilder.Append("</ProhibitedConnectionPoints >");
                        }
                        strBuilder.Append("</AirLegModifiers>");
                    }
                }
                else
                {
                    if (advSearch.ArvPermittedConnectionPoints != null || advSearch.ArvProhibitedConnectionPoints != null)
                    {
                        strBuilder.Append("<AirLegModifiers>");
                        if (advSearch.ArvPermittedConnectionPoints != null && advSearch.ArvPermittedConnectionPoints.Count > 0)
                        {
                            strBuilder.Append("<PermittedConnectionPoints>");
                            foreach (var cp in advSearch.ArvPermittedConnectionPoints)
                            {
                                strBuilder.Append($"<ConnectionPoint><City Code=='{cp}' /></ConnectionPoint>");
                            }
                            strBuilder.Append("</PermittedConnectionPoints>");
                        }
                        if (advSearch.ArvProhibitedConnectionPoints != null && advSearch.ArvProhibitedConnectionPoints.Count > 0)
                        {
                            strBuilder.Append("<ProhibitedConnectionPoints >");
                            foreach (var cp in advSearch.ArvProhibitedConnectionPoints)
                            {
                                strBuilder.Append($"<ConnectionPoint><City Code=='{cp}' /></ConnectionPoint>");
                            }
                            strBuilder.Append("</ProhibitedConnectionPoints >");
                        }
                        strBuilder.Append("</AirLegModifiers>");
                    }

                }

            }


        }

        private static void ArvSearchDateTimeRequest(FlightSearchDetails flightSearchDetails, SearchType searchType, ITravelportEngine travelportEngine, StringBuilder strBuilder, Models.Settings.ProviderDetails.ProviderDetail details, string commonNamespace)
        {
            var advSearch = flightSearchDetails.AdvanceSearchModifier;
            bool isUpdated = false;
            if (advSearch != null)
            {
                if (advSearch.SearchArrTime != null)
                {
                    isUpdated = true;
                    var arvTime = advSearch.SearchArrTime;
                    if (arvTime.PreferredTime != null)
                        strBuilder.Append($"<SearchArvTime PreferredTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}{arvTime.PreferredTime}'");
                    else
                        strBuilder.Append("<SearchArvTime>");

                    if (arvTime.EarliestTime != null && arvTime.LatestTime != null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}{arvTime.EarliestTime}' LatestTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}{arvTime.LatestTime}'/>");
                    else if (arvTime.EarliestTime != null && arvTime.LatestTime == null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}{arvTime.EarliestTime}' LatestTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}{"T23:59:59.000"}'/>");
                    else if (arvTime.EarliestTime == null && arvTime.LatestTime != null)
                        strBuilder.Append($"<TimeRange xmlns='{commonNamespace}' EarliestTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}' LatestTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}{arvTime.LatestTime}'/>");

                    strBuilder.Append("</SearchArvTime>");
                }
            }
            if (!isUpdated)
            {
                if (flightSearchDetails.DepartureDate == flightSearchDetails.ReturnDate)
                {
                    strBuilder.Append($"<SearchDepTime PreferredTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}T17:00'/>");
                }
                else
                {
                    strBuilder.Append($"<SearchDepTime PreferredTime='{flightSearchDetails.ReturnDate.ToString("yyyy-MM-dd")}'/>");
                }
            }

        }

        private static void AirSearchModifierRequest(FlightSearchDetails flightSearchDetails, StringBuilder strBuilder, string commonNamespace, ITravelportEngine travelportEngine)
        {
            var advSearch = flightSearchDetails.AdvanceSearchModifier;
            if (advSearch != null)
            {
                List<string> airModifierOptions = new List<string>();
                if (advSearch.ExcludeGroundTransportation)
                    airModifierOptions.Add("ExcludeGroundTransportation='true'");
                if (advSearch.JetServiceOnly)
                    airModifierOptions.Add("JetServiceOnly='true'");
                if (advSearch.MaxJourneyTime_Hours > 0)
                    airModifierOptions.Add($"MaxJourneyTime='{advSearch.MaxJourneyTime_Hours}'");

                if (airModifierOptions.Count > 0)
                    strBuilder.Append($"<AirSearchModifiers {string.Join(" ", airModifierOptions)}>");
                else
                    strBuilder.Append("<AirSearchModifiers>");
                strBuilder.Append("<PreferredProviders>");
                strBuilder.Append($"<Provider xmlns='{commonNamespace}' Code='{travelportEngine.Domain}'/>");
                strBuilder.Append("</PreferredProviders>");
                if (advSearch.DirectFlights)
                    strBuilder.Append("<FlightType NonStopDirects='true' />");

                //if (advSearch.CabinClass != CabinClass.None)
                //{
                //    strBuilder.Append("<PreferredCabins>");
                //    strBuilder.Append($"<CabinClass xmlns='{commonNamespace}' Type='{(advSearch.CabinClass == CabinClass.EconomyCoach ? CabinClass.Economy : advSearch.CabinClass)}' />");
                //    strBuilder.Append("</PreferredCabins>");
                //}
                if (advSearch.PrefferedCarrierCodes != null && advSearch.PrefferedCarrierCodes.Count > 0)
                {
                    strBuilder.Append("<PermittedCarriers>");
                    foreach (var carrier in advSearch.PrefferedCarrierCodes)
                    {
                        strBuilder.Append($"<Carrier xmlns='{commonNamespace}'  Code='{carrier}' />");
                    }
                    strBuilder.Append("</PermittedCarriers>");
                }
                if (advSearch.ProhibitedCarrierCodes != null && advSearch.ProhibitedCarrierCodes.Count > 0)
                {
                    strBuilder.Append("<ProhibitedCarriers>");
                    foreach (var carrier in advSearch.ProhibitedCarrierCodes)
                    {
                        strBuilder.Append($"<Carrier xmlns='{commonNamespace}' Code='{carrier}' />");
                    }
                    strBuilder.Append("</ProhibitedCarriers>");
                }
                strBuilder.Append("</AirSearchModifiers>");
            }


        }

        private static void AddCabinClass(FlightSearchDetails flightSearchDetails, StringBuilder strBuilder, string commonNamespace)
        {
            strBuilder.Append("<AirLegModifiers AllowDirectAccess='true'>");
            strBuilder.Append("<PreferredCabins>");
            strBuilder.Append($"<CabinClass xmlns='{commonNamespace}' Type='{(flightSearchDetails.AdvanceSearchModifier.CabinClass == CabinClass.EconomyCoach ? CabinClass.Economy : flightSearchDetails.AdvanceSearchModifier.CabinClass)}' />");
            strBuilder.Append("</PreferredCabins>");
            strBuilder.Append("</AirLegModifiers>");
        }
        public void BuildHeader(IProvider provider, SearchType searchType)
        {
            var strBuilder = new StringBuilder();
            strBuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            strBuilder.Append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:eb=\"http://www.ebxml.org/namespaces/messageHeader\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\">");
            strBuilder.Append("<SOAP-ENV:Header/>");
            strBuilder.Append("<SOAP-ENV:Body>");
            strBuilder.Append("{0}");
            strBuilder.Append("</SOAP-ENV:Body>");
            strBuilder.Append("</SOAP-ENV:Envelope>");
            Header = strBuilder.ToString();
        }
    }
}
