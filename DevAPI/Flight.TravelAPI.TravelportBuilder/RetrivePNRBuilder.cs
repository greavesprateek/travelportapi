﻿using Flight.TravelAPI.Builder;
using Flight.TravelAPI.Builder.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Providers.Base;
using Flight.TravelAPI.Providers.Travelport.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using System.Xml.Linq;

namespace Flight.TravelAPI.TravelportBuilder
{
    public class RetrivePNRBuilder : BuilderBase, IRetrievePNRBuilder
    {
        private readonly IConfigurationData _configurationData;
        private readonly ILogger<TravelportSearchBuilder> _logger;

        public RetrivePNRBuilder(ILogger<TravelportSearchBuilder> logger, IConfigurationData configurationData)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationData = configurationData ?? throw new ArgumentNullException(nameof(configurationData));
        }

        public void BuildBody(string locatorCode, string partnerPCC, IProvider provider)
        {
            if (!(provider is ITravelportEngine travelportEngine)) return;
            var stringBuilder = new StringBuilder();
            try
            {
                string comNamespace = @"http://www.travelport.com/schema/common_v50_0";
                string univNamespace = @"http://www.travelport.com/schema/universal_v50_0";

                if (string.IsNullOrEmpty(partnerPCC))
                {
                    //<universal: ProviderCode="1P" >
                    //	<BillingPointOfSaleInfo OriginApplication="UAPI" xmlns="http://www.travelport.com/schema/common_v50_0"/>
                    //</universal:UniversalRecordImportReq>
                    stringBuilder.Append($"<UniversalRecordImportReq xmlns=\"{univNamespace}\"" +
                    $" ProviderLocatorCode=\"{locatorCode}\"" +
                    $" TargetBranch=\"{travelportEngine.BranchCode}\" ProviderCode=\"{travelportEngine.Domain}\">");
                    stringBuilder.Append($"<BillingPointOfSaleInfo xmlns=\"{comNamespace}\" OriginApplication='uAPI' />");
                    //stringBuilder.Append($"<OverridePCC xmlns=\"{comNamespace}\" ProviderCode='{travelportEngine.Domain}' PseudoCityCode='{"3BT"}' />");
                    stringBuilder.Append($"</UniversalRecordImportReq>");
                }
                else
                {
                    stringBuilder.Append($"<UniversalRecordRetrieveReq xmlns=\"{univNamespace}\"" +
                        $" TraceId=\"{Guid.NewGuid().ToString()}\" AuthorizedBy=\"user\"" +
                        $" TargetBranch=\"{travelportEngine.BranchCode}\" RetrieveProviderReservationDetails=\"false\"" +
                        $" ViewOnlyInd=\"false\">");
                    stringBuilder.Append($"<BillingPointOfSaleInfo xmlns=\"{comNamespace}\" OriginApplication='uAPI' />");

                    /*You have a CHOICE of the next 2 items at this level either send PNR OR UniversalRecord*/
                    stringBuilder.Append($"<UniversalRecordLocatorCode>{locatorCode}</UniversalRecordLocatorCode>");
                    //stringBuilder.Append($"<ProviderReservationInfo ProviderCode=\"{travelportSetting.Domain}\"" +
                    //    $" ProviderLocatorCode=\"{pnrShortDetail.PNRNumber}\">");
                    stringBuilder.Append($"</UniversalRecordRetrieveReq>");
                }
                Body = stringBuilder.ToString();
               // Body= XDocument.Load(@"D:\APIWork\TravelportRQRS\2021-10-10\M4R6OA_\Req.xml").ToString();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
        }

        public void BuildHeader()
        {
            var strBuilder = new StringBuilder();
            strBuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            strBuilder.Append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:eb=\"http://www.ebxml.org/namespaces/messageHeader\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\">");
            strBuilder.Append("<SOAP-ENV:Header/>");
            strBuilder.Append("<SOAP-ENV:Body>");
            strBuilder.Append("{0}");
            strBuilder.Append("</SOAP-ENV:Body>");
            strBuilder.Append("</SOAP-ENV:Envelope>");
            Header = strBuilder.ToString();
        }
    }
}