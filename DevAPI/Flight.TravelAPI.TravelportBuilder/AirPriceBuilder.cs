﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flight.TravelAPI.Builder;
using Flight.TravelAPI.Configuration.TravelportBookingWorkflow;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Providers.Base;
using Flight.TravelAPI.Providers.Travelport.Interfaces;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using Microsoft.Extensions.Logging;

namespace Flight.TravelAPI.TravelportBuilder
{
    public class AirPriceBuilder : BookingBuilderBase
    {
        private readonly ILogger<AirPriceBuilder> _logger;
        private readonly List<IWorkflow> _workflows;

        public AirPriceBuilder(ILogger<AirPriceBuilder> logger, WorkflowList workflowList)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            var _workflowList = workflowList ?? throw new ArgumentNullException(nameof(workflowList));
            _workflows = _workflowList.Workflows;
        }

        public override void BuildBody(IEngine engine, ProviderRequest providerRequest = null)
        {
            try
            {
                ITravelportEngine _providerEngine = (ITravelportEngine)_workflows
                    .Where(x => x.Engine.Engine == engine.Engine)
                    .Select(x => x.Engine).FirstOrDefault();
                StringBuilder stringBuilder = new StringBuilder();

                string airNamespace = @"http://www.travelport.com/schema/air_v50_0";
                string comNamespace = @"http://www.travelport.com/schema/common_v50_0";
                string traceId = new Guid().ToString();
                string fareType = "PublicAndPrivateFares";

                stringBuilder.Append($"<air:AirPriceReq xmlns:air=\"{airNamespace}\" xmlns:com=\"{comNamespace}\"" +
                    $" AuthorizedBy=\"user\" TargetBranch=\"{_providerEngine.BranchCode}\" TraceId=\"{traceId}\"" +
                    $" CheckFlightDetails=\"true\" CheckOBFees=\"All\">");
                stringBuilder.Append($"<com:BillingPointOfSaleInfo  OriginApplication='uAPI' />");
                stringBuilder.Append($"<air:AirItinerary>");
                int boundSegmentCount = 0;
                List<FlightSegments> flightSegmentList = new List<FlightSegments>();
                if (providerRequest.SelectedContract.TripDetails.OutBoundSegment.Any())
                {
                    flightSegmentList.AddRange(providerRequest.SelectedContract.TripDetails.OutBoundSegment);
                    string boundData = GetBoundData(providerRequest.SelectedContract.TripDetails.OutBoundSegment, "OutBound", boundSegmentCount, providerRequest);
                    if (boundData != null)
                        stringBuilder.Append(boundData);
                }
                if (providerRequest.SelectedContract.TripType == TripType.ROUNDTRIP.ToString()
                    && providerRequest.SelectedContract.TripDetails != null
                    && providerRequest.SelectedContract.TripDetails.InBoundSegment.Any())
                {
                    flightSegmentList.AddRange(providerRequest.SelectedContract.TripDetails.InBoundSegment);
                    string boundData = GetBoundData(providerRequest.SelectedContract.TripDetails.InBoundSegment, "InBound", boundSegmentCount, providerRequest);
                    if (boundData != null)
                        stringBuilder.Append(boundData);
                }
                stringBuilder.Append("</air:AirItinerary>");

                if (providerRequest.SelectedContract.PricingSource == "PRIVATE")
                    fareType = "PrivateFaresOnly";
                else if (providerRequest.SelectedContract.PricingSource == "PUBLISHED")
                    fareType = "PublicFaresOnly";

                stringBuilder.Append($"<air:AirPricingModifiers FaresIndicator=\"{fareType}\" InventoryRequestType=\"DirectAccess\">");
                stringBuilder.Append($"<air:BrandModifiers>");
                stringBuilder.Append($"<air:FareFamilyDisplay ModifierType=\"FareFamily\"/>");
                stringBuilder.Append($"</air:BrandModifiers>");
                /*   <air:BrandModifiers>
                			<air:FareFamilyDisplay ModifierType="FareFamily"/>
				</air:BrandModifiers>*/
                stringBuilder.Append($"</air:AirPricingModifiers>");

                /*foreach (FlightSegments flightSegment in flightSegmentList)
                    GetAirSegmentPricing(flightSegment, ref stringBuilder);*/

                GetPassengerDetails(PaxType.ADT, providerRequest.SelectedContract.AdultCount, ref stringBuilder);
                int childCount = providerRequest.SelectedContract.ChildCount + providerRequest.SelectedContract.Child12To17Count;
                GetPassengerDetails(PaxType.CNN, childCount, ref stringBuilder);
                GetPassengerDetails(PaxType.INF, providerRequest.SelectedContract.InfantCount, ref stringBuilder);
                GetPassengerDetails(PaxType.INS, providerRequest.SelectedContract.InfantInLapCount, ref stringBuilder);
                GetPassengerDetails(PaxType.SEN, providerRequest.SelectedContract.SeniorCount, ref stringBuilder);

                //stringBuilder.Append("<air:AirPricingCommand>");

                //if (providerRequest.SelectedContract.TripDetails.OutBoundSegment.Any())
                //    foreach (FlightSegments segment in providerRequest.SelectedContract.TripDetails.OutBoundSegment)
                //        GetAirSegmentPricing(segment, ref stringBuilder);

                //if (providerRequest.SelectedContract.TripDetails.InBoundSegment.Any())
                //    foreach (FlightSegments segment in providerRequest.SelectedContract.TripDetails.InBoundSegment)
                //        GetAirSegmentPricing(segment, ref stringBuilder);

                var cabinClass = providerRequest.SelectedContract.TripDetails.OutBoundSegment.Any()
                    ? providerRequest.SelectedContract.TripDetails.OutBoundSegment.FirstOrDefault().Class
                    : null;
                stringBuilder.Append($"<air:AirPricingCommand CabinClass=\"{cabinClass}\"/>");
                //stringBuilder.Append($"</air:AirPricingCommand>");
                stringBuilder.Append($"<com:FormOfPayment Type=\"Credit\" />");
                stringBuilder.Append($"</air:AirPriceReq>");

                Body = stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
        }

        public override void BuildHeader(IEngine engine, string binarySecurityToken = null)
        {
            var strBuilder = new StringBuilder();
            string envNamespace = @"http://schemas.xmlsoap.org/soap/envelope/";
            string ebNamespace = @"http://www.ebxml.org/namespaces/messageHeader/";
            string xlinkNamespace = @"http://www.w3.org/1999/xlink/";
            string xsdNamespace = @"http://www.w3.org/1999/XMLSchema/";
            strBuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            strBuilder.Append($"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"{envNamespace}\" xmlns:eb=\"{ebNamespace}\" xmlns:xlink=\"{xlinkNamespace}\" xmlns:xsd=\"{xsdNamespace}\">");
            strBuilder.Append("<SOAP-ENV:Header/>");
            strBuilder.Append("<SOAP-ENV:Body>");
            strBuilder.Append("{0}");
            strBuilder.Append("</SOAP-ENV:Body>");
            strBuilder.Append("</SOAP-ENV:Envelope>");
            Header = strBuilder.ToString();
        }

        #region Travelport Helper Functions
        private string GetBoundData(List<FlightSegments> flightSegmentList, string BoundType, int boundSegmentCount, ProviderRequest providerRequest)
        {
            string airSegmentAndPricing = null;
            try
            {
                if (flightSegmentList.Any())
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (var segment in flightSegmentList)
                    {
                        DateTime bound_departureDate = new DateTime(segment.DepartureDate.Year, segment.DepartureDate.Month, segment.DepartureDate.Day, segment.DepartureTime.Hours, segment.DepartureTime.Minutes, segment.DepartureTime.Seconds);
                        DateTime bound_arrivalDate = new DateTime(segment.ArrivalDate.Year, segment.ArrivalDate.Month, segment.ArrivalDate.Day, segment.ArrivalTime.Hours, segment.ArrivalTime.Minutes, segment.ArrivalTime.Seconds);

                        string bound_departureDate_string = String.Format("{0:s}", bound_departureDate);
                        string bound_arrivalDate_string = String.Format("{0:s}", bound_arrivalDate);
                        double flightDuration = segment.FlightDuration != null ? segment.FlightDuration.Value.TotalMinutes : 0;

                        stringBuilder.Append($"<air:AirSegment Key=\"{segment.SegmentKey}\" AvailabilitySource=\"{segment.AvailabilitySource}\"" +
                            $" Equipment=\"{segment.EquipmentType}\" Group=\"{segment.MarriageGroup}\" Carrier=\"{segment.MarketingCarrier.AirlineCode}\"" +
                            $" FlightNumber=\"{segment.FlightNumber}\" Origin=\"{segment.Origin}\" Destination=\"{segment.Destination}\"" +
                            $" DepartureTime=\"{String.Format("{0:s}", bound_departureDate_string)}\"" +
                            $" ArrivalTime=\"{String.Format("{0:s}", bound_arrivalDate_string)}\" FlightTime=\"{flightDuration}\"" +
                            $" Distance=\"{segment.Distance}\" ProviderCode=\"{segment.ProviderCode}\">");
                        //if(segment.ValidatingCarrier != null)
                        //    stringBuilder.Append($"<air:CodeshareInfo OperatingCarrier=\"{segment.ValidatingCarrier.AirlineCode}\"" +
                        //        $" OperatingFlightNumber=\"{segment.ValidatingCarrier.AirLineNumber}\">" +
                        //        //$"{}</air:CodeshareInfo>");
                        //        $"</air:CodeshareInfo>");

                        var segmentConnection = providerRequest.SelectedContract.ListOfSegmentIndex != null
                            ? providerRequest.SelectedContract.ListOfSegmentIndex
                            .Where(x => x.SegmentIndex == boundSegmentCount.ToString())
                            .FirstOrDefault()
                            : null;
                        if (segmentConnection != null)
                            stringBuilder.Append("<air:Connection />");
                        /*
                        //stringBuilder.Append($"<com:HostToken Host=\"{0}\" Key=\"{1}\" ElStat=\"{2}\" KeyOverride=\"{3}\"></com:HostToken>");
                        stringBuilder.Append($"<com:HostToken />");
                        //stringBuilder.Append($"< air:APISRequirements Key=\"{0}\" Level=\"{1}\" GenderRequired =\"{2}\" DateOfBirthRequired=\"{3}\" RequiredDocuments=\"{4}\" NationalityRequired=\"{5}\" >");
                        stringBuilder.Append($"<air:APISRequirements />");
                        //stringBuilder.Append($"<air:Document Sequence=\"{0}\" Type=\"{1}\" Level=\"{2}\" />");
                        //stringBuilder.Append($"</air:APISRequirements >");
                        */
                        stringBuilder.Append("</air:AirSegment>");
                        //GetAirSegmentPricing(segment, ref stringBuilder);
                        boundSegmentCount++;
                    }
                    airSegmentAndPricing = stringBuilder != null ? stringBuilder.ToString() : null;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return airSegmentAndPricing;
        }
        private void GetAirSegmentPricing(FlightSegments flightSegment, ref StringBuilder stringBuilder)
        {
            try
            {
                stringBuilder.Append($"<air:AirSegmentPricingModifiers AirSegmentRef=\"{flightSegment.SegmentKey}\"" +
                           $" FareBasisCode=\"{flightSegment.FareBasis}\" CabinClass=\"{flightSegment.Cabin}\">");
                stringBuilder.Append($"<air:PermittedBookingCodes>");
                stringBuilder.Append($"<air:BookingCode Code=\"{flightSegment.Class}\"/>");
                stringBuilder.Append($"</air:PermittedBookingCodes>");
                stringBuilder.Append($"</air:AirSegmentPricingModifiers>");
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
        }
        private void GetPassengerDetails(PaxType paxType, int paxCount, ref StringBuilder stringBuilder)
        {
            try
            {
                if (paxCount > 0)
                {
                    for (int passengerCount = 0; passengerCount < paxCount; passengerCount++)
                    {
                        /*var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes(
                        $"ADTPAX{passengerCount}adtpax{passengerCount + 1}PAXADT{passengerCount + 2}"));*/

                        var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes(
                            $"ADTPAX_{paxType.ToString()}_{passengerCount + 1}"));

                        stringBuilder.Append($"<com:SearchPassenger Code=\"{paxType.ToString()}\"" +
                            $" Key=\"{paxType.ToString() + "_" + passengerCount}\" BookingTravelerRef=\"{travelerRef}\" />");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
        }
        #endregion
    }
}
