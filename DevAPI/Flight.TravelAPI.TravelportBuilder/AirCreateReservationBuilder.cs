﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Flight.TravelAPI.Builder;
using Flight.TravelAPI.Configuration.TravelportBookingWorkflow;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Providers.Base;
using Flight.TravelAPI.Providers.Travelport.Interfaces;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using Microsoft.Extensions.Logging;

namespace Flight.TravelAPI.TravelportBuilder
{
    public class AirCreateReservationBuilder : BookingBuilderBase
    {
        private readonly ILogger<AirCreateReservationBuilder> _logger;
        private readonly List<IWorkflow> _workflows;
        public AirCreateReservationBuilder(ILogger<AirCreateReservationBuilder> logger, WorkflowList workflowList)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            var _workflowList = workflowList ?? throw new ArgumentNullException(nameof(workflowList));
            _workflows = _workflowList.Workflows;
        }

        public override void BuildBody(IEngine engine, ProviderRequest providerRequest = null)
        {
            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                ITravelportEngine _providerEngine = (ITravelportEngine)_workflows
                   .Where(x => x.Engine.Engine == engine.Engine)
                   .Select(x => x.Engine).FirstOrDefault();

                var selectedContract = providerRequest.SelectedContract;
                var flightBookingDetails = providerRequest.FlightBooking;
                //var flightSearch = providerRequest.FlightSearch != null
                //    ? providerRequest.FlightSearch
                //    : selectedContract.FlightSearchDetail;

                string univNamespace = @"http://www.travelport.com/schema/universal_v50_0";
                string airNamespace = @"http://www.travelport.com/schema/air_v50_0";
                string comNamespace = @"http://www.travelport.com/schema/common_v50_0";

                string currencyCode = selectedContract.AdultFare != null ? selectedContract.AdultFare.CurrencyCode : "USD";
                string providerCode = selectedContract.apigds_provider;
                string traceID =  Guid.NewGuid().ToString();
                stringBuilder.Append($"<univ:AirCreateReservationReq xmlns:univ=\"{univNamespace}\"" +
                    $" xmlns:air=\"{airNamespace}\"" +
                    $" xmlns:com=\"{comNamespace}\" " +
                    $"AuthorizedBy=\"user\" TargetBranch=\"{_providerEngine.BranchCode}\"" +
                    $" TraceId=\"{traceID}\"  ProviderCode=\"{providerCode}\" RetainReservation=\"Both\">");
                stringBuilder.Append($"<com:BillingPointOfSaleInfo OriginApplication=\"uAPI\" />");

                int travellerLoopCount = 0;
                var travelers = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "ADT" || x.PaxType == "CNN");
                int adult = 0;
                flightBookingDetails.PaymentSummary.Country = flightBookingDetails.PaymentSummary.Country.ToUpper() == "UK" ? "GB" : flightBookingDetails.PaymentSummary.Country;
                foreach (var passanger in travelers)
                {
                    string gender = (passanger.Gender.Equals("1", StringComparison.OrdinalIgnoreCase) || passanger.Gender.Equals("Male", StringComparison.OrdinalIgnoreCase)) ? "M" : "F";
                    var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes($"ADTPAX{adult}adtpax{adult + 1}PAXADT{adult + 2}"));
                    adult++;
                    var middleName = string.IsNullOrEmpty(passanger.MiddleName) ? "" : "Middle='" + passanger.MiddleName.Replace("-", " ").Replace("'", " ") + "'";
                    stringBuilder.Append($"<com:BookingTraveler Key='{travelerRef}' TravelerType='{passanger.PaxType}' DOB='{passanger.DOB.Date.ToString("yyyy-MM-dd")}' Gender='{gender}'");
                    if (passanger.PaxType == "CNN")
                    {
                        stringBuilder.Append($" Age='{CalculateAge(passanger.DOB)}'");
                    }
                    stringBuilder.Append(">");
                    stringBuilder.Append($"<com:BookingTravelerName First='{passanger.FirstName.Replace("-", " ").Replace("'", " ")}' Last='{passanger.LastName.Replace("-", " ").Replace("'", " ")}' {middleName}/>");
                    if (travellerLoopCount == 0)
                    {
                        stringBuilder.Append("<com:DeliveryInfo>");
                        stringBuilder.Append($"<com:ShippingAddress Key='{passanger.FirstName + passanger.LastName}'>");
                        stringBuilder.Append($"<com:Street>{flightBookingDetails.PaymentSummary.AddressLine1}</com:Street>");
                        stringBuilder.Append($"<com:City>{flightBookingDetails.PaymentSummary.City}</com:City>");
                        stringBuilder.Append($"<com:State>{flightBookingDetails.PaymentSummary.State}</com:State>");
                        stringBuilder.Append($"<com:PostalCode>{flightBookingDetails.PaymentSummary.ZipCode}</com:PostalCode>");
                        stringBuilder.Append($"<com:Country>{flightBookingDetails.PaymentSummary.Country}</com:Country>");
                        stringBuilder.Append("</com:ShippingAddress>");
                        stringBuilder.Append("</com:DeliveryInfo>");
                        stringBuilder.Append($"<com:PhoneNumber Number='{flightBookingDetails.PaymentSummary.BillingPhone}' />");
                    }
                    stringBuilder.Append($"<com:SSR Carrier='YY' FreeText='////{passanger.DOB.Date.ToString("ddMMMyy")}/{gender}//{passanger.LastName}/{passanger.FirstName}' Key='{passanger.FirstName + passanger.LastName}' Status='HK' Type='DOCS' />");

                    if (travellerLoopCount == 0)
                    {

                        stringBuilder.Append("<com:Address>");
                        stringBuilder.Append($"<com:AddressName>{flightBookingDetails.PaymentSummary.AddressLine1}</com:AddressName>");
                        stringBuilder.Append($"<com:Street>{flightBookingDetails.PaymentSummary.AddressLine1}</com:Street>");
                        stringBuilder.Append($"<com:City>{flightBookingDetails.PaymentSummary.City}</com:City>");
                        stringBuilder.Append($"<com:State>{flightBookingDetails.PaymentSummary.State}</com:State>");
                        stringBuilder.Append($"<com:PostalCode>{flightBookingDetails.PaymentSummary.ZipCode}</com:PostalCode>");
                        stringBuilder.Append($"<com:Country>{flightBookingDetails.PaymentSummary.Country}</com:Country>");
                        stringBuilder.Append("</com:Address>");
                    }
                    stringBuilder.Append("</com:BookingTraveler>");
                    travellerLoopCount++;
                }
                travelers = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "INS");
                int infant = 0;
                foreach (var passanger in travelers)
                {
                    flightBookingDetails.PaymentSummary.Country = flightBookingDetails.PaymentSummary.Country.ToUpper() == "UK" ? "GB" : flightBookingDetails.PaymentSummary.Country;
                    string gender = (passanger.Gender.Equals("1", StringComparison.OrdinalIgnoreCase) || passanger.Gender.Equals("Male", StringComparison.OrdinalIgnoreCase)) ? "MI" : "FI";
                    string gen = (passanger.Gender.Equals("1", StringComparison.OrdinalIgnoreCase) || passanger.Gender.Equals("Male", StringComparison.OrdinalIgnoreCase)) ? "M" : "F";
                    var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes($"INSPAX{infant}inspax{infant + 1}PAXINS{infant + 2}"));
                    infant++;
                    var middleName = string.IsNullOrEmpty(passanger.MiddleName) ? "" : "Middle='" + passanger.MiddleName.Replace("-", " ").Replace("'", " ") + "'";
                    stringBuilder.Append($"<com:BookingTraveler Key='{travelerRef}' TravelerType='{passanger.PaxType}' DOB='{passanger.DOB.Date.ToString("yyyy-MM-dd")}' Gender='{gen}' Age='{CalculateAge(passanger.DOB)}'>");
                    stringBuilder.Append($"<com:BookingTravelerName First='{passanger.FirstName.Replace("-", " ").Replace("'", " ")}' Last='{passanger.LastName.Replace("-", " ").Replace("'", " ")}' {middleName}/>");

                    stringBuilder.Append($"<com:SSR Carrier='YY' FreeText='////{passanger.DOB.Date.ToString("ddMMMyy")}/{gender}//{passanger.LastName}/{passanger.FirstName}' Key='{passanger.FirstName + passanger.LastName}' Status='HK' Type='DOCS' />");
                    stringBuilder.Append("<com:NameRemark>");
                    stringBuilder.Append($"<com:RemarkData>{passanger.DOB.ToString("ddMMMyy")}</com:RemarkData>");
                    stringBuilder.Append("</com:NameRemark>");
                    stringBuilder.Append("</com:BookingTraveler>");
                    travellerLoopCount++;
                }
                travelers = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "INF");
                infant = 0;
                foreach (var passanger in travelers)
                {
                    flightBookingDetails.PaymentSummary.Country = flightBookingDetails.PaymentSummary.Country.ToUpper() == "UK" ? "GB" : flightBookingDetails.PaymentSummary.Country;
                    string gender = (passanger.Gender.Equals("1", StringComparison.OrdinalIgnoreCase) || passanger.Gender.Equals("Male", StringComparison.OrdinalIgnoreCase)) ? "MI" : "FI";
                    string gen = (passanger.Gender.Equals("1", StringComparison.OrdinalIgnoreCase) || passanger.Gender.Equals("Male", StringComparison.OrdinalIgnoreCase)) ? "M" : "F";
                    var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes($"INFPAX{infant}infpax{infant + 1}PAXINF{infant + 2}"));
                    infant++;
                    var middleName = string.IsNullOrEmpty(passanger.MiddleName) ? "" : "Middle='" + passanger.MiddleName.Replace("-", " ").Replace("'", " ") + "'";
                    stringBuilder.Append($"<com:BookingTraveler Key='{travelerRef}' TravelerType='{passanger.PaxType}' DOB='{passanger.DOB.Date.ToString("yyyy-MM-dd")}' Gender='{gen}' Age='{CalculateAge(passanger.DOB)}'>");
                    stringBuilder.Append($"<com:BookingTravelerName First='{passanger.FirstName.Replace("-", " ").Replace("'", " ")}' Last='{passanger.LastName.Replace("-", " ").Replace("'", " ")}' {middleName}/>");
                    stringBuilder.Append($"<com:SSR Carrier='YY' FreeText='////{passanger.DOB.Date.ToString("ddMMMyy")}/{gender}//{passanger.LastName}/{passanger.FirstName}' Key='{passanger.FirstName + passanger.LastName}' Status='HK' Type='DOCS' />");
                    stringBuilder.Append("<com:NameRemark>");
                    stringBuilder.Append($"<com:RemarkData>{passanger.DOB.ToString("ddMMMyy")}</com:RemarkData>");
                    stringBuilder.Append("</com:NameRemark>");
                    stringBuilder.Append("</com:BookingTraveler>");
                    travellerLoopCount++;
                }
                travelers = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "CHD");
                int child = 0;
                foreach (var passanger in travelers)
                {
                    passanger.PaxType = "CNN";
                    flightBookingDetails.PaymentSummary.Country = flightBookingDetails.PaymentSummary.Country.ToUpper() == "UK" ? "GB" : flightBookingDetails.PaymentSummary.Country;
                    string gender = (passanger.Gender.Equals("1", StringComparison.OrdinalIgnoreCase) || passanger.Gender.Equals("Male", StringComparison.OrdinalIgnoreCase)) ? "M" : "F";
                    var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes($"CNNPAX{child}cnnpax{child + 1}PAXCNN{child + 2}"));
                    child++;
                    var middleName = string.IsNullOrEmpty(passanger.MiddleName) ? "" : "Middle='" + passanger.MiddleName.Replace("-", " ").Replace("'", " ") + "'";
                    stringBuilder.Append($"<com:BookingTraveler Key='{travelerRef}' TravelerType='{passanger.PaxType}' DOB='{passanger.DOB.Date.ToString("yyyy-MM-dd")}' Gender='{gender}' Age='{CalculateAge(passanger.DOB)}'>");
                    stringBuilder.Append($"<com:BookingTravelerName First='{passanger.FirstName.Replace("-", " ").Replace("'", " ")}' Last='{passanger.LastName.Replace("-", " ").Replace("'", " ")}' {middleName}/>");
                    stringBuilder.Append($"<com:SSR Carrier='YY' FreeText='////{passanger.DOB.Date.ToString("ddMMMyy")}/{gender}//{passanger.LastName}/{passanger.FirstName}' Key='{passanger.FirstName + passanger.LastName}' Status='HK' Type='DOCS' />");
                    stringBuilder.Append("<com:NameRemark>");
                    stringBuilder.Append($"<com:RemarkData>{passanger.DOB.ToString("ddMMMyy")}</com:RemarkData>");
                    stringBuilder.Append("</com:NameRemark>");
                    stringBuilder.Append("</com:BookingTraveler>");
                    travellerLoopCount++;
                }

                var adultTraveller = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "ADT").First();
                if (adultTraveller.PaxType != "INF" && adultTraveller.PaxType != "INS")
                {
                    stringBuilder.Append($"<com:GeneralRemark ProviderCode=\"{providerCode}\" Key=\"{(adultTraveller.FirstName + adultTraveller.LastName)}\" Category=\"E\" TypeInGds=\"Alpha\">");
                    stringBuilder.Append($"<com:RemarkData>{flightBookingDetails.PaymentSummary.BillingPhone}</com:RemarkData>");
                    stringBuilder.Append("</com:GeneralRemark>");
                }
                //stringBuilder.Append("<com:AgencyContactInfo>");
                //stringBuilder.Append($"<com:PhoneNumber CountryCode=\"{0}\" AreaCode=\"{1}\"  Key=\"11\" Number=\"{2}-{3}\" Text=\"TravelportUAPI\" Type=\"Agency\" />", ConfigManager.AgencyNumber[0], ConfigManager.AgencyNumber[1], ConfigManager.AgencyNumber[2], ConfigManager.AgencyNumber[3]));
                //stringBuilder.Append(" </com:AgencyContactInfo>");

                #region Credit Card Information For Payment
                //string cardExpiry = flightBookingDetails.PaymentSummary.ExpiryYear.ToString() + "-" + flightBookingDetails.PaymentSummary.ExpiryMonth.ToString("00");
                //stringBuilder.Append($"<com:FormOfPayment Type=\"Credit\" Key=\"1\">");
                //stringBuilder.Append($"<com:CreditCard CVV=\"{flightBookingDetails.PaymentSummary.CVVNumber}\"" +
                //    $" ExpDate=\"{cardExpiry}\" Number=\"{flightBookingDetails.PaymentSummary.CardNumber}\"" +
                //    $" Name=\"{flightBookingDetails.PaymentSummary.CCHolderName}\"" +
                //    $" Type=\"{flightBookingDetails.PaymentSummary.CardType}\"/>");
                //stringBuilder.Append("</com:FormOfPayment>");
                #endregion

                #region Check Payment, If Enable
                stringBuilder.Append($"<com:FormOfPayment Type=\"Check\" Key=\"1\">");
                stringBuilder.Append($"<com:Check CheckNumber=\"1234567\" AccountNumber=\"7890\" RoutingNumber=\"456\"/>");
                stringBuilder.Append($"</com:FormOfPayment>");
                #endregion


                stringBuilder.Append(string.Format("<air:AirPricingSolution Key='{0}' TotalPrice='{1}' BasePrice='{2}' ApproximateTotalPrice='{3}' ApproximateBasePrice='{4}' EquivalentBasePrice='{5}' Taxes='{6}'",
                    selectedContract.AirPricingKey,
                    string.Format("{0}{1:0.00}", currencyCode, selectedContract.TotalBaseFare + selectedContract.TotalTax),
                    selectedContract.ActualCurrencyBaseFare,
                    string.Format("{0}{1:0.00}", currencyCode, selectedContract.TotalBaseFare + selectedContract.TotalTax),
                    string.Format("{0}{1:0.00}", currencyCode, selectedContract.TotalBaseFare),
                    selectedContract.EquivalentBasePrice,
                    string.Format("{0}{1:0.00}", currencyCode, selectedContract.TotalTax)));
                if (!string.IsNullOrEmpty(selectedContract.Fees))
                {
                    stringBuilder.Append(string.Format(" Fees='{0}'", selectedContract.Fees));
                }
                if (!string.IsNullOrEmpty(selectedContract.ApproximateTaxes))
                {
                    stringBuilder.Append(string.Format(" ApproximateTaxes='{0}'", selectedContract.ApproximateTaxes));
                }
                if (!string.IsNullOrEmpty(selectedContract.QuoteDate))
                {
                    stringBuilder.Append(string.Format(" QuoteDate='{0}'", selectedContract.QuoteDate));
                }
                stringBuilder.Append(">");

                int boundCount = 0;
                if (selectedContract.TripDetails != null && selectedContract.TripDetails.OutBoundSegment != null)
                {
                    string boundData = GetBoundData(selectedContract.TripDetails.OutBoundSegment, selectedContract, ref boundCount);
                    if (!string.IsNullOrEmpty(boundData))
                        stringBuilder.Append(boundData);
                }
                if (selectedContract.TripDetails != null && selectedContract.TripDetails.InBoundSegment != null)
                {
                    string boundData = GetBoundData(selectedContract.TripDetails.InBoundSegment, selectedContract, ref boundCount);
                    if (!string.IsNullOrEmpty(boundData))
                        stringBuilder.Append(boundData);
                }

                if (selectedContract.AdultCount > 0)
                {
                    var fare = selectedContract.AdultFare;
                    var tax = string.Format("{0}{1:0.00}", currencyCode, fare.Tax);
                    var approximateTotalPrice = string.Format("{0}{1:0.00}", currencyCode, fare.TotalFare);
                    var approximateBasePrice = string.Format("{0}{1:0.00}", currencyCode, fare.BaseFare);
                    stringBuilder.Append($"<air:AirPricingInfo Key='{fare.AirPricingInfoKey}' TotalPrice='{string.Format("{0}{1:0.00}", currencyCode, fare.TotalFare)}' BasePrice='{fare.ActualCurrencyBaseFare}' ApproximateTotalPrice='{approximateTotalPrice}' ApproximateBasePrice='{approximateBasePrice}'");
                    if (!string.IsNullOrEmpty(fare.EquivalentBasePrice))
                    {
                        stringBuilder.Append($" EquivalentBasePrice='{fare.EquivalentBasePrice}'");
                    }
                    if (!string.IsNullOrEmpty(fare.ApproximateTaxes))
                    {
                        stringBuilder.Append($" ApproximateTaxes='{fare.ApproximateTaxes}'");
                    }
                    stringBuilder.Append($" Taxes='{tax}'");
                    if (fare.LatestTicketingTime > new DateTime())
                    {
                        stringBuilder.Append(string.Format(" LatestTicketingTime='{0:s}'", fare.LatestTicketingTime));
                    }
                    if (!string.IsNullOrEmpty(fare.PricingMethod))
                    {
                        stringBuilder.Append($" PricingMethod='{fare.PricingMethod}'");
                    }
                    stringBuilder.Append($" Refundable='{fare.Refundable.ToString().ToLower()}'");
                    if (!string.IsNullOrEmpty(fare.IncludesVAT))
                    {
                        stringBuilder.Append($" IncludesVAT='{fare.IncludesVAT}'");
                    }
                    if (!string.IsNullOrEmpty(fare.ETicketability))
                    {
                        stringBuilder.Append($" ETicketability='{fare.ETicketability}'");
                    }
                    if (!string.IsNullOrEmpty(fare.PlatingCarrier))
                    {
                        stringBuilder.Append($" PlatingCarrier='{fare.PlatingCarrier}'");
                    }
                    stringBuilder.Append($" ProviderCode='{fare.ProviderCode ?? providerCode}'");
                    stringBuilder.Append(">");
                    foreach (var fareInfo in selectedContract.AdultFare.FareInfo)
                    {
                        if (string.IsNullOrEmpty(fareInfo.DepartureDate)) fareInfo.DepartureDate = "0001-01-01";
                        var effectiveDate = String.Format("{0:s}", fareInfo.FareEffectiveDate);
                        stringBuilder.Append($"<air:FareInfo Key='{fareInfo.FareRefKey}' FareBasis='{fareInfo.FareBasis}' PassengerTypeCode='ADT' Origin='{fareInfo.Origin}' Destination='{fareInfo.Destination}' EffectiveDate='{effectiveDate}' DepartureDate='{fareInfo.DepartureDate}' Amount='{fareInfo.FareAmount}'");
                        if(!string.IsNullOrEmpty(fareInfo.NegotiatedFare))
                        {
                            stringBuilder.Append($" NegotiatedFare='{fareInfo.NegotiatedFare}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.NotValidBefore))
                        {
                            stringBuilder.Append($" NotValidBefore='{fareInfo.NotValidBefore}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.NotValidAfter))
                        {
                            stringBuilder.Append($" NotValidAfter='{fareInfo.NotValidAfter}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.TaxAmount))
                        {
                            stringBuilder.Append($" TaxAmount='{fareInfo.TaxAmount}'");
                        }
                        stringBuilder.Append(">");
                        stringBuilder.Append(string.Format("<air:FareRuleKey FareInfoRef='{0}' ProviderCode='{1}'>{2}</air:FareRuleKey>"
                            , fareInfo.FareRefKey, fareInfo.ProviderCode, fareInfo.FareInfoValue));
                        stringBuilder.Append("</air:FareInfo>");
                    }
                    foreach (var booking in selectedContract.AdultFare.ListOfBookingInfo)
                    {
                        stringBuilder.Append(string.Format("<air:BookingInfo BookingCode='{0}' CabinClass='{1}' FareInfoRef='{2}' SegmentRef='{3}' HostTokenRef='{4}' />"
                            , booking.BookingCode, booking.CabinClass, booking.FareInfoRef, booking.SegmentRef, booking.HostTokenRef));
                    }
                    var passengerList = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "ADT");
                    adult = 0;
                    foreach (var pass in passengerList)
                    {
                        var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("ADTPAX{0}adtpax{1}PAXADT{2}", adult, adult + 1, adult + 2)));
                        stringBuilder.Append($"<air:PassengerType Code='ADT' BookingTravelerRef='{travelerRef}'/>");
                        adult++;
                    }
                    stringBuilder.Append("</air:AirPricingInfo>");
                }

                if (selectedContract.InfantCount > 0)
                {
                    var fare = selectedContract.InfantFare;
                    var tax = string.Format("{0}{1:0.00}", currencyCode, fare.Tax);
                    var approximateTotalPrice = string.Format("{0}{1:0.00}", currencyCode, fare.TotalFare);
                    var approximateBasePrice = string.Format("{0}{1:0.00}", currencyCode, fare.BaseFare);

                    stringBuilder.Append($"<air:AirPricingInfo Key='{fare.AirPricingInfoKey}' TotalPrice='{string.Format("{0}{1:0.00}", currencyCode, fare.TotalFare)}' BasePrice='{fare.ActualCurrencyBaseFare}' ApproximateTotalPrice='{approximateTotalPrice}' ApproximateBasePrice='{approximateBasePrice}'");
                    if (!string.IsNullOrEmpty(fare.EquivalentBasePrice))
                    {
                        stringBuilder.Append($" EquivalentBasePrice='{fare.EquivalentBasePrice}'");
                    }
                    if (!string.IsNullOrEmpty(fare.ApproximateTaxes))
                    {
                        stringBuilder.Append($" ApproximateTaxes='{fare.ApproximateTaxes}'");
                    }
                    stringBuilder.Append($" Taxes='{tax}'");
                    if (fare.LatestTicketingTime > new DateTime())
                    {
                        stringBuilder.Append(string.Format(" LatestTicketingTime='{0:s}'", fare.LatestTicketingTime));
                    }
                    if (!string.IsNullOrEmpty(fare.PricingMethod))
                    {
                        stringBuilder.Append($" PricingMethod='{fare.PricingMethod}'");
                    }
                    stringBuilder.Append($" Refundable='{fare.Refundable.ToString().ToLower()}'");
                    if (!string.IsNullOrEmpty(fare.IncludesVAT))
                    {
                        stringBuilder.Append($" IncludesVAT='{fare.IncludesVAT}'");
                    }
                    if (!string.IsNullOrEmpty(fare.ETicketability))
                    {
                        stringBuilder.Append($" ETicketability='{fare.ETicketability}'");
                    }
                    if (!string.IsNullOrEmpty(fare.PlatingCarrier))
                    {
                        stringBuilder.Append($" PlatingCarrier='{fare.PlatingCarrier}'");
                    }
                    stringBuilder.Append($" ProviderCode='{fare.ProviderCode ?? providerCode}'");
                    stringBuilder.Append(">");

                    foreach (var fareInfo in selectedContract.InfantFare.FareInfo)
                    {
                        var effectiveDate = String.Format("{0:s}", fareInfo.FareEffectiveDate);
                        stringBuilder.Append($"<air:FareInfo Key='{fareInfo.FareRefKey}' FareBasis='{fareInfo.FareBasis}' PassengerTypeCode='INS' Origin='{fareInfo.Origin}' Destination='{fareInfo.Destination}' EffectiveDate='{effectiveDate}' DepartureDate='{fareInfo.DepartureDate}' Amount='{fareInfo.FareAmount}'");
                        if (!string.IsNullOrEmpty(fareInfo.NegotiatedFare))
                        {
                            stringBuilder.Append($" NegotiatedFare='{fareInfo.NegotiatedFare}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.NotValidBefore))
                        {
                            stringBuilder.Append($" NotValidBefore='{fareInfo.NotValidBefore}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.NotValidAfter))
                        {
                            stringBuilder.Append($" NotValidAfter='{fareInfo.NotValidAfter}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.TaxAmount))
                        {
                            stringBuilder.Append($" TaxAmount='{fareInfo.TaxAmount}'");
                        }
                        stringBuilder.Append(">");
                        stringBuilder.Append(string.Format("<air:FareRuleKey FareInfoRef='{0}' ProviderCode='{1}'>{2}</air:FareRuleKey>"
                            , fareInfo.FareRefKey, fareInfo.ProviderCode, fareInfo.FareInfoValue));
                        stringBuilder.Append("</air:FareInfo>");
                    }
                    foreach (var booking in selectedContract.InfantFare.ListOfBookingInfo)
                    {
                        stringBuilder.Append(string.Format("<air:BookingInfo BookingCode='{0}' CabinClass='{1}' FareInfoRef='{2}' SegmentRef='{3}' HostTokenRef='{4}' />"
                            , booking.BookingCode, booking.CabinClass, booking.FareInfoRef, booking.SegmentRef, booking.HostTokenRef));
                    }
                    var infantPassengerList = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "INS");
                    infant = 0;
                    foreach (var pass in infantPassengerList)
                    {
                        var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("INSPAX{0}inspax{1}PAXINS{2}", infant, infant + 1, infant + 2)));
                        stringBuilder.Append($"<air:PassengerType Code='INS' BookingTravelerRef='{travelerRef}'  Age='{CalculateAge(pass.DOB)}'/>");
                        infant++;
                    }
                    stringBuilder.Append("</air:AirPricingInfo>");
                }

                if (selectedContract.InfantInLapCount > 0)
                {
                    var fare = selectedContract.InfantInLapFare;
                    var tax = string.Format("{0}{1:0.00}", currencyCode, fare.Tax);
                    var approximateTotalPrice = string.Format("{0}{1:0.00}", currencyCode, fare.TotalFare);
                    var approximateBasePrice = string.Format("{0}{1:0.00}", currencyCode, fare.BaseFare);
                    stringBuilder.Append($"<air:AirPricingInfo Key='{fare.AirPricingInfoKey}' TotalPrice='{string.Format("{0}{1:0.00}", currencyCode, fare.TotalFare)}' BasePrice='{fare.ActualCurrencyBaseFare}' ApproximateTotalPrice='{approximateTotalPrice}' ApproximateBasePrice='{approximateBasePrice}'");
                    if (!string.IsNullOrEmpty(fare.EquivalentBasePrice))
                    {
                        stringBuilder.Append($" EquivalentBasePrice='{fare.EquivalentBasePrice}'");
                    }
                    if (!string.IsNullOrEmpty(fare.ApproximateTaxes))
                    {
                        stringBuilder.Append($" ApproximateTaxes='{fare.ApproximateTaxes}'");
                    }
                    stringBuilder.Append($" Taxes='{tax}'");
                    if (fare.LatestTicketingTime > new DateTime())
                    {
                        stringBuilder.Append(string.Format(" LatestTicketingTime='{0:s}'", fare.LatestTicketingTime));
                    }
                    if (!string.IsNullOrEmpty(fare.PricingMethod))
                    {
                        stringBuilder.Append($" PricingMethod='{fare.PricingMethod}'");
                    }
                    stringBuilder.Append($" Refundable='{fare.Refundable.ToString().ToLower()}'");
                    if (!string.IsNullOrEmpty(fare.IncludesVAT))
                    {
                        stringBuilder.Append($" IncludesVAT='{fare.IncludesVAT}'");
                    }
                    if (!string.IsNullOrEmpty(fare.ETicketability))
                    {
                        stringBuilder.Append($" ETicketability='{fare.ETicketability}'");
                    }
                    if (!string.IsNullOrEmpty(fare.PlatingCarrier))
                    {
                        stringBuilder.Append($" PlatingCarrier='{fare.PlatingCarrier}'");
                    }
                    stringBuilder.Append($" ProviderCode='{fare.ProviderCode ?? providerCode}'");
                    stringBuilder.Append(">");

                    foreach (var fareInfo in selectedContract.InfantInLapFare.FareInfo)
                    {
                        var effectiveDate = String.Format("{0:s}", fareInfo.FareEffectiveDate);
                        stringBuilder.Append($"<air:FareInfo Key='{fareInfo.FareRefKey}' FareBasis='{fareInfo.FareBasis}' PassengerTypeCode='INF' Origin='{fareInfo.Origin}' Destination='{fareInfo.Destination}' EffectiveDate='{effectiveDate}' DepartureDate='{fareInfo.DepartureDate}' Amount='{fareInfo.FareAmount}'");
                        if (!string.IsNullOrEmpty(fareInfo.NegotiatedFare))
                        {
                            stringBuilder.Append($" NegotiatedFare='{fareInfo.NegotiatedFare}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.NotValidBefore))
                        {
                            stringBuilder.Append($" NotValidBefore='{fareInfo.NotValidBefore}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.NotValidAfter))
                        {
                            stringBuilder.Append($" NotValidAfter='{fareInfo.NotValidAfter}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.TaxAmount))
                        {
                            stringBuilder.Append($" TaxAmount='{fareInfo.TaxAmount}'");
                        }
                        stringBuilder.Append(">");

                        stringBuilder.Append(string.Format("<air:FareRuleKey FareInfoRef='{0}' ProviderCode='{1}'>{2}</air:FareRuleKey>"
                            , fareInfo.FareRefKey, fareInfo.ProviderCode, fareInfo.FareInfoValue));
                        stringBuilder.Append("</air:FareInfo>");
                    }
                    foreach (var booking in selectedContract.InfantInLapFare.ListOfBookingInfo)
                    {
                        stringBuilder.Append(string.Format("<air:BookingInfo BookingCode='{0}' CabinClass='{1}' FareInfoRef='{2}' SegmentRef='{3}' HostTokenRef='{4}' />"
                            , booking.BookingCode, booking.CabinClass, booking.FareInfoRef, booking.SegmentRef, booking.HostTokenRef));
                    }
                    var infantPassengerList = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "INF");
                    infant = 0;
                    foreach (var pass in infantPassengerList)
                    {
                        var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("INFPAX{0}infpax{1}PAXINF{2}", infant, infant + 1, infant + 2)));
                        stringBuilder.Append($"<air:PassengerType Code='INF' BookingTravelerRef='{travelerRef}'  Age='{CalculateAge(pass.DOB)}'/>");
                        infant++;
                    }
                    stringBuilder.Append("</air:AirPricingInfo>");
                }

                if (selectedContract.ChildCount > 0 || selectedContract.Child12To17Count > 0)
                {
                    var fare = selectedContract.ChildFare;
                    var tax = string.Format("{0}{1:0.00}", currencyCode, fare.Tax);
                    var approximateTotalPrice = string.Format("{0}{1:0.00}", currencyCode, fare.TotalFare);
                    var approximateBasePrice = string.Format("{0}{1:0.00}", currencyCode, fare.BaseFare);
                    stringBuilder.Append($"<air:AirPricingInfo Key='{fare.AirPricingInfoKey}' TotalPrice='{string.Format("{0}{1:0.00}", currencyCode, fare.TotalFare)}' BasePrice='{fare.ActualCurrencyBaseFare}' ApproximateTotalPrice='{approximateTotalPrice}' ApproximateBasePrice='{approximateBasePrice}'");
                    if (!string.IsNullOrEmpty(fare.EquivalentBasePrice))
                    {
                        stringBuilder.Append($" EquivalentBasePrice='{fare.EquivalentBasePrice}'");
                    }
                    if (!string.IsNullOrEmpty(fare.ApproximateTaxes))
                    {
                        stringBuilder.Append($" ApproximateTaxes='{fare.ApproximateTaxes}'");
                    }
                    stringBuilder.Append($" Taxes='{tax}'");
                    if (fare.LatestTicketingTime > new DateTime())
                    {
                        stringBuilder.Append(string.Format(" LatestTicketingTime='{0:s}'", fare.LatestTicketingTime));
                    }
                    if (!string.IsNullOrEmpty(fare.PricingMethod))
                    {
                        stringBuilder.Append($" PricingMethod='{fare.PricingMethod}'");
                    }
                    stringBuilder.Append($" Refundable='{fare.Refundable.ToString().ToLower()}'");
                    if (!string.IsNullOrEmpty(fare.IncludesVAT))
                    {
                        stringBuilder.Append($" IncludesVAT='{fare.IncludesVAT}'");
                    }
                    if (!string.IsNullOrEmpty(fare.ETicketability))
                    {
                        stringBuilder.Append($" ETicketability='{fare.ETicketability}'");
                    }
                    if (!string.IsNullOrEmpty(fare.PlatingCarrier))
                    {
                        stringBuilder.Append($" PlatingCarrier='{fare.PlatingCarrier}'");
                    }
                    stringBuilder.Append($" ProviderCode='{fare.ProviderCode ?? providerCode}'");
                    stringBuilder.Append(">");

                    foreach (var fareInfo in selectedContract.ChildFare.FareInfo)
                    {
                        var effectiveDate = String.Format("{0:s}", fareInfo.FareEffectiveDate);
                        stringBuilder.Append($"<air:FareInfo Key='{fareInfo.FareRefKey}' FareBasis='{fareInfo.FareBasis}' PassengerTypeCode='CNN' Origin='{fareInfo.Origin}' Destination='{fareInfo.Destination}' EffectiveDate='{effectiveDate}' DepartureDate='{fareInfo.DepartureDate}' Amount='{fareInfo.FareAmount}'");
                        if (!string.IsNullOrEmpty(fareInfo.NegotiatedFare))
                        {
                            stringBuilder.Append($" NegotiatedFare='{fareInfo.NegotiatedFare}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.NotValidBefore))
                        {
                            stringBuilder.Append($" NotValidBefore='{fareInfo.NotValidBefore}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.NotValidAfter))
                        {
                            stringBuilder.Append($" NotValidAfter='{fareInfo.NotValidAfter}'");
                        }
                        if (!string.IsNullOrEmpty(fareInfo.TaxAmount))
                        {
                            stringBuilder.Append($" TaxAmount='{fareInfo.TaxAmount}'");
                        }
                        stringBuilder.Append(">");
                        stringBuilder.Append(string.Format("<air:FareRuleKey FareInfoRef='{0}' ProviderCode='{1}'>{2}</air:FareRuleKey>"
                            , fareInfo.FareRefKey, fareInfo.ProviderCode, fareInfo.FareInfoValue));
                        stringBuilder.Append("</air:FareInfo>");
                    }
                    foreach (var booking in selectedContract.ChildFare.ListOfBookingInfo)
                    {
                        stringBuilder.Append(string.Format("<air:BookingInfo BookingCode='{0}' CabinClass='{1}' FareInfoRef='{2}' SegmentRef='{3}' HostTokenRef='{4}' />"
                            , booking.BookingCode, booking.CabinClass, booking.FareInfoRef, booking.SegmentRef, booking.HostTokenRef));
                    }
                    var childPassengerList = flightBookingDetails.TravellerDetails.Where(x => x.PaxType == "CNN" || x.PaxType == "CHD");
                    child = 0;
                    foreach (var pass in childPassengerList)
                    {
                        var travelerRef = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("CNNPAX{0}cnnpax{1}PAXCNN{2}", child, child + 1, child + 2)));
                        stringBuilder.Append($"<air:PassengerType Code='CNN' BookingTravelerRef='{travelerRef}'  Age='{CalculateAge(pass.DOB)}'/>");
                        child++;
                    }
                    stringBuilder.Append("</air:AirPricingInfo>");
                }

                foreach (var hosted in selectedContract.ListOfHostToken)
                {
                    stringBuilder.Append($"<com:HostToken Key=\"{hosted.HostTokenKey}\">{hosted.HostTokenValue}</com:HostToken>");
                }
                stringBuilder.Append($"</air:AirPricingSolution>");

                //if (firstDepDate > DateTime.Now.AddDays(2))
                //{
                //    DateTime ttlDate = DateTime.Now.AddDays(2);

                //    stringBuilder.Append($"<com:ActionStatus Type='TAW' TicketDate='{0}{1}{2}' ProviderCode='1P'/>", ttlDate.ToString("yyyy-MM-dd"), "T", ttlDate.ToString("hh:mm:ss")));
                //}
                //else
                //{
                //    stringBuilder.Append("<com:ActionStatus  Type='ACTIVE' TicketDate='T*' ProviderCode='1P' />");
                //}
                stringBuilder.Append($"<com:ActionStatus Type=\"ACTIVE\" TicketDate=\"T*\" ProviderCode=\"{providerCode}\" />");
                stringBuilder.Append($"<com:Payment Key=\"2\" Type=\"Itinerary\" FormOfPaymentRef=\"1\" Amount=\"{currencyCode + Convert.ToString(selectedContract.TotalGDSFareV2)}\" />");
                stringBuilder.Append($"</univ:AirCreateReservationReq>");
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            Body = stringBuilder.ToString();
        }

        public override void BuildHeader(IEngine engine, string binarySecurityToken = null)
        {
            var stringBuilder = new StringBuilder();
            string envNamespace = @"http://schemas.xmlsoap.org/soap/envelope/";
            string ebNamespace = @"http://www.ebxml.org/namespaces/messageHeader/";
            string xlinkNamespace = @"http://www.w3.org/1999/xlink/";
            string xsdNamespace = @"http://www.w3.org/1999/XMLSchema/";
            stringBuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            stringBuilder.Append($"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"{envNamespace}\" xmlns:eb=\"{ebNamespace}\" xmlns:xlink=\"{xlinkNamespace}\" xmlns:xsd=\"{xsdNamespace}\">");
            stringBuilder.Append("<SOAP-ENV:Header/>");
            stringBuilder.Append("<SOAP-ENV:Body>");
            stringBuilder.Append("{0}");
            stringBuilder.Append("</SOAP-ENV:Body>");
            stringBuilder.Append("</SOAP-ENV:Envelope>");
            Header = stringBuilder.ToString();
        }

        private string GetBoundData(List<FlightSegments> boundSegments, FlightContracts selectedContract, ref int boundCount)
        {
            string boundData = null;
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var boundSegment in boundSegments)
                {
                    DateTime depDate = new DateTime(boundSegment.DepartureDate.Year, boundSegment.DepartureDate.Month, boundSegment.DepartureDate.Day, boundSegment.DepartureTime.Hours, boundSegment.DepartureTime.Minutes, boundSegment.DepartureTime.Seconds);
                    DateTime arrDate = new DateTime(boundSegment.ArrivalDate.Year, boundSegment.ArrivalDate.Month, boundSegment.ArrivalDate.Day, boundSegment.ArrivalTime.Hours, boundSegment.ArrivalTime.Minutes, boundSegment.ArrivalTime.Seconds);
                    string departureDate = string.Format("{0:s}", depDate);
                    string arrivalDate = string.Format("{0:s}", arrDate);
                    stringBuilder.Append($"<air:AirSegment Key='{boundSegment.SegmentKey}' Group='{boundSegment.MarriageGroup}' Carrier='{boundSegment.MarketingCarrier.AirlineCode}' FlightNumber='{boundSegment.FlightNumber}' ProviderCode='{boundSegment.ProviderCode}' Origin='{boundSegment.Origin}' Destination='{boundSegment.Destination}' DepartureTime='{String.Format("{0:s}", departureDate)}' ArrivalTime='{String.Format("{0:s}", arrivalDate)}' ");
                    if (!string.IsNullOrEmpty(boundSegment.FlightTime))
                    {
                        stringBuilder.Append($" FlightTime='{boundSegment.FlightTime}'");
                    }
                    if (!string.IsNullOrEmpty(boundSegment.TravelTime))
                    {
                        stringBuilder.Append($" TravelTime='{boundSegment.TravelTime}'");
                    }
                    if (!string.IsNullOrEmpty(boundSegment.Distance))
                    {
                        stringBuilder.Append(string.Format(" Distance='{0}'", boundSegment.Distance));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.ClassOfService))
                    {
                        stringBuilder.Append(string.Format(" ClassOfService='{0}'", boundSegment.ClassOfService));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.Equipment))
                    {
                        stringBuilder.Append(string.Format(" Equipment='{0}'", boundSegment.Equipment));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.ChangeOfPlane))
                    {
                        stringBuilder.Append(string.Format(" ChangeOfPlane='{0}'", boundSegment.ChangeOfPlane));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.OptionalServicesIndicator))
                    {
                        stringBuilder.Append(string.Format(" OptionalServicesIndicator='{0}'", boundSegment.OptionalServicesIndicator));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.AvailabilitySource))
                    {
                        stringBuilder.Append(string.Format(" AvailabilitySource='{0}'", boundSegment.AvailabilitySource));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.ParticipantLevel))
                    {
                        stringBuilder.Append(string.Format(" ParticipantLevel='{0}'", boundSegment.ParticipantLevel));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.LinkAvailability))
                    {
                        stringBuilder.Append(string.Format(" LinkAvailability='{0}'", boundSegment.LinkAvailability));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.PolledAvailabilityOption))
                    {
                        stringBuilder.Append(string.Format(" PolledAvailabilityOption='{0}'", boundSegment.PolledAvailabilityOption));
                    }
                    if (!string.IsNullOrEmpty(boundSegment.AvailabilityDisplayType))
                    {
                        stringBuilder.Append(string.Format(" AvailabilityDisplayType='{0}'", boundSegment.AvailabilityDisplayType));
                    }
                    stringBuilder.Append(">");
                    if (selectedContract.ListOfSegmentIndex != null)
                    {
                        string boundCount_string = boundCount.ToString();
                        var segmentConnection = selectedContract.ListOfSegmentIndex.Where(x => x.SegmentIndex == boundCount_string).FirstOrDefault();
                        if (segmentConnection != null)
                        {
                            stringBuilder.Append($"<air:Connection />");
                        }
                    }
                    stringBuilder.Append($"</air:AirSegment>");
                    boundCount++;
                }
                boundData = stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return boundData;
        }

        public static int CalculateAge(DateTime BirthDate)
        {
            int YearsPassed = DateTime.Now.Year - BirthDate.Year;
            // Are we before the birth date this year? If so subtract one year from the mix
            if (DateTime.Now.Month < BirthDate.Month || (DateTime.Now.Month == BirthDate.Month && DateTime.Now.Day < BirthDate.Day))
            {
                YearsPassed--;
            }
            return YearsPassed == 0 ? 1 : YearsPassed;
        }

    }
}
