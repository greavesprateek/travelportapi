﻿using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Providers.Base;
using System.Collections.Generic;

namespace Flight.TravelAPI.Directors.Interface
{
    public interface IDirector
    {
        string Construct(ProcessMode processMode, string userName, string userPwd, string pccCode, string domain);

        string Construct(ProcessMode processMode, FlightSearchDetails flightSearchDetails, IProvider provider,
            SearchType searchType, KeyValuePair<string, string> qualifier = default(KeyValuePair<string, string>));
        string Construct(IEngine engine, ProviderRequest ProviderReq, ProcessMode processMode);
        string Construct(ProcessMode processMode,IProvider provider, string locatorCode,string partnerPCC );
    }
}