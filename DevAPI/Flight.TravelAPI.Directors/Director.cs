﻿using System;
using System.Collections.Generic;
using Flight.TravelAPI.Builder.Interface;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Providers.Base;
using static Flight.TravelAPI.Builder.BuilderDelegate;

namespace Flight.TravelAPI.Directors
{
    public class Director : IDirector
    {
        private readonly BuilderResolver _builderResolver;

        public Director(BuilderResolver builderResolver)
        {
            _builderResolver = builderResolver ?? throw new ArgumentNullException(nameof(builderResolver));
        }

        public string Construct(ProcessMode processMode, string userName, string userPwd, string pccCode, string domain)
        {
            if (!(_builderResolver(processMode) is ISessionBuilder builder)) return null;
            builder.BuildHeader(userName, userPwd, pccCode, domain);
            builder.BuildBody();
            return builder.GetResult();
        }

        public string Construct(ProcessMode processMode, FlightSearchDetails flightSearchDetails, IProvider provider,
            SearchType searchType, KeyValuePair<string, string> qualifier = default(KeyValuePair<string, string>))
        {
            dynamic builder;
            switch (processMode)
            {
                case ProcessMode.TravelportSearch:
                    builder = _builderResolver(processMode) as ISearchBuilder;
                    if (builder == null) return null;
                    builder.BuildHeader(provider, searchType);
                    builder.BuildBody(flightSearchDetails, provider, searchType, qualifier);
                    return builder.GetResult();
                    //break;
                default:
                    return builder = null;
                    //break;
            }
        }

        public string Construct(IEngine engine, ProviderRequest providerRequest, ProcessMode processMode)
        {
            var builder = _builderResolver(processMode) as IBookingBuilder;
            if (builder == null) return null;
            builder.BuildHeader(engine, providerRequest.SelectedContract.Provider_SesionID);
            builder.BuildBody(engine, providerRequest);
            return builder.GetResult();
        }

        public string Construct( ProcessMode processMode, IProvider provider,string locatorCode,string partnerPCC )
        {
            var builder = _builderResolver(processMode) as IRetrievePNRBuilder;
            if (builder == null) return null;
            builder.BuildHeader();
            builder.BuildBody(locatorCode,partnerPCC, provider);
            return builder.GetResult();
        }

        //public string Construct(ProcessMode processMode,string pnr)
        //{
        //    dynamic builder;
        //    switch (processMode)
        //    {
        //        case ProcessMode.TravelportSearch:
        //            builder = _builderResolver(processMode) as ISearchBuilder;
        //            if (builder == null) return null;
        //            builder.BuildHeader(null, null);
        //            builder.BuildBody(flightSearchDetails, provider, searchType, qualifier);
        //            return builder.GetResult();
        //        //break;
        //        default:
        //            return builder = null;
        //            //break;
        //    }
        //}
    }
}