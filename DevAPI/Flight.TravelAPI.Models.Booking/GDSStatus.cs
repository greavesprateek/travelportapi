﻿namespace Flight.TravelAPI.Models.Booking
{
    public enum GDSStatus : byte
    {
        None = 0,
        Held = 6,
        Confirmed = 7,
        Pending_Confirmation = 12,
        Cancelled = 14,
        Expired = 17,
        Test = 18,
        Ticketed = 19,
        Fraud = 20,
        In_Progress = 21,
        Shipping_In_Progress = 22,
        Credit_Card_Denied = 23,
        Ticket_With_Supplier = 24,
        Failed = 25
    }
}
