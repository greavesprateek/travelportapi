﻿using Flight.TravelAPI.Models.Enumerations;

namespace Flight.TravelAPI.Models.Booking
{
    public class PaymentDetails
    {
        public int PaymentId { get; set; }
        public int TransactionID { get; set; }
        public string PaymentMode { get; set; }
        public float TotalAmount { get; set; }
        public PaidFor PaymentPaidFor { get; set; }
        public string CardCode { get; set; }

        public string PaymentMethod { get; set; }
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string CVVNumber { get; set; }
    }
}
