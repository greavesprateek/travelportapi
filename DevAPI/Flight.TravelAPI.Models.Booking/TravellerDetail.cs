﻿using System;

namespace Flight.TravelAPI.Models.Booking
{
    public class TravellerDetail
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public string PaxType { get; set; }
        public string Email { get; set; }
        public bool IsChild12To17Age { get; set; }
        public RequiredFieldsToBook requiredFieldsToBook { get; set; }
    }
}
