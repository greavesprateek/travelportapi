﻿using System;

namespace Flight.TravelAPI.Models.Booking
{
    public class SignInToken
    {
        public string SessionId { get; set; }
        public int SeqNumber { get; set; }
        public string Token { get; set; }
        public DateTime SessionTime { get; set; }

        public SignInToken()
        {
            this.SessionTime = DateTime.Now.AddMinutes(11);
        }

        public bool IsSessionAvailable
        {
            get
            {
                return this.SessionTime > DateTime.Now ? true : false;
            }
        }
    }
}
