﻿using System;

namespace Flight.TravelAPI.Models.Booking
{
    public class ResponseData
    {
        public string SearchGuid { get; set; }
        public string Engine { get; set; }
        public string StepName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string BookingStatus { get; set; }
        public string GDSStatus { get; set; }
        public string GDSConfirmationNo { get; set; }
        public string PNR { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsPriceChanged { get; set; }
        public bool IsSoldOut { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
    }
}
