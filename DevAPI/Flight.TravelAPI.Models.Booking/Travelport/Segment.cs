﻿using Flight.TravelAPI.Models.Response;
using System;

namespace Flight.TravelAPI.Models.Booking.Travelport
{
    public class Segment
    {
        public string AirSegmentKey { get; set; }
        public int Group { get; set; }
        public string Carrier { get; set; }
        public string FlightNumber { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string FlightTime { get; set; }
        public string Distance { get; set; }
        public string Equipment { get; set; }
        public string ChangeOfPlane { get; set; }
        public string FlightDetailsRef { get; set; }
        public string AvailabilitySource { get; set; }
        public string ProviderCode { get; set; }
        public Airline OperatingCarrier { get; set; }
        public string OptionalServicesIndicator { get; set; }
        public string AvailabilityDisplayType { get; set; }
        public string TravelTime { get; set; }
        public string ClassOfService { get; set; }
        public string ETicketability { get; set; }
        public string ParticipantLevel { get; set; }
        public string LinkAvailability { get; set; }
        public string PolledAvailabilityOption { get; set; }
    }
}
