﻿namespace Flight.TravelAPI.Models.Booking.Travelport
{
    public class TaxInfo
    {
        public string Category { get; set; }
        public string Amount { get; set; }
    }
}
