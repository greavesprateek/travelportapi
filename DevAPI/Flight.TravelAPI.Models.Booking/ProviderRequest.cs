﻿using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Response;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Booking
{
    public class ProviderRequest
    {
        public string SearchGuid { get; set; }
        public ContractDetails SelectedContract { get; set; }//Fare Verification Contract object
        public FlightBookDetails FlightBooking { get; set; }//Flight Booking Object
    }
}
