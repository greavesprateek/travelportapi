﻿
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Response;
using System;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Booking
{
    public class FlightContracts1
    {
        public FlightContracts1()
        {
            IsNearByAirport = false;
        }
        public int ContractID { get; set; }
        public string Provider { get; set; }
        public int ItemNumber { get; set; }
        public SignInToken AmadeusSessionToken { get; set; }
        public string GTT_SesionID { get; set; }
        public string GTT_key { get; set; }
        public string Origin { get; set; }
        public string OriginCityName { get; set; }
        public string Destination { get; set; }
        public string DestinationCityName { get; set; }

        public string OriginCityCodeSearch { get; set; }
        public string DestinationCityCodeSearch { get; set; }

        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }

        public TripDetails TripDetails { get; set; }

        public Airline ValidatingCarrier { get; set; }
        //public string ValidatingCarrierName { get; set; }
        public string FareType { get; set; }
        public string TripType { get; set; }

        public int AdultCount { get; set; }
        public int SeniorCount { get; set; }
        public int ChildCount { get; set; }
        public int Child12To17Count { get; set; }
        public int InfantCount { get; set; }
        public int InfantInLapCount { get; set; }

        public string FareBasisCode { get; set; }
        public FareDetails AdultFare { get; set; }
        public FareDetails ChildFare { get; set; }
        public FareDetails Child12To17Fare { get; set; }
        public FareDetails InfantFare { get; set; }
        public FareDetails InfantInLapFare { get; set; }
        public FareDetails SeniorFare { get; set; }
        //todo check
        public FlightSearchDetails FlightSearchDetail { get; set; }
        public float TotalMarkup { get; set; }
        public float TotalSupplierFee { get; set; }
        public float TotalBaseFare { get; set; }
        public float TotalTax { get; set; }
        public float TotalGDSFareV2 { get; set; }

        //added new prop need to initialize...
        public bool IsPubtonetEnabled { get; set; }
        public bool IsPubToNetContract { get; set; }
        public int EnginePriority { get; set; }
        public string Contractkey { get; set; }
        public string DatesKey { get; set; }
        public bool IsNearByAirport { get; set; }
        public bool IsContractWithoutTime { get; set; }

        //public bool IsRefundable { get; set; }
        //public bool isETicket { get; set; }
        // public EngineType Engine { get; set; }
        //TODO Check wheather it is used or not
        //public bool IsNearByAirportContract { get; set; }

        public int MinSeatAvailableForContract { get; set; }
        public int MaxNoOfStopsInContract { get; set; }
        public bool IsAlternateDateContract { get; set; }
        public bool IsMultipleAirlineContract { get; set; }
        public bool IsContractContainsLCCAirline { get; set; }

        public bool IsRateRuleCompleted { get; set; }
        //public bool IsContractSoldOutAfterBook { get; set; }

        //used in transam request/response
        public string JourneyType { get; set; }
        public bool PaymentRequired { get; set; }
        public string PricingSource { get; set; }
        public string RequestedPaxCodes { get; set; }
        public byte FareCategoryID { get; set; }

        //Time Duration of Flight
        public TimeSpan FlightDuration { get; set; }
        public TimeSpan TotalOutBoundFlightDuration { get; set; }
        public TimeSpan TotalInBoundFlightDuration { get; set; }
        public TimeSpan CompleteTimeDuration { get; set; }
        public bool IsContractSoldOutOrUnavailable { get; set; }
        public BookingStatus SelectedContractBookingStatus { get; set; }

        //new added
        public float TransactionFee { get; set; }
        public float MinCommissionFee { get; set; }
        public bool IsFlightSoldOrUnavailable { get; set; }
        public float CheapestPriceFromActualAirport { get; set; }
        public bool IsCouponApplied { get; set; }
        public float CouponAmount { get; set; }
        public string CouponCode { get; set; }
        public bool IsInsuranceApplied { get; set; }
        public float InsuranceAmount { get; set; }
        public float InsuranceAmountPerPerson { get; set; }
        public string Affiliate { get; set; }
        public string GDSResponseXML { get; set; }
        public bool IsPriceChanged { get; set; }
        //public FLightInsurance FlightInsurance { get; set; }
        //GTT Property
        public string apigds_provider { get; set; }
        public string lastTicketDate { get; set; }
        public bool domesticTicket { get; set; }
        public bool layover { get; set; }
        public bool operatedBy { get; set; }
        public bool overnightFlight { get; set; }
        public bool overnightStay { get; set; }
        public bool studentTicket { get; set; }
        public bool majorAirline { get; set; }
        public bool IsCreditCardAcceptable { get; set; }
        public PriceChangeRules rules { get; set; }
        public List<string> priceBaggageInfo { get; set; }
        public float TotalGDSQuotedAmount { get; set; }
        //Sabre Property
        public string SABRE_SesionID { get; set; }

        public string CorporateID { get; set; }
        public string AccountCode { get; set; }
        // for seat map
        public int TransactionID { get; set; }

        //AccessFare Property
        public string AccessFare_SesionID { get; set; }
        /// <summary>
        /// AirPricingSolution's key is AirPricingKey, which is required for booking
        /// </summary>
        public string AirPricingKey { get; set; }
        public List<HostToken> ListOfHostToken { get; set; }
        public List<Connection> ListOfSegmentIndex { get; set; }
        public string PNR { get; set; }
        public string TrxId { get; set; }
        public bool IsPassport { get; set; }
        public string Fees { get; set; }
        public string ApproximateTaxes { get; set; }
        public string QuoteDate { get; set; }
        public string ActualCurrencyBaseFare { get; set; }
        public string EquivalentBasePrice { get; set; }
    }
}
