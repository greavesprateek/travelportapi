﻿
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Response;
using System;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Booking
{
    public class BookingResponse
    {
        public bool IsSuccess { get; set; }
        public Error Errors { get; set; }
        public string PNR { get; set; }
        public int NoOfTST { get; set; }
        public float FareDifference { get; set; }
        public bool IsPriceChanged { get; set; }
        public bool IsContractSoldOutOrUnavailable { get; set; }
        public string ResponseXML { get; set; }
        public BookingStatus CurrentBookingStatus { get; set; }
        public GDSStatus GDSStatus { get; set; }
        public string GDSConfirmationNo { get; set; }
        public ContractDetails PriceChangedContract { get; set; }
        public string customerSessionId { get; set; }

        public int TransactionId { get; set; }
        public Guid TransactionGuid { get; set; }
        public DateTime BookingDate { get; set; }
        public ContractDetails SoldOutContract { get; set; }
        public List<AncillarySeats> AncillarySeats { get; set; }
        public string Version { get; set; }
        public List<TravellerDetails> Travelers { get; set; }
        //public FlightContracts SelectedContract { get; set; }
    }
}
