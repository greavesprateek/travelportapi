﻿namespace Flight.TravelAPI.Models.Booking
{
    public class PaymentSummary
    {
       
        public string CCHolderName { get; set; }
        public string CardNumber { get; set; }
        public string CVVNumber { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }

       //Address
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string BillingPhone { get; set; }
        public string CardType { get; set; }
        public string BillingCountryPhoneCode { get; set; }
    }
}
