﻿using Flight.TravelAPI.Models.Enumerations;

namespace Flight.TravelAPI.Models.Booking
{
    public class Error
    {
        public ErrorCategory Category { get; set; }
        public ErrorType Type { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
