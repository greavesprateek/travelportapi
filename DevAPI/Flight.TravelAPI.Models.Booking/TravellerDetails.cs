﻿using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Travelport;
using System;

namespace Flight.TravelAPI.Models.Booking
{
    public class TravellerDetails
    {
        public int PaxOrder { get; set; }
        public string PaxType { get; set; }
        public string PaxTItle { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public string FrequentFlyerNumber { get; set; }
        public string PassportNumber { get; set; }
        public string PassportExpireDate { get; set; }
        public string PassportIssuedBy { get; set; }
        public string EMail { get; set; }
        public string MealPreference { get; set; }
        public string SpecialPreference { get; set; }
        public string TicketNo { get; set; }
        public bool IsPassport { get; set; }
        public string PassengerNationality { get; set; }
        public string PassportIssueCountry { get; set; }

        public TravellerDetails(PaxType paxType_)
        {
            PaxType = paxType_.ToString();
        }
        public SSR SSR { get; set; }
        public Address ShippingAddress { get; set; }
        public Address Address { get; set; }
        public int Age { get; set; }
    }
}
