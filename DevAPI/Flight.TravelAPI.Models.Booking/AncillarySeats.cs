﻿namespace Flight.TravelAPI.Models.Booking
{
    public class AncillarySeats
    {
        public int PaxOrderId { get; set; }
        public int PassengerTatto { get; set; }
        public int SegmentOrderId { get; set; }
        public int SegmentTatto { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PaxType { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string SeatLocation { get; set; }
        public decimal Amount { get; set; }
        public bool? IsChargeable { get; set; }
        public bool? IsRefundable { get; set; }
        public bool? IsCommissionable { get; set; }
        public bool IsSeatAvailable { get; set; }
        public string Status { get; set; }
        public string SegmentType { get; set; }
        public string FlightNo { get; set; }
        public string SeatNo { get; set; }
        public string BookingResponseXML { get; set; }
    }
}
