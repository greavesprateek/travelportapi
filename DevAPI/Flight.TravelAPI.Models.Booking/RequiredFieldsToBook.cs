﻿namespace Flight.TravelAPI.Models.Booking
{
    public class RequiredFieldsToBook
    {
        public bool Passport { get; set; }
        public string PassportNo { get; set; }
        public string PassengerNationality { get; set; }
        public string IssueCountry { get; set; }
        public string ExpiredOn { get; set; }
    }
}
