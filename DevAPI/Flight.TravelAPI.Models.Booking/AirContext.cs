﻿using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Response;

namespace Flight.TravelAPI.Models.Booking
{
    public class AirContext
    {
        public FlightSearchDetails FlightSearchDetails { get; set; }
        public FlightContracts SelectedContract { get; set; }
        public FlightBookDetails BookDetails { get; set; }
        public BookingResponse BookingResponse { get; set; }
        public string APIServer { get; set; }
        public string AppServer { get; set; }
    }
}
