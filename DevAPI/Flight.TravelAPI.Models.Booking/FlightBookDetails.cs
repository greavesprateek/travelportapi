﻿using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Models.Travelport;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Booking
{
    public class FlightBookDetails
    {
        public List<TravellerDetails> TravellerDetails { get; set; }
        public PaymentSummary PaymentSummary { get; set; }
       

    }

    //public class AddressInfo
    //{
    //    public Address BillingAddress { get; set; }
    //    public Address ShippingAddress { get; set; }
    //}
}
