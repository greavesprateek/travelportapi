﻿using Flight.TravelAPI.Models.Response;
using System;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Booking
{
    public class FlightSegments1
    {
        public int Id { get; set; }
        public bool IsReturn { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
        public Airline MarketingCarrier { get; set; }
        public Airline OperatingCarrier { get; set; }
        public Airline ValidatingCarrier { get; set; }
        public TimeSpan DepartureTime { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public string StopOverTime { get; set; }
        public string OutTerminal { get; set; }
        public string InTerminal { get; set; }
        public string EquipmentType { get; set; }
        public string FlightNumber { get; set; }
        public string CnxType { get; set; }
        public string FareBasis { get; set; }
        public string Class { get; set; }
        public string Cabin { get; set; }
        public string Flightindicator { get; set; }
        public string Origin { get; set; }
        public string OriginCity { get; set; }
        public string Destination { get; set; }
        public string DestinationCity { get; set; }
        public string PNR { get; set; }
        public bool IsTripFlexible { get; set; }
        public TimeSpan? FlightDuration { get; set; }
        public int AvailableSeats { get; set; }
        public string companyFranchiseDetails { get; set; }
        public int NoOfStops { get; set; }
        public int SegmentID { get; set; }
        public string MarriageGroup { get; set; }
        public bool majorAirline { get; set; }
        public bool IsSoldOut { get; set; }
        public List<TechnicalStop> TechnicalStop { get; set; }
        // For Sabre
        public string AirEquipmentTypeCode { get; set; }
        ///* For TravelPort */

        public string ProviderCode { get; set; }
        /// <summary>
        /// Distance is flight segment distance origin to destination 
        /// </summary>
        public string Distance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AvailabilitySource { get; set; }
        public string SegmentKey { get; set; }
        public string HostedTokenKey { get; set; }
        public string ETicketability { get; set; }
        public string ChangeOfPlane { get; set; }
        public string ParticipantLevel { get; set; }
        public string LinkAvailability { get; set; }
        public string PolledAvailabilityOption { get; set; }
        public string OptionalServicesIndicator { get; set; }
        public string AvailabilityDisplayType { get; set; }
        public string FlightTime { get; set; }
        public string TravelTime { get; set; }
        public string ClassOfService { get; set; }
        public string Equipment { get; set; }

    }
}
