﻿using System;

namespace Flight.TravelAPI.Models.Booking
{
    public class AirFailedBooking
    {
        public int PortalID { get; set; }
        public int ProductType { get; set; }
        public int Engine { get; set; }
        public string GDSErrorCode { get; set; }
        public string GDSErrorMessage { get; set; }
        public string FailureReason { get; set; }
        public byte TripType { get; set; }
        public string SourceCode { get; set; }
        public string DestinationCode { get; set; }
        public DateTime DeptDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public int InfantCount { get; set; }
        public bool IsDomestic { get; set; }
        public int MFSErrorCode { get; set; }
        public string Description { get; set; }
        public string DisplayMsg { get; set; }
        public string SearchDetails { get; set; }
        public string BookingDetails { get; set; }
        public string MachineName { get; set; }
        public string UTM_Source { get; set; }
        public int NoOfInfantInLap { get; set; }
        public string ValidatingCarrier { get; set; }
        public string Airline { get; set; }
        public DateTime InsertedOn { get; set; }
        public string FareBasis { get; set; }
        public string CabinClass { get; set; }
        public string GDSResponseXML { get; set; }
        public string GDSSessionID { get; set; }
        public string TravelerDetails { get; set; }
        public string CustomerContactDetails { get; set; }
        public float GDSPrice { get; set; }
        public string UTM_Campaign { get; set; }
        public string ClientIPAddress { get; set; }

        public string APIServer { get; set; }
        public string AppServer { get; set; }
        public string AgentUserName { get; set; }
        public string FlightGuid { get; set; }
    }
}
