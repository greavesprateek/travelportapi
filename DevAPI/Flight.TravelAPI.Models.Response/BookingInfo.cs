﻿namespace Flight.TravelAPI.Models.Response
{
    public class BookingInfo
    {
        public string BookingCode { get; set; }
        public string BookingCount { get; set; }
        public string CabinClass { get; set; }
        public string FareInfoRef { get; set; }
        public string FareInfoValue { get; set; }
        public string SegmentRef { get; set; }
        public string HostTokenRef { get; set; }
    }
}