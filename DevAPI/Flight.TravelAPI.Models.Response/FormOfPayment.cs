﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flight.TravelAPI.Models.Response
{
    public class FormOfPayment
    {
        
        public string Key { get; set; }
        public string ProfileKey { get; set; }
        public string  Type { get; set; }
        public bool Resuable { get; set; }
        public Check Check { get; set; }
    }

    public class Check
    {
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string CheckNumber { get; set; }
    }
}
