﻿using System;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Response
{
    public class AgencyInfo
    {
        public AgencyInfo()
        {
            AgentActions = new List<AgentAction>();
        }
        public List<AgentAction> AgentActions { get; set; }
    }
    public class AgentAction
    {
        public string ActionType { get; set; }
        public string AgentCode { get; set; }
        public string BranchCode { get; set; }
        public string AgencyCode { get; set; }
        public DateTime EventTime { get; set; }
    }
}
