﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flight.TravelAPI.Models.Response
{
    public class ContractDetails:FlightContracts
    {
        public string PNR { get; set; }
        public string UniversalLocatorCode { get; set; }
        public string PCC { get; set; }
        public string PartnerPCC { get; set; }
        public AgencyInfo AgencyInfo { get; set; }
        public List<FormOfPayment> FormOfPayments { get; set; }
    }
}
