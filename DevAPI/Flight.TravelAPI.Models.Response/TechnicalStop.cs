﻿using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Response
{
    public class TechnicalStop
    {
        public TechnicalStop()
        {
            LstStopDetails = new List<StopDetails>();
        }

        public List<StopDetails> LstStopDetails { get; set; }
    }
}