﻿namespace Flight.TravelAPI.Models.Response
{
    public class FareInfo
    {
        public string ResBookDesigCode { get; set; }
        public string Cabin { get; set; }
        public int SeatsRemaining { get; set; }
    }
}