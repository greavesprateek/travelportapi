﻿using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Response
{
    public class TripDetails
    {
        public TripDetails()
        {
            OutBoundSegment = new List<FlightSegments>();
            InBoundSegment = new List<FlightSegments>();
        }

        public List<FlightSegments> OutBoundSegment { get; set; }
        public List<FlightSegments> InBoundSegment { get; set; }
    }
}