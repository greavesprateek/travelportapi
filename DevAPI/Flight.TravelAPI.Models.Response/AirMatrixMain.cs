﻿using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Response
{
    public class AirMatrixMain
    {
        public AirMatrixMain()
        {
            AirlineMatrixList = new List<AirlineMatrixColumn>();
        }

        public AirMatrixMain AirlineMatrixMain { get; set; }
        public bool IsNonStopAvailable { get; set; }
        public bool IsOneStopAvailable { get; set; }
        public bool IsMultiStopAvailable { get; set; }
        public List<AirlineMatrixColumn> AirlineMatrixList { get; set; }
        public string TripType { get; set; }
    }
}