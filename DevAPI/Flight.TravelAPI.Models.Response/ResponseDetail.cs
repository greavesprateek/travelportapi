﻿using System;

namespace Flight.TravelAPI.Models.Response
{
    public class ResponseDetail
    {
        public int PortalId { get; set; }
        public string APIServerName { get; set; }
        public string SearchGuid { get; set; }
        public string Provider { get; set; }
        public string RequestType { get; set; }
        public string Process { get; set; }
        public  DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public long TimeTaken { get; set; }
        public int Results { get; set; }
        public string XMLData { get; set; }
    }
}
