﻿using System;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Response
{
    public class FareDetails
    {
        public int passengerCount { get; set; }
        public string PaxType { get; set; }
        public float ActualBaseFare { get; set; }
        public float DISCOUNT_MARKUP_AMOUNT { get; set; }
        public bool IS_FIXED_DISCOUNT { get; set; }
        public float BaseFare { get; set; }
        public float Tax { get; set; }
        public float TotalFare { get; set; }
        public float Markup { get; set; }
        public float SupplierFee { get; set; }
        public string FareCode { get; set; }
        public string CurrencyCode { get; set; }
        public string AlternateCurrencyType { get; set; }
        public float AlternateCurrencyPrice { get; set; }

        public float TotalFareV2 => BaseFare + Markup + SupplierFee + Tax;

        public bool IsYouth { get; set; }
        public string AirPricingInfoKey { get; set; }
        public string ProviderCode { get; set; }
        public List<FareInfoRule> FareInfo { get; set; }
        public List<BookingInfo> ListOfBookingInfo { get; set; }
        public string ApproximateTaxes { get; set; }
        public DateTime LatestTicketingTime { get; set; }
        public string PricingMethod { get; set; }
        public string IncludesVAT { get; set; }
        public string PlatingCarrier { get; set; }
        public string ActualCurrencyBaseFare { get; set; }
        public string EquivalentBasePrice { get; set; }
        public bool Refundable { get; set; }
        public string ETicketability { get; set; }
    }
}