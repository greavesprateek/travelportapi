﻿namespace Flight.TravelAPI.Models.Response
{
    public class AirlineMatrixColumn
    {
        public Airline AirlineInfo { get; set; }
        public bool IsNonStop { get; set; }
        public float NonStopFare { get; set; }
        public bool IsOneStop { get; set; }
        public float OneStopFare { get; set; }
        public bool IsMultiStop { get; set; }
        public float MultiStopFare { get; set; }
        public string AirlineImageUrl { get; set; }
        public float AirlineMinPrice { get; set; }
        public bool IsMultipleAirline { get; set; }
    }
}