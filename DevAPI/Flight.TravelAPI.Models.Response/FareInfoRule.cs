﻿using System;

namespace Flight.TravelAPI.Models.Response
{
    public class FareInfoRule
    {
        public string Destination { get; set; }
        public string Origin { get; set; }
        public string FareBasis { get; set; }
        public string FareAmount { get; set; }
        public string ProviderCode { get; set; }
        public string FareRefKey { get; set; }
        public string FareInfoValue { get; set; }
        public DateTime FareEffectiveDate { get; set; }
        public string NegotiatedFare { get; set; }
        public string NotValidBefore { get; set; }
        public string NotValidAfter { get; set; }
        public string TaxAmount { get; set; }
        public string DepartureDate { get; set; }
    }
}