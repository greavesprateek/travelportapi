﻿using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Response
{
    public class ResponseWrapper
    {
        public List<FlightContracts> FlightContracts { get; set; }

    }
}