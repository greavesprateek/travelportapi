﻿namespace Flight.TravelAPI.Models.Response
{
    public class Airline
    {
        public string AirlineCode { get; set; }
    }
}