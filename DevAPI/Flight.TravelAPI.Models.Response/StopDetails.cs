﻿namespace Flight.TravelAPI.Models.Response
{
    public class StopDetails
    {
        public string DateQualifier { get; set; }
        public string Date { get; set; }
        public string FirstTime { get; set; }
        public string LocationId { get; set; }
    }
}