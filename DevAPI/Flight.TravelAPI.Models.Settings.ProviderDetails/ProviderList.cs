﻿using Flight.TravelAPI.Providers.Base;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Settings.ProviderDetails
{
    public class ProviderList
    {
        public List<IProvider> Providers { get; set; }
    }
}