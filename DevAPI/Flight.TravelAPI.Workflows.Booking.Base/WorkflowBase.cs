﻿using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Parsers.Parser;
using Flight.TravelAPI.Providers.Base;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;

namespace Flight.TravelAPI.Workflows.Booking.Base
{
    public abstract class WorkflowBase : IWorkflow
    {
        #region Properties

        public List<IStep> WorkflowSteps { get; set; }
        public IDirector Director { get; set; }
        public IClient Client { get; set; }
        public ParserResolver ParserResolver { get; set; }
        public Status WorkflowStatus
        {
            get
            {
                if (WorkflowSteps.Any(x => x.StepStatus == Status.Fail))
                    return Status.Fail;
                else if (WorkflowSteps.All(x => x.StepStatus == Status.Pass))
                    return Status.Pass;
                else if (WorkflowSteps.Any(x => x.StepStatus == Status.InProgress))
                    return Status.InProgress;
                else
                    return Status.NotStarted;
            }
        }

        public IEngine Engine { get; set; }

        #endregion Properties

        #region Concrete Methods

        public abstract void PrepareWorkflow(IDirector director, IClient client, BookingParserResolver parserResolver,IConfigurationData configurationData);

        public virtual BookingResponse PerformWorkflow(ProviderRequest ProviderReq, List<ResponseData> responseData, IConfigurationData configurationData)
        {
            BookingResponse bookingResponse = null;
            foreach (IStep st in WorkflowSteps)
            {
                if (bookingResponse != null && !bookingResponse.IsSuccess && st.RunOn == StepRunOn.Fail)
                {
                    var bResponse = st.PerformStep(Engine, ProviderReq, responseData);
                    continue;
                }
                else if (bookingResponse == null || bookingResponse.IsSuccess && st.RunOn == StepRunOn.Pass)
                {
                    bookingResponse = st.PerformStep(Engine, ProviderReq, responseData);
                    if (bookingResponse == null || bookingResponse.GDSStatus == GDSStatus.Failed || bookingResponse.IsSuccess == false || bookingResponse.IsContractSoldOutOrUnavailable)
                    {
                        st.StepStatus = Status.Fail;
                    }
                }
            }
            return bookingResponse;
        }
        
        #endregion Concrete Methods
    }
}
