﻿
using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Parsers.Parser.Interfaces;
using Flight.TravelAPI.Parsers.Travelport;
using Flight.TravelAPI.Providers.Base;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml;

namespace Flight.TravelAPI.Workflows.Booking.Base
{
    public abstract class Step : IStep
    {
        private readonly IDirector _director;
        private readonly IClient _client;
        protected Parser Parser;
        protected IBookingParser BookingParser;
        protected string ContentType;
        private IConfigurationData _configurationData;
        #region Properties

        public ProcessMode StepName { get; set; }

        public IStep NextStep { get; set; }

        public Status StepStatus { get; set; }

        public StepRunOn RunOn { get; set; }
        public bool IsSaveResponse { get; set; }

        #endregion Properties

        #region Constructors

        public Step(IDirector director, IClient client,IConfigurationData data)
        {
            StepStatus = Status.NotStarted;
            _director = director ?? throw new ArgumentNullException(nameof(director));
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _configurationData = data;
           

        }

        #endregion Constructors

        public BookingResponse PerformStep(IEngine Engine, ProviderRequest ProviderReq, List<ResponseData> responseData)
        {
            BookingResponse bookingResponse = null;
            StepStatus = Status.InProgress;
            var startTime = DateTime.Now;
            var request = _director.Construct(Engine, ProviderReq, StepName);
            string response = string.Empty;
            XmlDocument doc = new XmlDocument();
            //doc.Load(@"D:\APIWork\TravelportRQRS\2021-12-15\LAS_LAX_TRDHF-HFKGU-JFKFG-SJKKK\Booking_LAS_LAX__09-15-26-340_RS.XML");
            for (int loop = 1; loop < 4; loop++)
            {
                response = _client.CallService(Engine, request, ContentType, StepName.ToString());
                if (!response.Contains("UNA PROC-RETRY ENTRY IN 5 SECONDS"))
                {
                    break;
                }
                Thread.Sleep(6000);
            }
            try
            {
                Utilities.UtilityMethods.WriteProviderRQRS(_configurationData, $"Booking_{StepName.ToString()}_{ProviderReq.SelectedContract.Origin}_{ProviderReq.SelectedContract.Destination}"
                    , ProviderReq.SearchGuid, request, response);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            bookingResponse = BookingParser.Parse(ProviderReq, response);
            if (IsSaveResponse)
            {
                responseData.Add
                (
                    new ResponseData
                    {
                        BookingStatus = bookingResponse.CurrentBookingStatus.ToString(),
                        EndTime = DateTime.Now,
                        Engine = Engine.Engine,
                        GDSConfirmationNo = bookingResponse.GDSConfirmationNo,
                        GDSStatus = bookingResponse.GDSStatus.ToString(),
                        IsPriceChanged = bookingResponse.IsPriceChanged,
                        IsSoldOut = bookingResponse.IsContractSoldOutOrUnavailable,
                        IsSuccess = bookingResponse.IsSuccess,
                        PNR = bookingResponse.PNR,
                        Request = request,
                        Response = response,
                        SearchGuid = ProviderReq.SearchGuid,
                        StartTime = startTime,
                        StepName = StepName.ToString(),
                    }
                );
            }
            return bookingResponse;
        }
    }
}
