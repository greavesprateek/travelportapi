﻿
using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Providers.Base;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;

namespace Flight.TravelAPI.Workflows.Booking.Base.Interfaces
{
    public interface IWorkflow
    {
        List<IStep> WorkflowSteps { get; set; }
        Status WorkflowStatus { get; }
        IEngine Engine { get; set; }
        IDirector Director { get; set; }
        IClient Client { get; set; }
        ParserResolver ParserResolver { get; set; }
        void PrepareWorkflow(IDirector director, IClient client, BookingParserResolver parserResolver, IConfigurationData configurationData);
        BookingResponse PerformWorkflow(ProviderRequest ProviderReq, List<ResponseData> responseData,  IConfigurationData configurationData);
    }
}
