﻿using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Providers.Base;
using System.Collections.Generic;

namespace Flight.TravelAPI.Workflows.Booking.Base.Interfaces
{
    public interface IStep
    {
        ProcessMode StepName { get; set; }
        IStep NextStep { get; set; }
        Status StepStatus { get; set; }
        StepRunOn RunOn { get; set; }
        bool IsSaveResponse { get; set; }
        BookingResponse PerformStep(IEngine Engine, ProviderRequest ProviderReq, List<ResponseData> responseData);
    }
}
