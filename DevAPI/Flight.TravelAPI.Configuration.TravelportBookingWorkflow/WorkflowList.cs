﻿using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using System.Collections.Generic;

namespace Flight.TravelAPI.Configuration.TravelportBookingWorkflow
{
    public class WorkflowList
    {
        public List<IWorkflow> Workflows { get; set; }
    }
}
