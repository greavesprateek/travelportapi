﻿namespace Flight.TravelAPI.Models.Travelport
{
    public class Address
    {
        public string AddressName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string Location { get; set; }

    }
}
