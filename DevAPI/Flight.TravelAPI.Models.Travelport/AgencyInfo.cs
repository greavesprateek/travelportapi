﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flight.TravelAPI.Models.Travelport
{
    public class AgencyInfo
    {
        public string ActionType { get; set; }
        public string AgentCode { get; set; }
        public string BranchCode { get; set; }
        public string AgencyCode { get; set; }
        public DateTime EventTime { get; set; }
    }
}
