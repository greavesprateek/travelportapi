﻿namespace Flight.TravelAPI.Models.Travelport
{
    public class SSR
    {
        public string Status { get; set; }
        public string Type { get; set; }
        public string FreeText { get; set; }
        public string Carrier { get; set; }
    }
}
