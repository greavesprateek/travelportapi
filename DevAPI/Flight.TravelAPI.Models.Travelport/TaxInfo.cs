﻿namespace Flight.TravelAPI.Models.Travelport
{
    public class TaxInfo
    {
        public string Category { get; set; }
        public string Amount { get; set; }
    }
}
