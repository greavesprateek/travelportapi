﻿using Flight.TravelAPI.Models.Response;
using System;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Travelport
{
    public class AirPricingInfo
    {
        public string AirPricingInfoKey { get; set; }
        public string ApproximateTotalPrice { get; set; }
        public string TotalPrice { get; set; }
        public string ApproximateBasePrice { get; set; }
        public string BasePrice { get; set; }
        public string ApproximateTaxes { get; set; }
        public string Taxes { get; set; }
        public DateTime LatestTicketingTime { get; set; }
        public string PricingMethod { get; set; }
        public bool Refundable { get; set; }
        public string ETicketability { get; set; }
        public string PlatingCarrier { get; set; }
        public string ProviderCode { get; set; }
        public string Cat35Indicator { get; set; }
        public string PassengerType { get; set; }

        public List<FareInfoRule> ListOfFareInfo { get; set; }
        public List<BookingInfo> ListOfBookingInfo { get; set; }
        public List<TaxInfo> TaxList { get; set; }
        public List<string> ChangePenalty { get; set; }
    }
}