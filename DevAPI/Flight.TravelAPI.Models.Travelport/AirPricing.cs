﻿using Flight.TravelAPI.Models.Request;
using System.Collections.Generic;

namespace Flight.TravelAPI.Models.Travelport
{
    public class AirPricing
    {
        public string ApproximateTotalPrice { get; set; }
        public string ApproximateBasePrice { get; set; }
        public string ApproximateTaxes { get; set; }
        public string AirPricingKey { get; set; }
        public List<string> JourneySegmentRef { get; set; }
        public List<AirPricingInfo> AirPricingInfo { get; set; }
        public List<HostToken> HostTokenList { get; set; }
        public List<Connection> ListOfSegmentIndex { get; set; }
    }
}
