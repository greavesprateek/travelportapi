﻿using System;
using System.Linq;
using System.Text;
using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using Flight.TravelAPI.Providers.Base;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace Flight.TravelAPI.Client
{
    public class RestSharpClient : IClient
    {
        private readonly ILogger<IClient> _logger;
        private readonly ProviderList _providerList;

        //private readonly IRestClient _restClient;
        public RestSharpClient(ILogger<IClient> logger /*, IRestClient restClient*/, ProviderList providerList)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            //_restClient = restClient ?? throw new ArgumentNullException(nameof(logger));
            _providerList = providerList ?? throw new ArgumentNullException(nameof(providerList));
        }

        public string CallService(string requestUrl, string body, string contentType, string timeout,
            string action = null, string Engine = null)
        {

            try
            {
                var restClient = new RestClient(requestUrl) {Timeout = -1};
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", contentType);
                if (!string.IsNullOrEmpty(action)) request.AddHeader("SOAPAction", action);

                request.AddHeader("Accept", "text/xml");
                request.AddHeader("Accept-Encoding", "GZip");
                request.AddParameter(contentType, body, ParameterType.RequestBody);
                if (Engine != null)
                {
                    switch (Engine.ToUpper())
                    {
                        case "TRAVELPORT":
                            var travelportSettings = _providerList.Providers
                               .Where(x => x.BasedOn == GDS.Travelport && x.Engine == Engine)
                               .ToList().FirstOrDefault();
                            request.AddHeader("Authorization",
                                $"Basic {Convert.ToBase64String(Encoding.ASCII.GetBytes(travelportSettings.UserName + ":" + travelportSettings.UserPwd))}");
                            break;
                        default:
                            break;
                    }
                }
                  var response = restClient.Execute(request);
                if (response.ErrorException != null || response.ResponseStatus != ResponseStatus.Completed || !response.IsSuccessful)
                {
                    var error = string.Empty;
                    if (response.ErrorException != null)
                    {
                        error = response.ErrorException.StackTrace;
                    }
                    _logger.LogError($"RESTClient | URL: {requestUrl}  | Status: {response.StatusDescription} | Exception: {error} | Response: {response.Content}");
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                _logger.LogError($"RESTClient | URL: {requestUrl} Exception: {ex.Message} StackTrace: {ex.StackTrace}");
            }

            return null;
        }

        public string CallService(IEngine engine, string body, string contentType,
           string action = null)
        {
            return CallAPI(engine, body, contentType, action);
        }

        private string CallAPI(IEngine engine, string body, string contentType, string action)
        {
            try
            {
                var restClient = new RestClient(engine.ApiUrl) { Timeout = -1 };
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", contentType);
                if (!string.IsNullOrEmpty(action)) request.AddHeader("SOAPAction", action);

                request.AddHeader("Accept", "text/xml");
                request.AddHeader("Accept-Encoding", "GZip");
                request.AddParameter(contentType, body, ParameterType.RequestBody);
                if (engine != null)
                {
                    switch (engine.Engine.ToUpper())
                    {
                        case "TRAVELPORT":
                            var travelportSettings = _providerList.Providers
                               .Where(x => x.BasedOn == GDS.Travelport && x.Engine == engine.Engine)
                               .ToList().FirstOrDefault();
                            request.AddHeader("Authorization",
                                $"Basic {Convert.ToBase64String(Encoding.ASCII.GetBytes(travelportSettings.UserName + ":" + travelportSettings.UserPwd))}");
                            break;
                        default:
                            break;
                    }
                }
                var response = restClient.Execute(request);
                if (response.ErrorException != null || response.ResponseStatus != ResponseStatus.Completed || !response.IsSuccessful)
                {
                    var error = string.Empty;
                    if (response.ErrorException != null)
                    {
                        error = response.ErrorException.StackTrace;
                    }
                    _logger.LogError($"RESTClient | URL: {engine.ApiUrl}  | Status: {response.StatusDescription} | Exception: {error} | Response: {response.Content}");
                }
                return response.Content;
            }
            catch (Exception ex)
            {
                _logger.LogError($"RESTClient | URL: {engine.ApiUrl} Exception: {ex.Message} StackTrace: {ex.StackTrace}");
            }

            return null;
        }
    }
}