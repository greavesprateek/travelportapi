﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Providers.Base;
using Microsoft.Extensions.Logging;

namespace Flight.TravelAPI.Client
{
    public class ServiceClient : IClient
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<IClient> _logger;

        public ServiceClient(ILogger<IClient> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
        }

        public string CallService(string requestUrl, string body, string contentType, string timeout,
            string action = null, string Engine = null)
        {
            string apiResponse = null;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var request = new StringContent(body ?? string.Empty, Encoding.UTF8, contentType);
                using (var httpClient = _httpClientFactory.CreateClient())
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    if (!string.IsNullOrEmpty(action)) httpClient.DefaultRequestHeaders.Add("SOAPAction", action);

                    using (var response = httpClient.PostAsync(requestUrl, request))
                    {
                        apiResponse = response.Result.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"RESTClient | URL: {requestUrl} Exception: {ex.Message} StackTrace: {ex.StackTrace}");
            }

            return apiResponse;
        }

        public string CallService(IEngine engine, string body, string contentType, string action = null)
        {
            throw new NotImplementedException();
        }
    }
}