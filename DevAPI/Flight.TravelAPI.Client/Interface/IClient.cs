﻿using Flight.TravelAPI.Providers.Base;

namespace Flight.TravelAPI.Client.Interface
{
    public interface IClient
    {
        string CallService(string requestUrl, string body, string contentType, string timeout, string action = null, string Engine = null);
        string CallService(IEngine engine, string body, string contentType, string action = null);
    }
}