﻿using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Configuration.Load.Interfaces;
using Flight.TravelAPI.Configuration.TravelportBookingWorkflow;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Settings.DB;
using Flight.TravelAPI.Models.Settings.JSON;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using Flight.TravelAPI.Providers.Travelport.Interfaces;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;

namespace Flight.TravelAPI.Configuration.Load
{
    public class ConfigurationLoader : IConfigurationLoader
    {
        private readonly IConfiguration _configuration;
        private readonly IConfigurationData _configurationData;
        private readonly ILogger<ConfigurationLoader> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly ProviderList _providerList;
        private readonly WorkflowList _workflowList;
        private readonly IDirector _director;
        private readonly IClient _client;
        private readonly BookingParserResolver _parserResolver;
        public ConfigurationLoader(IConfiguration configuration, ILogger<ConfigurationLoader> logger,
            IServiceProvider serviceProvider, IConfigurationData configurationData, ProviderList providerList, WorkflowList workflowList
            , IDirector director, IClient client, BookingParserResolver parserResolver)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            _configurationData = configurationData ?? throw new ArgumentNullException(nameof(configurationData));
            _providerList = providerList ?? throw new ArgumentNullException(nameof(providerList));
            _workflowList = workflowList ?? throw new ArgumentNullException(nameof(workflowList));
            _workflowList.Workflows = new List<IWorkflow>();
            _director = director ?? throw new ArgumentNullException(nameof(director));
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _parserResolver = parserResolver ?? throw new ArgumentNullException(nameof(parserResolver));
        }

        public void LoadAll()
        {
            try
            {
                _configurationData.IsProviderRQRSEnabled =
                    Convert.ToBoolean(_configuration.GetSection("IsProviderRQRSEnabled").Value);
                _configurationData.ProviderRQRSPath = _configuration.GetSection("ProviderRQRSPath").Value;
                _configurationData.ServerName = _configuration.GetSection("ServerName").Value;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }

            //Task.Factory.StartNew(() =>
            //{
            try
            {
                var travelportEngineDetails =
                 _configuration.GetSection("TRAVELPORT:Providers").Get<List<TravelportEngineDetail>>();
                var providerDetails = _configuration.GetSection("ProviderDetails").Get<List<ProviderDetail>>()
                    .Where(x => x.IsEnabled);
                travelportEngineDetails.ForEach(x =>
                   _providerList.Providers = x.Update(providerDetails.FirstOrDefault(y => y.ProviderEngine == x.Engine),
                       (ITravelportEngine)_serviceProvider.GetService(typeof(ITravelportEngine))).ToList());
                _workflowList.Workflows = new List<IWorkflow>();
                travelportEngineDetails.ForEach(x =>
                _workflowList.Workflows.AddRange(
                    x.Update((IWorkflow)_serviceProvider.GetService(typeof(Workflows.Booking.Travelport.TravelportWorkflow)), 
                    (ITravelportEngine)_serviceProvider.GetService(typeof(ITravelportEngine)),
                    _director, _client, _parserResolver, _configurationData)));

            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            // });

            try
            {
                Task.Factory.StartNew(() =>
                    _configurationData.AeroplaneDetails =
                        _configuration.GetSection("AeroplaneDetails").Get<List<AeroplaneDetail>>());
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }


        }


    }
}