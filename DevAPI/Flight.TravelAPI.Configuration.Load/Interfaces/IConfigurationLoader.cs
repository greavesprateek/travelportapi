﻿namespace Flight.TravelAPI.Configuration.Load.Interfaces
{
    public interface IConfigurationLoader
    {
        void LoadAll();
    }
}