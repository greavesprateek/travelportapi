﻿using Flight.TravelAPI.Models.Booking;

namespace Flight.TravelAPI.Parsers.Parser.Interfaces
{
    public interface IBookingParser
    {
        BookingResponse Parse(ProviderRequest ProviderReq, string response);
    }
}
