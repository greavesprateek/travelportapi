﻿using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Parsers.Parser.Interfaces;

namespace Flight.TravelAPI.Parsers.Parser
{
    public class ParserDelegate
    {
        public delegate object ParserResolver(ProcessMode processMode);
        public delegate IBookingParser BookingParserResolver(ProcessMode processMode);
        
    }
}