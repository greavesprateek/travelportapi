﻿using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Parsers.Travelport;
using Flight.TravelAPI.Workflows.Booking.Base;
using Microsoft.Extensions.Logging;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;

namespace Flight.TravelAPI.Workflows.Booking.Travelport.Steps
{
    internal class AirCreateReservation : Step
    {
        public AirCreateReservation(IDirector director, IClient client, BookingParserResolver bookingParserResolver,IConfigurationData configurationData) : base(director, client,configurationData)
        {
            if (bookingParserResolver != null)
            {
                BookingParser = bookingParserResolver(ProcessMode.AirCreateReservationRQ);
            }
            StepName = ProcessMode.AirCreateReservationRQ; ;
            RunOn = StepRunOn.Pass;
            IsSaveResponse = true;
            ContentType = "text/xml";
        }
    }
}
