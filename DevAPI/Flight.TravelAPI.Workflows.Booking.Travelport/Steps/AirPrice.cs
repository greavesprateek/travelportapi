﻿using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Workflows.Booking.Base;
using Microsoft.Extensions.Logging;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;

namespace Flight.TravelAPI.Workflows.Booking.Travelport.Steps
{
    internal class AirPrice : Step
    {
        public AirPrice(IDirector director, IClient client, BookingParserResolver parserResolver,IConfigurationData data) : base(director, client,data)
        {
            if (parserResolver != null)
            {
                BookingParser = parserResolver(ProcessMode.AirPriceRQ);
            }
            StepName = ProcessMode.AirPriceRQ; ;
            RunOn = StepRunOn.Pass;
            IsSaveResponse = true;
            ContentType = "text/xml";
        }
    }
}
