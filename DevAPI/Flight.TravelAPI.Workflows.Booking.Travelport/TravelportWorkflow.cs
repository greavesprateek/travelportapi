﻿using System.Collections.Generic;
using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Workflows.Booking.Base;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using Flight.TravelAPI.Workflows.Booking.Travelport.Steps;
using Microsoft.Extensions.Logging;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;

namespace Flight.TravelAPI.Workflows.Booking.Travelport
{
    public class TravelportWorkflow : WorkflowBase
    {
        public override void PrepareWorkflow(IDirector director, IClient client, BookingParserResolver parserResolver,IConfigurationData configurationData)
        {
            WorkflowSteps = new List<IStep>
            {
                new AirPrice(director, client, parserResolver,configurationData),
                new AirCreateReservation(director, client, parserResolver,configurationData),
            };
        }
    }
}
