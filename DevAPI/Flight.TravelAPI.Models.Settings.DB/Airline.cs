﻿namespace Flight.TravelAPI.Models.Settings.DB
{
    public class Airline
    {
        public string Name { get; set; }
        public bool IsLCC { get; set; }
    }
}