﻿using System;

namespace Flight.TravelAPI.Models.Settings.DB
{
    public class AirlineBlock
    {
        public long ID { get; set; }
        public string VALIDATING_CARRIER { get; set; }
        public string MARKETING_CARRIER { get; set; }
        public string OPERATED_BY_CARRIER { get; set; }
        public string SUPPLIER { get; set; }
        public string GDS { get; set; }
        public string PORTAL { get; set; }
        public int PORTALID { get; set; }
        public string BLOCKED { get; set; }
        public int UPDATED_BY_ID { get; set; }
        public DateTime UPDATED_ON_DATE { get; set; }
        public string CLONE { get; set; }
        public string AFFLIATES { get; set; }
        public string ACTIVE { get; set; }
        public string ISAPPROVED { get; set; }
    }
}