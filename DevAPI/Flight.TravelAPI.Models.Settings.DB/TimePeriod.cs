﻿namespace Flight.TravelAPI.Models.Settings.DB
{
    public enum TimePeriod : byte
    {
        Custom = 0,
        Weekly = 1,
        Monthly = 2,
        Fortnightly = 3,
        Quarterly = 4
    }
}