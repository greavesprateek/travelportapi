﻿namespace Flight.TravelAPI.Models.Settings.DB
{
    public class TimeZones
    {
        public string IATA_Code { get; set; }
        public string IANA_TZ { get; set; }
        public string Windows_TZ { get; set; }
    }
}