﻿namespace Flight.TravelAPI.Models.Settings.ProviderDetails
{
    public class ProviderDetail
    {
        public int Affiliate { get; set; }
        public string ProviderEngine { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsPrivateFareEnable { get; set; }
        public bool IsPublishFareEnable { get; set; }
        public bool IsPrivateCorporateEnable { get; set; }
        public int PrivateCorporateRecommendationNo { get; set; }
        public bool IsFlexi { get; set; }
        public bool IsNearBy { get; set; }
        public bool IsPrivateFlexi { get; set; }
        public bool IsPrivateNearBy { get; set; }
        public bool IsPrivateCorporateFlexi { get; set; }
        public bool IsPrivateCorporateNearBy { get; set; }
        public bool IsDup { get; set; }
        public int NearByAiprotRecommendationNo { get; set; }
        public int FlexibleDateRecommendationNo { get; set; }
        public int PrivateRecommendationNo { get; set; }
        public int PublishRecommendationNo { get; set; }
        public int MobileRecommendationNo { get; set; }
        public string ExcludedAirlines { get; set; }
        public string ExcludedAirlinesForPublishFares { get; set; }
        public string ExcludedAirlinesForPrivateFares { get; set; }
        public string BlockAirlinesForMobile { get; set; }
        public string BlockAirlinesForDesktop { get; set; }
        public string NearByAirportRadiusForAmadeus { get; set; }
        public string FlexiDateInterval { get; set; }
        public int EnginePriority { get; set; }
        public bool IsPubtonetEnabled { get; set; }
        public bool IsSeatMapEnabled { get; set; }
        public bool IsAffiliateSearchEnable { get; set; }
        public bool IsDomesticEnable { get; set; }
        public bool IsInternationalEnable { get; set; }
    }
}