﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum TripType : byte
    {
        NONE = 0,
        ONEWAY = 1,
        ROUNDTRIP = 2,
        MULTICITY = 3
    }
}
