﻿namespace Flight.TravelAPI.Models.Enumeration
{
    public enum FareType
    {
        PUBLISHED,
        PRIVATE,
        WEBFARE
    }
}
