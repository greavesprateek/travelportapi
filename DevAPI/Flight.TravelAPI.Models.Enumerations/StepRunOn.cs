﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum StepRunOn
    {
        Pass = 0,
        Fail = 1,
        Both = 2
    }
}
