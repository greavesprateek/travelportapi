﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum EnumAddOnsService : int
    {
        None = 0,
        BeforePNRCreate = 1,
        AfterPNRCreate = 2,
        AfterTicketCreate = 3
    }
}
