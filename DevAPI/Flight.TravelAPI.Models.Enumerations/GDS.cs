﻿namespace Flight.TravelAPI.Models.Enumeration
{
    public enum GDS : byte
    {
        Amadeus = 1,
        Sabre = 2,
        Travelport = 3
    }
}