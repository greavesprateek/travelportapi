﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum ErrorCategory : byte
    {
        Error = 0,
        Warning = 1,
        Message = 2
    }
}
