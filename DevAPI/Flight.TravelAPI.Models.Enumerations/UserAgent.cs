﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum UserAgent : int
    {
        None = 0,
        Desktop = 1,
        Mobile = 2
    }
}
