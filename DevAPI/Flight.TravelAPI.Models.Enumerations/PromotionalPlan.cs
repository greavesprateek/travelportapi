﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum PromotionalPlan
    {
        Normal,
        Regular,
        Critical
    }
}
