﻿namespace Flight.TravelAPI.Models.Enumeration
{
    public enum SearchType : byte
    {
        Regular = 0,
        NearBy = 1,
        Flexi = 2
    }
}