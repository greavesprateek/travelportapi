﻿namespace Flight.TravelAPI.Models.Enumeration
{
    public enum SearchFareType : int
    {
        None = 0,
        Publish = 1,
        Private = 2,
        Corporate = 3
    }
}
