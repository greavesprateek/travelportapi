﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum CabinClass
    {
        None = 0,
        Economy = 1,
        EconomyCoach = 2,
        PremiumEconomy = 3,
        Business = 4,
        PremiumBusiness = 5,
        First = 6
    }
}
