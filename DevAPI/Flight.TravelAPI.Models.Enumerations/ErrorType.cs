﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum ErrorType : byte
    {
        GDSError = 0,
        ApplicationError = 1,
    }
}
