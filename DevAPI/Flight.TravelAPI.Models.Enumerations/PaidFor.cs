﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum PaidFor : byte
    {
        None = 0,
        MainBooking = 1,
        MCO = 2,
        Insurance = 3,
        SeatBooking = 4
    }
}
