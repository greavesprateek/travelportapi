﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum AmadeusRequestTypes
    {

        Authenticate,
        FareSearch,
        FareVerification,
        Booking,
        SignOut,
        SeatMap,
        StateLess,
        StateFullBookingStartOrInSeris,
        StateFullBookingEnd,
        FlightDetail,
        RetrievePNR
    }
}
