﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum BookingStatus : byte
    {
        None = 0,
        InProgress = 1,
        PriceChanged = 2,
        BookingConfirmed = 3,
        BackToListing = 4,
        PendingConfirmation = 5,
        Error = 6,
        SoldOutOrUnavailable = 7,
        Cancel = 8,
        BookWithLowerPrice = 9,
        PubToNetEnabled = 10,
        NormalBookingProcess,
        BookWithHigherPrice,
    }
}
