﻿namespace Flight.TravelAPI.Models.Enumeration
{
    public enum PaxType_XYMS
    {
        ADULT = 1,
        CHILD = 2,
        INFANT = 3,
        SENIOR = 4
    }
}