﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum FailureReason
    {
        None = 0,
        SoldOut = 1,
        Application_Error = 2,
        GDS_Error = 3,
        Frequent_Flyer_Not_Found = 4,
        Invalid_Credit_Card = 5,
        Credit_Card_Verification_Failed = 6,
        Invalid_Passport_Number = 7,
        PriceChanged = 8
    }
}
