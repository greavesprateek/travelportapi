﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum ProviderType
    {
        None = 0,
        AMADEUS = 1,
        TRANSAM = 2,
        AMADEUSJCB = 3,
        TRIPPRO = 4,
        GTT = 5,
        SABRE = 10,
        SKYBIRD = 6,
        SKYBIRDJCB = 7,
        ACCESSFARE = 12,
        CTS = 15,
        EASTCOST = 20,
        TRAVELPORT = 25,
        MYSTIFLY = 26,
        PERFECTTRAVEL = 30,
        SABREH0CK = 35,
        VOYZANT = 40,
        T5FK = 45,
        VOYZANTNDC = 41,
        K7TK = 46,
        DX1K = 47,
        GALILEO_6WE6 = 48,
    }
}
