﻿namespace Flight.TravelAPI.Models.Enumeration
{
    public enum MCOType : byte
    {
        Dollar = 1,
        Percentage = 2,
        PercentagePlusDollar = 3,
        DollarPlusPercentage = 4
    }
}
