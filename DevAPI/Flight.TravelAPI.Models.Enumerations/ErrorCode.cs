﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum ErrorCode
    {
        /*
         * For 
         General error code between 2000 to 2999
         * Air error code between 3000 to 3999
         * Hotel error code between 4000 to 4999
         * car error code between 5000 to 5999
         * Cross sell error code between 6000 to 6999
         * vacation error code between 7000 to 7999     
   */

        None = 0,
        Invalid_Credit_Card = 1000,
        Payment_Gateway_Exceptio = 1001,
        Application_Error = 1002,
        Frequent_Flyer_Not_Found = 3000,
        SoldOut = 3001,
        PriceChanged = 3002

    }
}
