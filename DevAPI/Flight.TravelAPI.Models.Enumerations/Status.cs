﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum Status
    {
        NotStarted,
        InProgress,
        Pass,
        Fail,
    }
}
