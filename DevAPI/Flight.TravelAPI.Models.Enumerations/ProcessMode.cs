﻿namespace Flight.TravelAPI.Models.Enumeration
{
    public enum ProcessMode : byte
    {
        TravelportSearch = 0,
        TravelportBook = 1,
        AirPriceRQ = 2,
        AirCreateReservationRQ = 3,
        RetrivePNR = 4,

    }
}