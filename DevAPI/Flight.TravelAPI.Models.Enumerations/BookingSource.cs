﻿namespace Flight.TravelAPI.Models.Enumerations
{
    public enum BookingSource : byte
    {
        All = 0,
        Air = 1,
        Hotel = 2,
        Car = 3,
        CrossSell = 4,
        Vacation = 5,
    }
}
