﻿using Flight.TravelAPI.Providers.Base;

namespace Flight.TravelAPI.Providers.Travelport.Interfaces
{
    public interface ITravelportEngine : IProvider
    {
        string Domain { get; set; }
        string ApiUrl { get; set; }
        string Timeout { get; set; }
        string RedEyeEndTime { get; set; }
        string BranchCode { get; set; }
    }
}
