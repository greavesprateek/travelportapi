﻿using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using Flight.TravelAPI.Providers.Travelport.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;

namespace Flight.TravelAPI.Providers.Travelport
{
    public class Provider : ITravelportEngine
    {
        private readonly IClient _client;
        private readonly IDirector _director;
        private readonly ILogger<Provider> _logger;
        private readonly ParserResolver _parserResolver;
        private IConfigurationData _configurationData;

        public Provider(ILogger<Provider> logger, IDirector director, IClient client, ParserResolver parserResolver, IConfigurationData data)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _director = director ?? throw new ArgumentNullException(nameof(director));
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _parserResolver = parserResolver ?? throw new ArgumentNullException(nameof(parserResolver));
            SearchTypes = new List<SearchType>();
            _configurationData = data ?? throw new ArgumentNullException(nameof(data));
        }

        public string RedEyeEndTime { get; set; }
        public List<SearchType> SearchTypes { get; set; }
        public ProviderDetail ProviderDetails { get; set; }
        public GDS BasedOn { get; set; }
        public string Engine { get; set; }
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        public string PccCode { get; set; }
        public string ApiUrl { get; set; }
        public string RedEyeTime { get; set; }
        public string Domain { get; set; }
        public string Timeout { get; set; }
        public string BranchCode { get; set; }

        public IEnumerable<FlightContracts> Process(FlightSearchDetails flightSearchDetails, ConcurrentBag<ResponseDetail> responseDetails)
        {
            var flightContracts = new ConcurrentBag<ConcurrentBag<FlightContracts>>();
            var searchType = SearchTypes.FirstOrDefault();
            //Parallel.ForEach(SearchTypes, searchType =>
            //{ 
            try
            {
                var wStopwatch = Stopwatch.StartNew();
                var startTime = DateTime.Now;

                var request = _director.Construct(processMode: ProcessMode.TravelportSearch, flightSearchDetails: flightSearchDetails, provider: this, searchType: searchType);
                //XmlDocument x = new XmlDocument();
                //x.Load(@"D:\D drive data\APIWork\TravelportRQRS\2021-11-27\LAS_LAX_48f882e6-4591-440f-9ad8-d5620def620b\Searhing_LAS_LAX__12-28-21-927_RQ.xml");
                //request = x.InnerXml.ToString();

                responseDetails.Add(new ResponseDetail
                {
                    APIServerName = _configurationData.ServerName,
                    SearchGuid = flightSearchDetails.FlightGuid,
                    Provider = Engine,
                    RequestType = searchType.ToString(),
                    Process = "Request",
                    StartTime = startTime,
                    EndTime = DateTime.Now,
                    TimeTaken = wStopwatch.ElapsedMilliseconds,
                    XMLData = request,
                });
                wStopwatch.Reset();
                wStopwatch.Start();
                startTime = DateTime.Now;

                if (string.IsNullOrEmpty(request)) return null;

                var response = _client.CallService(requestUrl: ApiUrl + "/AirService", body: request, contentType: "text/xml", timeout: "30000", Engine: "Travelport");

                responseDetails.Add(new ResponseDetail
                {
                    APIServerName = _configurationData.ServerName,
                    SearchGuid = flightSearchDetails.FlightGuid,
                    Provider = Engine,
                    RequestType = searchType.ToString(),
                    Process = "Response",
                    StartTime = startTime,
                    EndTime = DateTime.Now,
                    TimeTaken = wStopwatch.ElapsedMilliseconds,
                    XMLData = response,
                });
                wStopwatch.Reset();
                wStopwatch.Start();
                startTime = DateTime.Now;

                if (string.IsNullOrEmpty(response)) return null;

                var parser = (Parsers.Travelport.Parser)_parserResolver(ProcessMode.TravelportSearch);
                var parsedContracts = parser.Parse(response: response, flightSearchDetails: flightSearchDetails, detail: ProviderDetails);
                try
                {
                    Utilities.UtilityMethods.WriteProviderRQRS(_configurationData, $"Search_{"LowFareSearch"}_{flightSearchDetails.OriginAirportCode}_{flightSearchDetails.DestinationAirportCode}"
                        , flightSearchDetails.FlightGuid, request, response);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message} {ex.StackTrace}");
                }

                var responseDetail = new ResponseDetail
                {
                    APIServerName = _configurationData.ServerName,
                    SearchGuid = flightSearchDetails.FlightGuid,
                    Provider = Engine,
                    RequestType = searchType.ToString(),
                    Process = "Parsing",
                    StartTime = startTime,
                    EndTime = DateTime.Now,
                    TimeTaken = wStopwatch.ElapsedMilliseconds
                };
                wStopwatch.Stop();

                if (parsedContracts == null || !parsedContracts.Any()) return null;

                responseDetail.Results = parsedContracts.Count;
                responseDetails.Add(responseDetail);

                flightContracts.Add(parsedContracts);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }

            //});

            return flightContracts.SelectMany(x => x);
        }


        public ContractDetails GetDataUsingUniversalLocatorCode(string locatorCode, string partnerPCC)
        {
            ContractDetails flightContract = null;
            try
            {
                var request = _director.Construct(processMode: ProcessMode.RetrivePNR, provider: this, locatorCode: locatorCode, partnerPCC: partnerPCC);

                if (string.IsNullOrEmpty(request)) return null;

                var response = _client.CallService(requestUrl: ApiUrl + "/UniversalRecordService", body: request, contentType: "text/xml", timeout: "30000", Engine: "Travelport");

                if (string.IsNullOrEmpty(response)) return null;

                var parser = (Parsers.Travelport.RetrivePNRParser)_parserResolver(ProcessMode.RetrivePNR);
                // pNRDetails = parser.PNRResponseParse(response: response, detail: ProviderDetails);

                try
                {
                    Utilities.UtilityMethods.WriteProviderRQRS(_configurationData, $"RetrivePNR_{"UniversalRecordImport"}_{locatorCode}"
                        , new Guid().ToString(), request, response);
                }
                catch (Exception ex)
                {
                    //_logger.LogError($"{ex.Message} {ex.StackTrace}");
                }

                flightContract = parser.PNRResponseParse(response: response, detail: ProviderDetails,reponseType: "UniversalRecordRetrieveRsp");
                

            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }

            return flightContract;

        }

        public ContractDetails GetDataUsingPNR(string pnr)
        {
            ContractDetails flightContract = null;
            try

            {
                var request = _director.Construct(processMode: ProcessMode.RetrivePNR, provider: this, locatorCode: pnr, partnerPCC: "");

                if (string.IsNullOrEmpty(request)) return null;

                var response = _client.CallService(requestUrl: ApiUrl + "/UniversalRecordService", body: request, contentType: "text/xml", timeout: "30000", Engine: "Travelport");

                if (string.IsNullOrEmpty(response)) return null;

                try
                {
                    Utilities.UtilityMethods.WriteProviderRQRS(_configurationData, $"RetrivePNR_UniversalRecordRetrieve_{pnr}"
                        , new Guid().ToString(), request, response);
                }
                catch (Exception ex)
                {
                    //_logger.LogError($"{ex.Message} {ex.StackTrace}");
                }

                var parser = (Parsers.Travelport.RetrivePNRParser)_parserResolver(ProcessMode.RetrivePNR);
                // pNRDetails = parser.PNRResponseParse(response: response, detail: ProviderDetails);

                flightContract = parser.PNRResponseParse(response: response, detail: ProviderDetails,reponseType: "UniversalRecordImportRsp");
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return flightContract;
        }
    }
}
