﻿using Flight.TravelAPI.Models.Settings.DB;
using System.Collections.Generic;

namespace Flight.TravelAPI.Configuration.Data.Interfaces
{
    public interface IConfigurationData
    {
        string ProviderRQRSPath { get; set; }
        bool IsProviderRQRSEnabled { get; set; }
        string ServerName { get; set; }
        //Dictionary<string, int> DuplicateLogic { get; set; }
        List<AeroplaneDetail> AeroplaneDetails { get; set; }
        Dictionary<string, Airline> AllAirlines { get; set; }
        List<Airport> AllAirports { get; set; }
        List<TimeZones> TimeZoneInfo { get; set; }
        //bool SaveXML { get; set; }
        //int DefaultMarkup { get; set; }
    }
}