﻿using System.Collections.Generic;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Models.Settings.DB;

namespace Flight.TravelAPI.Configuration.Data
{
    public class ConfigurationData : IConfigurationData
    {
        public ConfigurationData()
        {
            AllAirports = new List<Airport>();
            TimeZoneInfo = new List<TimeZones>();
            AllAirlines = new Dictionary<string, Airline>();
            AeroplaneDetails = new List<AeroplaneDetail>();
        }

        public string ProviderRQRSPath { get; set; }
        public bool IsProviderRQRSEnabled { get; set; }
        public string ServerName { get; set; }
        public int FlexiInterval { get; set; }
        public bool UseFlexiInterval { get; set; }
        //public Dictionary<string, int> DuplicateLogic { get; set; }
        public List<AeroplaneDetail> AeroplaneDetails { get; set; }
        public Dictionary<string, Airline> AllAirlines { get; set; }
        public List<Airport> AllAirports { get; set; }
        public List<TimeZones> TimeZoneInfo { get; set; }
         //public bool SaveXML { get; set; }
        //public int DefaultMarkup { get; set; }
    }
}