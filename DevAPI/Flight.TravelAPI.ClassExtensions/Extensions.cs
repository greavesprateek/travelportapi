﻿using Flight.TravelAPI.Extensions.GlobalExceptionHandling;
using Microsoft.AspNetCore.Builder;
using System;

namespace Flight.TravelAPI.ClassExtensions
{
    public static class Extensions
    {
        public static bool IsDefault<T>(this T value) where T : struct
        {
            var isDefault = value.Equals(default(T));

            return isDefault;
        }

        public static T ConvertToEnum<T>(this string value) where T : struct, IConvertible
        {
            var enumVal = default(T);

            if (!typeof(T).IsEnum)
                throw new ArgumentException("Destination type is not enum");
            if (string.IsNullOrWhiteSpace(value) || Enum.TryParse(value, true, out enumVal) == false)
                throw new ArgumentException("Value is not valid Enum");
            return enumVal;
        }

        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
        
    }
}