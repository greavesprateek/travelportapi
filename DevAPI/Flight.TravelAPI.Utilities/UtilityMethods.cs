﻿using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Models.Enumerations;
using Flight.TravelAPI.Models.Response;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Flight.TravelAPI.Utilities
{
    public static class UtilityMethods
    {
        public static string PrepareContractKey(TripDetails segments)
        {
            var contractKey = string.Empty;
            if (segments?.OutBoundSegment == null || segments.OutBoundSegment.Count <= 0)
                return contractKey;
            var listOfSegments = segments.OutBoundSegment;
            if (segments.InBoundSegment != null && segments.InBoundSegment.Count > 0)
                listOfSegments = listOfSegments.Union(segments.InBoundSegment).ToList();

            contractKey = listOfSegments.Aggregate(contractKey, (current, s) => current +
                $"{(!string.IsNullOrEmpty(s.MarketingCarrier.AirlineCode) ? s.MarketingCarrier.AirlineCode : "")}" +
                $"{(!string.IsNullOrEmpty(s.FlightNumber) ? s.FlightNumber : "")}");
            return contractKey;
        }

        public static string PrepareDatesKey(FlightContracts contract)
        {
            var datesKey = string.Empty;
            if (contract?.TripDetails.OutBoundSegment == null || contract.TripDetails.OutBoundSegment.Count <= 0)
                return datesKey;
            if (contract.TripType == TripType.ROUNDTRIP.ToString())
                datesKey = datesKey + contract.TripDetails.OutBoundSegment.First().DepartureDate.Date +
                           contract.TripDetails.InBoundSegment.First().DepartureDate;
            else
                datesKey = datesKey + contract.TripDetails.OutBoundSegment.First().DepartureDate.Date;
            return datesKey;
        }

        public static void WriteProviderRQRS(IConfigurationData configurationData, string fileName, string guid, string rqXMLlog, string rsXMLLog)
        {

            bool isWriteLog = configurationData.IsProviderRQRSEnabled;
            string providerPath = configurationData.ProviderRQRSPath;

            if (isWriteLog)
            {
                string datePath = Path.Combine(providerPath, DateTime.Now.ToString("yyyy-MM-dd"), (guid));
                if (!Directory.Exists(@"" + datePath))
                {
                    Directory.CreateDirectory(@"" + datePath);
                }

                string datetime = string.Format("_{0}", DateTime.Now.ToString("hh-mm-ss-fff"));

                File.WriteAllText(@"" + datePath + "\\" + fileName + "_" + datetime + "_RQ" + ".xml", rqXMLlog, ASCIIEncoding.UTF8);
                File.WriteAllText(@"" + datePath + "\\" + fileName + "_" + datetime + "_RS" + ".xml", rsXMLLog, ASCIIEncoding.UTF8);
            }
        }

    }
}