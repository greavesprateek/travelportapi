﻿using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Providers.Base;

namespace Flight.TravelAPI.Builder.Interface
{
    public interface IBookingBuilder : IBuilder
    {
        void BuildHeader(IEngine engine, string binarySecurityToken = null);

        void BuildBody(IEngine engine, ProviderRequest providerRequest = null);
        
    }
}
