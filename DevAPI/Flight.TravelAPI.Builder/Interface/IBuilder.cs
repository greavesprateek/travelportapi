﻿namespace Flight.TravelAPI.Builder.Interface
{
    public interface IBuilder
    {
        string GetResult();
    }
}