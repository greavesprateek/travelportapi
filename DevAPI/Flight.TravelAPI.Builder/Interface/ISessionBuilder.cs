﻿namespace Flight.TravelAPI.Builder.Interface
{
    public interface ISessionBuilder : IBuilder
    {
        void BuildHeader(string userName, string userPwd, string pccCode, string domain);

        void BuildBody();
    }
}