﻿using Flight.TravelAPI.Providers.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Flight.TravelAPI.Builder.Interface
{
    public interface IRetrievePNRBuilder : IBuilder
    {
        void BuildHeader();

        void BuildBody(string locatorCode,string partnerPCC,IProvider provider);
    }
}
