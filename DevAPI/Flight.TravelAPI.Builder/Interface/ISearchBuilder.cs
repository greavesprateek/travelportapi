﻿using Flight.TravelAPI.Models.Enumeration;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Providers.Base;
using System.Collections.Generic;

namespace Flight.TravelAPI.Builder.Interface
{
    public interface ISearchBuilder : IBuilder
    {
        void BuildHeader(IProvider provider, SearchType searchType);

        void BuildBody(FlightSearchDetails flightSearchDetails, IProvider provider, SearchType searchType,
            KeyValuePair<string, string> qualifier = default(KeyValuePair<string, string>));
    }
}