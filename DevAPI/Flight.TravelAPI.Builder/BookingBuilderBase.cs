﻿using Flight.TravelAPI.Builder.Interface;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Providers.Base;

namespace Flight.TravelAPI.Builder
{
    public abstract class BookingBuilderBase : IBookingBuilder
    {
        protected string Body = string.Empty;
        protected string Header = string.Empty;

        public abstract void BuildHeader(IEngine engine, string binarySecurityToken = null);

        public abstract void BuildBody(IEngine engine, ProviderRequest providerRequest = null);

        public string GetResult()
        {
            return string.Format(Header, Body);
            //return $"{Header}{Body}";
        }
    }
}
