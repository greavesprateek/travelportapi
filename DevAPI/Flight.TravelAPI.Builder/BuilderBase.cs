﻿using Flight.TravelAPI.Builder.Interface;

namespace Flight.TravelAPI.Builder
{
    public class BuilderBase : IBuilder
    {
        protected string Body = string.Empty;
        protected string Header = string.Empty;

        public string GetResult()
        {
            return string.Format(Header, Body);
            //return $"{Header}{Body}";
        }
    }
}