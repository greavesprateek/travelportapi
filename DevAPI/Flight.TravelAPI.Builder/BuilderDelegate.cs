﻿using Flight.TravelAPI.Builder.Interface;
using Flight.TravelAPI.Models.Enumeration;

namespace Flight.TravelAPI.Builder
{
    public class BuilderDelegate
    {
        public delegate IBuilder BuilderResolver(ProcessMode processMode);
    }
}