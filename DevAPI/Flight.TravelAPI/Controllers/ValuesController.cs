﻿using Flight.TravelAPI.ClassExtensions;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Configuration.Load.Interfaces;
using Flight.TravelAPI.Configuration.TravelportBookingWorkflow;
using Flight.TravelAPI.Models.Booking;
using Flight.TravelAPI.Models.Request;
using Flight.TravelAPI.Models.Response;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Flight.TravelAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Route("Values")]
    public class ValuesController : ControllerBase
    {
        private readonly IConfigurationLoader _configurationLoader;
        private readonly ILogger<ValuesController> _logger;

        private readonly ProviderList _providerList;
        private IConfigurationData _data;
        private readonly List<IWorkflow> _workflows;
        //private readonly int _portalId;
        //private readonly string _apiServerName;
        //private readonly Dictionary<string, int> _duplicateLogic;

        public ValuesController(ILogger<ValuesController> logger, IConfigurationLoader configurationLoader,
            ProviderList providerList, IConfigurationData data, WorkflowList workflowList)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configurationLoader = configurationLoader ?? throw new ArgumentNullException(nameof(configurationLoader));
            _providerList = providerList ?? throw new ArgumentNullException(nameof(providerList));
            if (data == null)
            {
                return;
            }
            _data = data;
            var _workflowList = workflowList ?? throw new ArgumentNullException(nameof(workflowList));
            _workflows = _workflowList.Workflows;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Index()
        {
            try
            {
                return _providerList.Providers.Where(x => x.ProviderDetails != null && x.ProviderDetails.IsEnabled).Select(x =>
                    $"{x.ProviderDetails.ProviderEngine} is Enabled").ToArray();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }

            return new List<string> { "Unable to load provider details" };
        }


        [HttpPost]
        [Route("SearchFlights")]
        public ResponseWrapper SearchFlights(FlightSearchDetails flightSearchDetails)
        {
            var wStopwatchAll = Stopwatch.StartNew();
            var startTimeAll = DateTime.Now;
            try
            {
                var responseDetails = new ConcurrentBag<ResponseDetail>();
                if (string.IsNullOrEmpty(flightSearchDetails.FlightGuid))
                {
                    flightSearchDetails.FlightGuid = Guid.NewGuid().ToString();
                }

                var providers = _providerList.Providers;
                if (providers == null)
                {
                    _logger.LogError("Providers is null");
                }
                if (!providers.Any())
                {
                    _logger.LogError("No providers available");
                }
                var flightContracts = APICall(flightSearchDetails, responseDetails, providers);
                var responseWrapper = new ResponseWrapper();
                responseWrapper.FlightContracts = flightContracts.SelectMany(x => x).ToList();
                var index = 1;
                var responseWrapperFlightContracts = flightContracts.SelectMany(x => x);
                foreach (var contract in responseWrapperFlightContracts)
                {
                    contract.ContractID = index++;
                }

                wStopwatchAll.Stop();

                return responseWrapper;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }

            return null;
        }

        private ConcurrentBag<IEnumerable<FlightContracts>> APICall(FlightSearchDetails flightSearchDetails, ConcurrentBag<ResponseDetail> responseDetails, List<Providers.Base.IProvider> providers)
        {
            ConcurrentBag<IEnumerable<FlightContracts>> flightContracts = new ConcurrentBag<IEnumerable<FlightContracts>>();
            List<Task> lstTasks = new List<Task>();
            var provider = providers.FirstOrDefault();
            try
            {
                var wStopwatch = Stopwatch.StartNew();
                var startTime = DateTime.Now;

                var parsedContracts = provider.Process(flightSearchDetails, responseDetails);
                if (parsedContracts.Any()) flightContracts.Add(parsedContracts);

                wStopwatch.Stop();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }

            return flightContracts;
        }

        [HttpPost]
        [Route("BookFlights")]
        public BookingResponse BookFlights(ProviderRequest ProviderReq)
        {
           
            var startTime = DateTime.Now;
            List<ResponseData> responseData = new List<ResponseData>();
            var workflow = _workflows.FirstOrDefault(x => x.Engine.Engine == ProviderReq.SelectedContract.Provider);
            if (workflow == null)
            {
                return null;
            }
            var response = workflow.PerformWorkflow(ProviderReq, responseData, _data);
            Task.Factory.StartNew(() =>
            {
                try
                {
                    responseData.Add
                    (
                        new ResponseData
                        {
                            BookingStatus = response.CurrentBookingStatus.ToString(),
                            EndTime = DateTime.Now,
                            GDSConfirmationNo = response.GDSConfirmationNo,
                            GDSStatus = response.GDSStatus.ToString(),
                            IsPriceChanged = response.IsPriceChanged,
                            IsSoldOut = response.IsContractSoldOutOrUnavailable,
                            IsSuccess = response.IsSuccess,
                            PNR = response.PNR,
                            SearchGuid = ProviderReq.SearchGuid,
                            StartTime = startTime,
                            StepName = "All",
                        }
                    );

                    //_db.SaveResponseTime(responseData);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message} {ex.StackTrace}");
                }
            });
            if (response == null || response.GDSStatus == GDSStatus.Failed || response.IsSuccess == false || response.IsContractSoldOutOrUnavailable || response.IsPriceChanged)
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var serverNameKeyPair = Request.Headers.FirstOrDefault(x => x.Key == "X-ServerName");
                        var serverName = serverNameKeyPair.IsDefault() ? string.Empty : serverNameKeyPair.Value.FirstOrDefault();

                        ProviderReq.SelectedContract.TransactionID = -1;
                        ProviderReq.SelectedContract.SelectedContractBookingStatus = response.CurrentBookingStatus;
                        ProviderReq.SelectedContract.IsFlightSoldOrUnavailable = response.IsContractSoldOutOrUnavailable;
                        ProviderReq.SelectedContract.IsContractSoldOutOrUnavailable = response.IsContractSoldOutOrUnavailable;
                        ProviderReq.SelectedContract.IsPriceChanged = response.IsPriceChanged;
                        ProviderReq.SelectedContract.GDSResponseXML = !string.IsNullOrEmpty(response.ResponseXML) ? response.ResponseXML : string.Empty;
                        var context = new AirContext
                        {
                            SelectedContract = ProviderReq.SelectedContract,
                            BookDetails = ProviderReq.FlightBooking,
                            APIServer = _data.ServerName,
                            AppServer = serverName,
                            BookingResponse = response,
                        };
                        // _db.SaveFailedBookingInDB(context, "API-Payment-BookingVerification");
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"{ex.Message} {ex.StackTrace}");
                    }
                });
            }
            return response;
        }

        [HttpPost]
        [Route("GetDataUsingUniversalLocator")]
        public ContractDetails GetDataUsingUniversalLocator(string locatorCode, string partnerPCC)
        {
            try
            {
                if (string.IsNullOrEmpty(partnerPCC))
                    partnerPCC = "TEST";
                var providers = _providerList.Providers.FirstOrDefault();
                if (providers == null)
                {
                    _logger.LogError("Providers is null");
                    return null;
                }
                return providers.GetDataUsingUniversalLocatorCode(locatorCode, partnerPCC);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return null;
        }

        [HttpPost]
        [Route("GetDataUsingPNR")]
        public ContractDetails GetDataUsingPNR(string pnr)
        {
            try
            {
                var providers = _providerList.Providers.FirstOrDefault();
                if (providers == null)
                {
                    _logger.LogError("Providers is null");
                    return null;
                }
                return providers.GetDataUsingPNR(pnr);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message} {ex.StackTrace}");
            }
            return null;
        }
    }
}