﻿using System;
using Flight.TravelAPI.Client;
using Flight.TravelAPI.Client.Interface;
using Flight.TravelAPI.Configuration.Data;
using Flight.TravelAPI.Configuration.Data.Interfaces;
using Flight.TravelAPI.Configuration.Load;
using Flight.TravelAPI.Configuration.Load.Interfaces;
using Flight.TravelAPI.Directors;
using Flight.TravelAPI.Directors.Interface;
using Flight.TravelAPI.Models.Settings.ProviderDetails;
using Flight.TravelAPI.Providers.Travelport.Interfaces;
using Flight.TravelAPI.TravelportBuilder;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using static Flight.TravelAPI.Builder.BuilderDelegate;
using static Flight.TravelAPI.Parsers.Parser.ParserDelegate;
using Flight.TravelAPI.ClassExtensions;
using Flight.TravelAPI.Configuration.TravelportBookingWorkflow;
using Flight.TravelAPI.Workflows.Booking.Base.Interfaces;
using Flight.TravelAPI.Workflows.Booking.Travelport;
using Flight.TravelAPI.Parsers.Travelport;

namespace Flight.TravelAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            if (env == null) throw new ArgumentNullException(nameof(env));

            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            var appSettingsBuilder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
                .AddJsonFile($"engineSettings.{env.EnvironmentName}.json", true, true)
                .AddJsonFile($"providerSettings.{env.EnvironmentName}.json", true, true)
                .AddJsonFile($"aeroplaneSettings.{env.EnvironmentName}.json", true, true);
            Configuration = appSettingsBuilder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Travel API", Version = "version 1" });
            });
            services.AddSingleton(Configuration);
            services.AddHttpClient();
            services.AddSingleton<ProviderList>();
            services.AddSingleton<IConfigurationData, ConfigurationData>();
            services.AddSingleton<IConfigurationLoader, ConfigurationLoader>();
            services.AddTransient<ITravelportEngine, Providers.Travelport.Provider>();
            services.AddSingleton<IDirector, Director>();
            services.AddSingleton<IClient, RestSharpClient>();
            services.AddTransient<TravelportSearchBuilder>();
            services.AddSingleton<Parsers.Travelport.Parser>();
            services.AddSingleton<Parsers.Travelport.RetrivePNRParser>();
            services.AddTransient<AirPriceBuilder>();
            services.AddTransient<AirCreateReservationBuilder>();
            services.AddSingleton<WorkflowList>();
            services.AddTransient<TravelportWorkflow>();
            services.AddTransient<RetrivePNRBuilder>();
            services.AddTransient<AirPriceParser>();
            services.AddTransient<AirCreateReservationParser>();

            services.AddSingleton<BuilderResolver>(serviceProvider => key =>
            {
                switch (key)
                {
                    case Models.Enumeration.ProcessMode.TravelportSearch:
                        return serviceProvider.GetService<TravelportSearchBuilder>();
                    case Models.Enumeration.ProcessMode.AirPriceRQ:
                        return serviceProvider.GetService<AirPriceBuilder>();
                    case Models.Enumeration.ProcessMode.AirCreateReservationRQ:
                        return serviceProvider.GetService<AirCreateReservationBuilder>();
                    case Models.Enumeration.ProcessMode.RetrivePNR:
                        return serviceProvider.GetService<RetrivePNRBuilder>();
                    default:
                        return null;
                }
            });

            services.AddSingleton<ParserResolver>(serviceProvider => key =>
            {
                switch (key)
                {
                    case Models.Enumeration.ProcessMode.TravelportSearch:
                        return serviceProvider.GetService<Parsers.Travelport.Parser>();
                    case Models.Enumeration.ProcessMode.RetrivePNR:
                        return serviceProvider.GetService<Parsers.Travelport.RetrivePNRParser>();
                    default:
                        return null;
                }
            });
            services.AddSingleton<BookingParserResolver>(serviceProvider => key =>
            {
                switch (key)
                {
                    case Models.Enumeration.ProcessMode.AirPriceRQ:
                        return serviceProvider.GetService<AirPriceParser>();
                    case Models.Enumeration.ProcessMode.AirCreateReservationRQ:
                        return serviceProvider.GetService<AirCreateReservationParser>();
                    default:
                        return null;
                }
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            IConfigurationLoader configurationLoader, ILogger<Startup> logger)
        {
            configurationLoader.LoadAll();
            //xYMSLoader.Load();
            ChangeToken.OnChange(() => Configuration.GetReloadToken(), state => InvokeChanged(configurationLoader),
                env);
            app.ConfigureCustomExceptionMiddleware();

            logger.LogWarning($"Environment {env.EnvironmentName}");
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();
            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "TravelAPI");
            });
            app.UseMvc();
        }

        private void InvokeChanged(IConfigurationLoader configurationLoader)
        {
            configurationLoader.LoadAll();
        }
    }
}